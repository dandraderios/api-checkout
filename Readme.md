# API Checkout
Api de Pago , permite generar pagos asociados a un medio de pago en especifico.

## Configuración Inicial

configurar base de datos "DocumentDb" con las siguientes colecciones
 - payment_intentions
 - application_webhooks (partition key: **application**)
 - application_gateways_configuration (partition key: **payment_method**)
 - user_tokenizations (partition key: **application**)

 ### Configuración: **Payment Intention**

 Es necesario crear un trigger de nombre **CREATE_AUTOINCREMENT** del tipo **Pre** asociado a la acción **CREATE**.

```
 function CREATE_AUTOINCREMENT() {
  var collection = getContext().getCollection();
  var request = getContext().getRequest();
  var docToCreate = request.getBody();

  // Add Some Values , like the create time and the update time
  docToCreate.create_time = (new Date()).toISOString();
  docToCreate.update_time = (new Date()).toISOString();

  //Set the state to initial state: "CREATED"
  docToCreate.state = "created";

  collection.queryDocuments(
    collection.getSelfLink(),
    'SELECT * FROM c WHERE c.name="COLLECTION_COUNTERS"',
    function (err, docs, options) {
      if (docs.length > 0) {

        // Get the counters!
        var doc = docs[0];

        // Generate the invoice number
        var str = doc.counters.ordinal.toString();
        var pad = "0000000000";
        var ans = pad.substring(0, pad.length - str.length) + str

        docToCreate.invoice_number = "INPA-" + ans;

        // Set the Gateway Order Id to INPA if is not setted
        if (!docToCreate.transaction.gateway_order) {
          docToCreate.transaction.gateway_order = docToCreate.invoice_number;
        }

        doc.counters.ordinal = doc.counters.ordinal + 1;
        collection
          .replaceDocument(doc._self, doc, function (err, docReplaced) {
            request.setBody(docToCreate);
          });
      }
    });

}
```

Ademas de lo anterior, se debe generar un documento con los contadores incrementales (limitante dada por DocumentDB), la cúal sirve para saber el ordinal incremental asociado al invoice number con cada nuevo documento insertado:

```json
{
    "name": "COLLECTION_COUNTERS",
    "counters": {
        "ordinal": 1000
    }
}
```
