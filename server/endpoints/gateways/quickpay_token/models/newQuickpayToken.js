'use strict';
const mongoose = require('mongoose');
const validators = require('mongoose-validators');
/* eslint camelcase: ["error", {properties: "never"}] */
module.exports = new mongoose.Schema({
  capture_token: {
    type: String,
    required: false,
    validate: validators.isUUID(),
  },
});
