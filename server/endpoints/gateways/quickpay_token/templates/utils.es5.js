(function () {

    var Utilities = function () {

        /**
         * @param {string}  text - Text to trim
         * @return {string} trimmed text
         */
        this.trim = function (text) {
            // Make sure we trim BOM and NBSP
            var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
            return text.toString().replace(rtrim, '');
        }
    };

    var EventUtils = function () {

        /**
         * Cross Browser helper to addEventListener.
         *
         * @param {HTMLElement} obj The Element to attach event to.
         * @param {string} evt The event that will trigger the binded function.
         * @param {function(event)} fnc The function to bind to the element. 
         * @return {boolean} true if it was successfuly binded.
         */
        this.bind = function (obj, evt, fnc) {
            // W3C model
            if (obj.addEventListener) {
                obj.addEventListener(evt, fnc, false);
                return true;
            }
            // Microsoft model
            else if (obj.attachEvent) {
                return obj.attachEvent('on' + evt, fnc);
            }
            // Browser don't support W3C or MSFT model, go on with traditional
            else {
                evt = 'on' + evt;
                if (typeof obj[evt] === 'function') {
                    // Object already has a function on traditional
                    // Let's wrap it with our own function inside another function
                    fnc = (function (f1, f2) {
                        return function () {
                            f1.apply(this, arguments);
                            f2.apply(this, arguments);
                        }
                    })(obj[evt], fnc);
                }
                obj[evt] = fnc;
                return true;
            }
            return false;
        };

    };

    var DOMUtils = function () {
        /**
         * DOM Traversal (Sizzle)
         *
         * @param {string} selector Selector to find 
         * @return {HTMLElement} return the finded DOM element
         */
        this.findBySelector = function (selector) {
            return document.querySelector(selector);
        };

        /**
         * DOM Traversal (Sizzle)
         *
         * @param {string} selector Selector to find 
         * @return {HTMLElement} return the collection of DOM elements
         */
        this.findAllBySelector = function (selector) {
            return document.querySelectorAll(selector);
        };

        /**
         * DOM Traversal (Sizzle)
         *
         * @param {HTMLElement} elm Element to search in his parent's
         * @param {string} selector Selector to find 
         * @return {HTMLElement} return the finded DOM element
         */
        this.findParent = function (elm, selector) {
            return elm.closest(selector);
        };

        /**
         * DOM Traversal (Sizzle)
         *
         * @param {string} id id element to find 
         * @return {HTMLElement} return the finded DOM element
         */
        this.findById = function (id) {
            return document.getElementById(id);
        };

        /**
         * Add class to an element
         *
         * @param {HTMLElement} elm Element to add the classname
         * @param {string} classNameToAdd Class Name to Add
         */
        this.addClass = function (elm, classNameToAdd) {
            elm.className += ' ' + classNameToAdd;
        }

        /**
         * Remove class to an element
         *
         * @param {HTMLElement} elm Element to remove the classname
         * @param {string} classNameToAdd Class Name to Remove
         */
        this.removeClass = function (elm, classNameToRemove) {
            var elClass = ' ' + elm.className + ' ';
            while (elClass.indexOf(' ' + classNameToRemove + ' ') !== -1) {
                elClass = elClass.replace(' ' + classNameToRemove + ' ', '');
            }
            elm.className = elClass;
        }
    };

    window.qpay = {
        utils: new Utilities(),
        events: new EventUtils(),
        DOM: new DOMUtils()
    };
})();