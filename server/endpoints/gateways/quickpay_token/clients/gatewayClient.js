'use strict';
const fileSystem = require('fs');
const _ = require('lodash');
const Handlebars = require('handlebars');
const request = require('request');
const libxmljs = require('libxmljs');
const GatewayClientBase = require('./../../../../utils/GatewayClientBase');
const CONFIG = require('peanut-restify/config');
const logger = require('peanut-restify/logger');
const expr = require('peanut-restify/expressions');
const errors = require('./../../../../utils/errors');

const paymentClient = require('../../../payments/clients/paymentClient');
const tokenizationClient = require('../../../tokenizations/clients/tokenizationClient');
const $configurations = require('../../../configurations/clients/$configurations');
const app = require('./../../../../server');

class GatewayClient extends GatewayClientBase {

  /**
   * Check if the payment data is invalid
   * @param {*} data Model Data to check
   */
  isInvalid(data) {
    const model = app.models.newQuickpayToken(data);
    if (model.isInvalid()) {
      return model.getValidationErrors();
    }

    return false;
  }

  /**
   * Get Additional Links for the payment object
   * @param {String} payRoute base url to payment object
   */
  getAdditionalLinks(payRoute) {
    return [
      {
        href: `${payRoute}/silent`,
        rel: 'silent_charge',
        security: ['Jwt'],
        method: 'POST',
      },
      {
        href: `${payRoute}/refund`,
        rel: 'refund_method',
        security: ['Jwt'],
        method: 'POST',
      },
    ];
  }

  /**
   * Retrieve the gateway resume for the specific gateway
   * @param {any} payment Payment Document
   * @param {any} gatewayData Gateway Data, sended in the change state step
   * @returns {GatewayResume} gateway resume
   * @memberof GatewayClient
   */
  getGatewayResume(payment, gatewayData) {
    return new app.models.gatewayResume({
      'response': {
        'code': parseInt(gatewayData.reasonCode),
      },
      'transaction': {
        'gateway_id': gatewayData.requestID,
        'type': 'CREDIT',
        'date': (new Date()).toISOString(),
        'currency': gatewayData.purchaseTotals.currency,
        'buy_order': (gatewayData.merchantReferenceCode),
        'amount': gatewayData.ccAuthReply.amount,
        'installments_number': (gatewayData.installments_number || 0),
      },
      'authorizations': {
        'code': gatewayData.ccAuthReply.authorizationCode,
      },
      'card_number': {
        'pan_last4': gatewayData['capture_data'].panLast4,
        'pan_first6': gatewayData['capture_data'].panFirst6,
      },
    });
  };

  /**
   * Create a Capture Intention , with "embedded" UI for embedded  in the
   * Checkout
   * @param {*} payment Payment Datas
   */
  createCaptureIntention(payment, baseUrl) {
    return new Promise((resolve, reject) => {
      try {
        // Create a New Capture Intention
        request.post({
          url: CONFIG.get('GATEWAY_QUICKPAY_TOKEN_ENDPOINT'),
          json: true,
          body: {
            'capture': 'PEINAU_CAPTURE',
            'capture_method': 'TOKENIZATION',
            'cardholder': {
              'reference_id': payment.payer.payer_info.email,
              'name': payment.payer.payer_info.full_name,
              'email': payment.payer.payer_info.email,
              'country': payment.payer.payer_info.country,
            },
            'billing': {
              'line1': payment.transaction.item_list.shipping_address.line1,
              'city': payment.transaction.item_list.shipping_address.city,
              'state': payment.transaction.item_list.shipping_address.city,
              'country': payment.transaction.item_list.shipping_address.country_code,
            },
            'settings': {
              'UI': 'embedded',
            },
            'redirect_urls': {
              // ONLY FOR STANDARD, BUT THE CAPTURE CANCEL THE REDIRECTION VIA POSTMESSAGe
              'return_url': `${baseUrl}`,
              'cancel_url': `${baseUrl}`,
            },
          },
          headers: {
            'Authorization': `apikey ${CONFIG.get('GATEWAY_QUICKPAY_TOKEN_MASTERKEY')}`,
          },
        }, (error, response, body) => {
          // Has Error's?
          if (error) {
            return reject(new errors.PeinauError(
              'REQUEST_ERROR',
              'Internal error has ocurred in the request trying to create a capture intention', {
                'request_error': error,
              }
            ));
          }

          if (response.statusCode !== 201) {
            return reject(new errors.PeinauError(
              'INVALID_HTTP_STATUS_CODE_IN_REQUEST',
              'The request return a invalid status code response trying to create a capture intention', {
                'response_body': body,
              }
            ));
          }

          // JUST .... resolve xD!
          resolve(body);
        });
      } catch (ex) {
        return reject(new errors.UnknowError(
          'An error has ocurred trying to create a capture intention for quickpay_token',
          ex
        ));
      }
    });
  }

  /**
   * Get Approval Dialog
   * @param {String} paymentId Payment Token id
   * @param {*} req Http Request
   */
  getApprovalDialog(paymentId, req) {
    return new Promise((resolve, reject) => {
      paymentClient
        .getById(paymentId)
        .then((payment) => {
          // CHECK IF HAS CAPTURE TOKEN
          let paymentFlow = 'express';
          if (payment.additional_attributes && payment.additional_attributes.capture_token) {
            paymentFlow = 'with_token'; // FULL PROCESS (TOKENIZATION + CHECKOUT)
          }

          let templateUI = 'falabella';
          if (payment.settings && payment.settings.UI) {
            templateUI = payment.settings.UI.toLowerCase();
          }

          const templatePath = `${__dirname}/../templates/${paymentFlow}/${templateUI}/approval_dialog.hbs`;
          let content = 'TEMPLATE UI NOT FOUND';

          if (fileSystem.existsSync(templatePath)) {
            content = fileSystem.readFileSync(templatePath, 'utf-8');
          }
          var template = Handlebars.compile(content);
          payment.routePrefix = paymentClient.getBaseUrl(req);

          if (paymentFlow == 'with_token') {
            // GET THE CAPTURE CARD DETAILS (ALREADY TOKENIZED)
            tokenizationClient
              .getById(payment.additional_attributes.capture_token)
              .then((capture) => {
                payment.capture = capture;
                resolve(template(payment));
              }, reject);
          } else {
            // NOT TOKENIZATION YET, BRING IT ON THE FULL PROCESS
            // (EXPRESS CHECKOUT) => Tokenization + Checkout
            this
              .createCaptureIntention(payment, paymentClient.getBaseUrl(req))
              .then((newCapture) => {
                const captureUrl = _.find(newCapture.links, (link) => {
                  return link.rel === 'capture_url';
                });
                payment.captureUrl = captureUrl;
                payment.capture = newCapture;
                resolve(template(payment));
              }, reject);
          }
        }, reject);
    });
  };

  /**
   * Approve payment
   * @param {String} paymentId Payment Token
   * @param {*} gatewayData Gateway Payment Data
   */
  approvePayment(paymentId, gatewayData) {
    return new Promise((resolve, reject) => {
      paymentClient
        .getById(paymentId)
        .then((payment) => {
          let captureToken = gatewayData.capture_token;
          if (payment.additional_attributes &&
            payment.additional_attributes.capture_token) {
            captureToken = payment.additional_attributes.capture_token;
          }

          // Check state before do all other things cybersource
          switch (payment.state) {
            case 'rejected':
              // Cant be refunded
              return reject(new errors.PeinauError(
                'PAYMENT_IN_REJECTED_STATE_CANT_BE_PAID',
                'A payment in reject state cant be paid', {
                  'payment_token': paymentId,
                }
              ));
            case 'paid':
              return reject(new errors.PeinauError(
                'PAYMENT_ALREADY_PAID',
                'A payment in paid state cant be paid again', {
                  'payment_token': paymentId,
                }
              ));
          };

          Promise.all([
            tokenizationClient.getById(captureToken),
            $configurations.getConfiguration(payment.application, 'QUICKPAY_TOKEN'),
          ]).then((resolves) => {
            var capture = resolves[0];
            var configuration = resolves[1];

            // ADD THE DECISION MANAGER PLACE
            var defers = [];
            if (configuration.decisionManager_enabled) {
              defers.push(this.runDecisionManager(capture, configuration, payment));
            }

            Promise
              .all(defers)
              .then(() => {
                // EXECUTE PAY IN CYBERSOURCE
                var products = [];
                var pIndex = 0;
                _.each(payment.transaction.item_list.items, (item) => {
                  products.push(`<item id="${pIndex}">`);
                  products.push(` <unitPrice>${item.price}</unitPrice>`);
                  products.push(` <quantity>${item.quantity}</quantity>`);
                  products.push(` <productName>${item.name}</productName>`);
                  products.push(` <productSKU>${item.sku}</productSKU>`);
                  products.push('</item>');
                  pIndex++;
                });

                var installments = '';
                if (parseInt(gatewayData.installments_number) > 0) {
                  installments = [
                    '<installment>',
                    ` <totalCount>${parseInt(gatewayData.installments_number)}</totalCount>`,
                    '</installment>',
                  ].join('');
                }

                const ccv = [
                  '<card>',
                  ' <cvIndicator>1</cvIndicator>',
                  ' <cvNumber>123</cvNumber>',
                  '</card>',
                ].join('');

                var soapMessage = [
                  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">',
                  ' <soapenv:Header>',
                  '  <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">',
                  '   <wsse:UsernameToken>',
                  `    <wsse:Username>${configuration.webServiceUsername}</wsse:Username>`,
                  `    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wssusername-token-profile-1.0#PasswordText">${configuration.webServicePassword}</wsse:Password>`,
                  '   </wsse:UsernameToken>',
                  '  </wsse:Security>',
                  ' </soapenv:Header>',
                  ' <soapenv:Body>',
                  '  <requestMessage xmlns="urn:schemas-cybersource-com:transaction-data-1.115">',
                  `   <merchantID>${configuration.merchant}</merchantID>`,
                  `   <merchantReferenceCode>${payment.invoice_number}</merchantReferenceCode>`,
                  products.join(''),
                  '   <purchaseTotals>',
                  `    <currency>${payment.transaction.amount.currency}</currency>`,
                  `    <grandTotalAmount>${payment.transaction.amount.total}</grandTotalAmount>`,
                  '   </purchaseTotals>',
                  installments,
                  ccv,
                  '   <recurringSubscriptionInfo>',
                  `    <subscriptionID>${capture.gateway.payment_token}</subscriptionID>`,
                  '   </recurringSubscriptionInfo>',
                  '   <ccAuthService run="true"></ccAuthService>',
                  '   <ccCaptureService run="true"></ccCaptureService>',
                  '  </requestMessage>',
                  ' </soapenv:Body>',
                  '</soapenv:Envelope>',
                ].join('');

                request.post({
                  url: CONFIG.get('GATEWAY_CYBERSOURCE_TOKEN_ENDPOINT'),
                  json: false,
                  timeout: CONFIG.getNumber('GATEWAY_CYBERSOURCE_TIMEOUT'),
                  body: soapMessage,
                  headers: {
                    'Content-Type': 'application/xml',
                    'Cache-Control': 'no-cache',
                  },
                }, (err, response, body) => {
                  if (err) {
                    return reject(new errors.PeinauError(
                      'REQUEST_ERROR',
                      'Exception has ocurred when trying tokenize the credit card in cybersource', {
                        'request_error': err,
                      }
                    ));
                  }

                  // xpath queries
                  var xmlDoc = libxmljs.parseXml(body);
                  var nodes = xmlDoc.find('//c:replyMessage', {
                    c: 'urn:schemas-cybersource-com:transaction-data-1.115',
                  });

                  if (nodes.length == 0) {
                    let rawFault = body;
                    var soapFault = xmlDoc.find('//soap:Fault', {
                      soap: 'http://schemas.xmlsoap.org/soap/envelope/',
                    });
                    // if we can "clean" the fault
                    expr.whenTrue(soapFault.length > 0, () => {
                      rawFault = this.parseCybersourceResponse(soapFault);
                    });
                    return reject(new errors.PeinauError(
                      'GATEWAY_DATA_DONT_EXISTS',
                      'Exception ocurred when trying to read the cybersource response: reply message from cybersource is empty', {
                        'soap_fault': rawFault,
                      }
                    ));
                  }

                  Object.assign(gatewayData, this.parseCybersourceResponse(nodes));
                  if (!gatewayData.ccAuthReply) {
                    return reject(new errors.PeinauError(
                      'CYBERSOURCE_MISSING_REPLY',
                      'the response in cybersource has some invalid values', {
                        'gateway_data': gatewayData,
                        'payment_token': payment.id,
                      }
                    ));
                  }

                  if (gatewayData.ccAuthReply.reasonCode !== '100') {
                    // Don't throw .. just append to the gateway section
                    return paymentClient
                      .updateToRejectState(paymentId, {
                        error: new errors.LocalizedError(
                          `GW01_${gatewayData.reasonCode}`, {
                            'gateway_data': gatewayData,
                          }),
                      })
                      .then(resolve, reject);
                  }

                  // Add some important data :P to the gateway
                  gatewayData['capture_data'] = {
                    panFirst6: capture.gateway.panFirst6,
                    panLast4: capture.gateway.panLast4,
                  };

                  paymentClient
                    .updateToPaidState(paymentId, gatewayData)
                    .then(resolve, reject);
                });
              }, (ex) => {
                // Can be a Gateway Localized Error??
                if (ex instanceof errors.LocalizedError &&
                  ex.error_code.indexOf('GW01_') >= 0) {
                  // If Gateway Decision Manager error , dont throw error, just
                  // reject the payment.
                  return paymentClient
                    .updateToRejectState(paymentId, {
                      error: ex,
                    })
                    .then(resolve, reject);
                }
              });
          }, (err) => {
            reject(new errors.PeinauError(
              'ERROR_WAITING_FOR_PROMISES_TO_RESOLVE',
              'Exception has ocurred waiting for all promises to resolve when trying to approve payment', {
                'error': err,
              }
            ));
          });
        }, (err) => {
          return reject(new errors.PeinauError(
            'ERROR_GETTING_PAYMENT_ID',
            'Exception has ocurred trying to get the payment by his id', {
              'error': err,
            }
          ));
        });
    });
  };

  /**
   * Reject Payment
   * @param {String} paymentId Payment token id
   * @param {*} gatewayData Gateway Data
   */
  rejectPayment(paymentId, gatewayData) {
    gatewayData['remember_capture'] = false;
    return paymentClient.updateToRejectState(paymentId, gatewayData);
  };

  /**
   * Refund Payment
   * @param {String} paymentId Payment Token
   * @param {*} refundModel Gateway payment data
   */
  refundPayment(paymentId, refundModel) {
    return new Promise((resolve, reject) => {
      try {
        paymentClient
          .getById(paymentId)
          .then((payment) => {
            switch (payment.state) {
              case 'created':
                // Cant be refunded
                return reject(new errors.PeinauError(
                  'PAYMENT_IN_CREATED_STATE_CANT_BE_REFUNDED',
                  'A captu re in created state cant be refunded', {
                    'payment_token': paymentId,
                  }
                ));
              case 'rejected':
                // Cant be refunded
                return reject(new errors.PeinauError(
                  'PAYMENT_IN_REJECTED_STATE_CANT_BE_REFUNDED',
                  'A capture in reject state cant be refunded', {
                    'payment_token': paymentId,
                  }
                ));
              case 'refunded':
                return reject(new errors.PeinauError(
                  'PAYMENT_ALREADY_REFUNDED',
                  'The payment is not refund because the payment or credit information has already been submitted to your processor. Or, you requested a refund for a type of transaction that cannot be refunded.', {
                    'payment_token': paymentId,
                  }
                ));
            };

            if (!refundModel.hasOwnProperty('refunded_amount')) {
              return reject(new errors.PeinauError(
                'INVALID_PARAMETER_REQUEST',
                'The attribute refunded_amount is required in request.', {
                  'payment_token': paymentId,
                }
              ));
            }

            // CHECK IF REFUNDED AMOUNT IS NOT NUMERIC AND IF REFUNDED AMOUNT <= ZERO
            if (!parseFloat(refundModel.refunded_amount) || refundModel.refunded_amount <= 0) {
              return reject(new errors.PeinauError(
                'INVALID_REFUNDED_AMOUNT',
                'The refunded amount must be a numeric type and its value must be greater than zero.', {
                  'payment_token': paymentId,
                }
              ));
            }
            // CHECK IF REFUNDED AMOUNT > PAYMENT TOTAL
            // refundModel['refunded_amount'] = refundModel.refunded_amount || payment.transaction.amount.total;
            if (refundModel.refunded_amount > payment.transaction.amount.total) {
              return reject(new errors.PeinauError(
                'REFUNDED_AMOUNT_HIGHER_PAYMENT_TOTAL',
                'The refunded amount can not be higher than payment total.', {
                  'payment_token': paymentId,
                }
              ));
            }

            Promise.all([
              $configurations.getConfiguration(payment.application, 'QUICKPAY_TOKEN'),
            ]).then((resolves) => {
              try {
                var configuration = resolves[0];

                // Add Web Service
                var soapMessage = [
                  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">',
                  '   <soapenv:Header>',
                  '       <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">',
                  '           <wsse:UsernameToken>',
                  `               <wsse:Username>${configuration.webServiceUsername}</wsse:Username>`,
                  '               <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wssusername-token-profile-1.0#PasswordText">' + configuration.webServicePassword + '</wsse:Password>',
                  '           </wsse:UsernameToken>',
                  '       </wsse:Security>',
                  '   </soapenv:Header>',
                  '   <soapenv:Body>',
                  '       <requestMessage xmlns="urn:schemas-cybersource-com:transaction-data-1.115">',
                  `           <merchantID>${configuration.merchant}</merchantID>`,
                  `           <merchantReferenceCode>${payment.invoice_number}</merchantReferenceCode>`,
                  '           <purchaseTotals>',
                  `               <currency>${payment.transaction.amount.currency}</currency>`,
                  `               <grandTotalAmount>${refundModel.refunded_amount}</grandTotalAmount>`,
                  '           </purchaseTotals>',
                  '           <ccCreditService run="true">',
                  `	    		    <captureRequestID>${payment.gateway.requestID}</captureRequestID>`,
                  '		        </ccCreditService>',
                  '       </requestMessage>',
                  '   </soapenv:Body>',
                  '</soapenv:Envelope>',
                ].join('');

                request.post({
                  url: CONFIG.get('GATEWAY_CYBERSOURCE_TOKEN_ENDPOINT'),
                  json: false,
                  timeout: CONFIG.getNumber('GATEWAY_CYBERSOURCE_TIMEOUT'),
                  body: soapMessage,
                  headers: {
                    'Content-Type': 'application/xml',
                    'Cache-Control': 'no-cache',
                  },
                },
                  (error, response, body) => {
                    if (error) {
                      return reject(new errors.PeinauError(
                        'REQUEST_ERROR',
                        'internal error, soap request fails', {
                          'request_error': error,
                        }
                      ));
                    }

                    // xpath queries
                    var xmlDoc = libxmljs.parseXml(body);
                    var nodes = xmlDoc.find('//c:replyMessage', {
                      c: 'urn:schemas-cybersource-com:transaction-data-1.115',
                    });

                    if (nodes.length == 0) {
                      let rawFault = body;
                      var soapFault = xmlDoc.find('//soap:Fault', {
                        soap: 'http://schemas.xmlsoap.org/soap/envelope/',
                      });
                      // if we can "clean" the fault
                      expr.whenTrue(soapFault.length > 0, () => {
                        rawFault = this.parseCybersourceResponse(soapFault);
                      });
                      return reject(new errors.PeinauError(
                        'GATEWAY_DATA_DONT_EXISTS',
                        'Cybersource SOAP Message from cybersource or has some missing information', {
                          'soap_fault': rawFault,
                        }
                      ));
                    }

                    Object.assign(refundModel, this.parseCybersourceResponse(nodes));

                    if (refundModel.reasonCode !== '100') {
                      // Don't throw .. just append to the gateway section
                      return reject(new errors.LocalizedError(
                        `GW01_${refundModel.reasonCode}`, {
                          'error_details': refundModel,
                        }
                      ));
                    }

                    paymentClient
                      .updateToRefundedState(paymentId, refundModel)
                      .then(resolve, reject);
                  });
              } catch (ex) {
                return reject(new errors.UnknowError(
                  'An error has ocurred tryng to refund a payment',
                  ex
                ));
              }
            }, reject);
          }, reject);
      } catch (ex) {
        return reject(new errors.UnknowError(
          'An error has ocurred tryng to refund a payment',
          ex
        ));
      }
    });
  };

  /**
   * Run Decision Manager in Cybersource
   * @param {String} capture Capture Token
   * @param {*} configuration Configuration for the payment method
   * @param {*} payment Payment Object Data
   */
  runDecisionManager(capture, configuration, payment) {
    return new Promise((resolve, reject) => {
      // EXECUTE PAY IN CYBERSOURCE
      var products = [];
      var pIndex = 0;
      _.each(payment.transaction.item_list.items, (item) => {
        products.push('<item id="' + pIndex + '">');
        products.push('   <unitPrice>' + item.price + '</unitPrice>');
        products.push('   <quantity>' + item.quantity + '</quantity>');
        products.push('   <productName>' + item.name + '</productName>');
        products.push('   <productSKU>' + item.sku + '</productSKU>');
        products.push('</item>');
        pIndex++;
      });

      // Add Web Service
      var soapMessage = [
        '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">',
        '   <soapenv:Header>',
        '       <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">',
        '           <wsse:UsernameToken>',
        `               <wsse:Username>${configuration.webServiceUsername}</wsse:Username>`,
        `               <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wssusername-token-profile-1.0#PasswordText">${configuration.webServicePassword}</wsse:Password>`,
        '           </wsse:UsernameToken>',
        '       </wsse:Security>',
        '   </soapenv:Header>',
        '   <soapenv:Body>',
        '       <requestMessage xmlns="urn:schemas-cybersource-com:transaction-data-1.115">',
        `           <merchantID>${configuration.merchant}</merchantID>`,
        `           <merchantReferenceCode>${payment.invoice_number}</merchantReferenceCode>`,
        products.join(''),
        '           <purchaseTotals>',
        `               <currency>${payment.transaction.amount.currency}</currency>`,
        `               <grandTotalAmount>${payment.transaction.amount.total}</grandTotalAmount>`,
        '           </purchaseTotals>',
        '           <recurringSubscriptionInfo>',
        `              <subscriptionID>${capture.gateway.payment_token}</subscriptionID>`,
        '           </recurringSubscriptionInfo>',
        '           <afsService run="true" />',
        '           <businessRules>',
        `              <scoreThreshold>${configuration.decisionManager_threshold}</scoreThreshold>`,
        '           </businessRules>',
        `           <deviceFingerprintID>${capture.id}</deviceFingerprintID>`,
        '       </requestMessage>',
        '   </soapenv:Body>',
        '</soapenv:Envelope>',
      ].join('');

      request.post({
        url: CONFIG.get('GATEWAY_CYBERSOURCE_TOKEN_ENDPOINT'),
        json: false,
        timeout: CONFIG.getNumber('GATEWAY_CYBERSOURCE_TIMEOUT'),
        body: soapMessage,
        headers: {
          'Content-Type': 'application/xml',
          'Cache-Control': 'no-cache',
        },
      },
        (error, response, body) => {
          if (error) {
            return reject(new errors.PeinauError(
              'REQUEST_ERROR',
              'Internal error, Soap request fails', {
                'request_error': error,
              }
            ));
          }

          // xpath Queries
          var xmlDoc = libxmljs.parseXml(body);
          var nodes = xmlDoc.find('//c:replyMessage', {
            c: 'urn:schemas-cybersource-com:transaction-data-1.115',
          });

          if (nodes.length == 0) {
            let rawFault = body;
            var soapFault = xmlDoc.find('//soap:Fault', {
              soap: 'http://schemas.xmlsoap.org/soap/envelope/',
            });
            // if we can "clean" the fault
            expr.whenTrue(soapFault.length > 0, () => {
              rawFault = this.parseCybersourceResponse(soapFault);
            });
            return reject(new errors.PeinauError(
              'GATEWAY_DATA_DONT_EXISTS',
              'Fraud Message from cybersource is empty', {
                'soap_fault': rawFault,
              }
            ));
          }

          var gatewayData = this.parseCybersourceResponse(nodes);

          if (gatewayData.reasonCode !== '100') {
            return reject(new errors.LocalizedError(
              `GW01_${gatewayData.reasonCode}`, {
                'gateway_data': gatewayData,
              }
            ));
          }

          resolve(gatewayData);
        });
    });
  };

  /**
   * Parse Cybersource XML result
   * @param {any} nodes XML Nodes
   * @param {any} source XML source node
   * @returns {Object} Parsed XML into a JSON plain object
   * @memberof GatewayClient
   */
  parseCybersourceResponse(nodes, source) {
    var response = (source || {});

    if (nodes.length > 0) {
      var children = _.first(nodes).childNodes();
      _.each(children, (child) => {
        if (child.childNodes().length > 0 && child.childNodes()[0].type() !== 'text') {
          var childObj = {};
          _.each(child.childNodes(), (schild) => {
            childObj[schild.name()] = schild.text();
          });
          response[child.name()] = childObj;
        } else {
          response[child.name()] = child.text();
        }
      });
    }
    return response;
  };
}

const client = new GatewayClient();
module.exports = client;
