'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');
const paymentClient = require('../../../payments/clients/paymentClient');
const gatewayClient = require('../clients/gatewayClient');

function implementation(req, res, next) {
  var token = req.params.id;
  var gatewayData = (req.body || {});
  gatewayData.message = 'user cancel error';

  gatewayClient
    .rejectPayment(token, gatewayData)
    .then((payment) => {
      // ---------------------------------------------------------
      paymentClient
        .createErrorRedirectionPage(payment, {
          'error_code': -1,  // Reject Payment (Document is updated)
          'error_message': gatewayData.message,
        })
        .then((html) => {
          res.setHeader('content-type', 'text/html');
          res.setHeader('cache-control', 'public, max-age=3600'); // 1hr cache control
          res.send(HttpStatus.OK, html);
          next();
        }, next);
      // ---------------------------------------------------------
    }, next);
}

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.post({
    path: '/:id/reject',
    public: true,
  }, implementation);
};
