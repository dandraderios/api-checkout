'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');
const paymentClient = require('../../../payments/clients/paymentClient');
const gatewayClient = require('../clients/gatewayClient');

function implementation(req, res, next) {
  gatewayClient
    .approvePayment(req.params.id, (req.body || {}))
    .then((payment) => {
      res.send(HttpStatus.OK, payment);
    }, next);
}

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.post({
    path: '/:id/silent',
  }, implementation);
};
