'use strict';
const app = require('../../../../server');

module.exports = (server, routePrefix) => {
  // Utils Script File
  app.staticFile({
    route: `${routePrefix}/utils.min.js`,
    contentType: 'text/javascript',
    path: `${__dirname}/../templates/utils.es5.js`,
  });

  app.staticFile({
    route: `${routePrefix}/styles.min.css`,
    contentType: 'text/css',
    path: `${__dirname}/../templates/styles.css`,
  });
};
