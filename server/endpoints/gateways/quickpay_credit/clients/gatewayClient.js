'use strict';
const fs = require('fs');
const Handlebars = require('handlebars');
const GatewayClientBase = require('./../../../../utils/GatewayClientBase');

const crypto = require('crypto');
const CONFIG = require('peanut-restify/config');
const paymentClient = require('../../../payments/clients/paymentClient');
const $configurations = require('../../../configurations/clients/$configurations');
const app = require('./../../../../server');

class GatewayClient extends GatewayClientBase {

  /**
   * Get Additional Links for the payment object
   * @param {String} payRoute base url to payment object
   */
  getAdditionalLinks(payRoute) {
    return [];
  }

  /**
   * Retrieve the gateway resume for the specific gateway
   * @param {any} payment Payment Document
   * @param {any} gatewayData Gateway Data, sended in the change state step
   * @returns {GatewayResume} gateway resume
   * @memberof GatewayClient
   */
  getGatewayResume(payment, gatewayData) {
    return new app.models.gatewayResume({
      response: {
        code: parseInt(gatewayData.responseCode),
      },
      transaction: {
        'gateway_id': gatewayData.qpTxnId,
        'type': 'CREDIT',
        'date': (new Date()).toISOString(),
        'currency': gatewayData.amounts.currency,
        'buy_order': payment.transaction.gateway_order,
        'amount': gatewayData.amounts.money,
        'installments_number': (gatewayData.installments || 0),
      },
      'authorizations': {
        'code': gatewayData.authorizationCodes.money,
      },
      'card_number': {
        'pan_last4': gatewayData.panLast4,
        'pan_first6': gatewayData.panFirst6,
      },
    });
  };

  /**
   * Get Approval Dialog
   * @param {String} paymentId Payment Token id
   * @param {*} req Http Request
   */
  getApprovalDialog(paymentId) {
    return new Promise((resolve, reject) => {
      paymentClient
        .getById(paymentId)
        .then((payment) => {
          $configurations
            .getConfiguration(
              payment.application,
              'QUICKPAY_CREDIT'
            ).then((configuration) => {
              var filePath = __dirname + '/../templates/quickpay_credit.html';
              var content = fs.readFileSync(filePath, 'utf8');

              // SOME OVERRIDES
              const metadata = payment.additional_attributes || {};

              var timestamp = new Date().toISOString().replace(/\..+/, '') + 'Z';
              var data = {
                /* eslint-disable camelcase */
                payment_token: payment.id,
                qp_timestamp: timestamp,
                qp_merchTxnId: payment.transaction.gateway_order,
                qp_currency: payment.transaction.amount.currency,
                qp_amount: payment.transaction.amount.total,
                qp_quickframeUrl: CONFIG.get('GATEWAY_QUICKPAY_CREDIT_ENDPOINT'),
                qp_accessToken: 'qw',
                qp_msgType: 'CreditRequest',
                qp_merchantId: configuration.merchantId,
                qp_branchId: (metadata.branch_id || configuration.branchId),
                qp_terminalId: (metadata.terminal_id || configuration.terminalId),
                qp_terminalCode: (metadata.terminal_code || configuration.terminalCode),
                qp_localCode: (metadata.local_code || configuration.localCode),
                qp_commerceCode: (metadata.commerce_code || configuration.commerceCode),
                qp_channel: configuration.channel,
                qp_merchantAuthKey: configuration.merchantAuthKey,
                qp_returnUrl: payment.redirect_urls.return_url,
                qp_cancelUrl: payment.redirect_urls.cancel_url,
                qp_timeout: (app.get('is-dev-mode') ? 15000 : 50000), // DEFAULT TIME TO WAIT UNITL TIME OUT
                /* eslint-enable camelcase */
              };

              // ------------------------------------------
              // Build the String to hash for signature
              var generatedSignature = [];
              generatedSignature.push(data.qp_msgType); // MsgType
              generatedSignature.push(data.qp_merchantId); // Merchant Id
              generatedSignature.push(data.qp_branchId); // Branch Id
              generatedSignature.push(data.qp_terminalId); // Terminal Id
              generatedSignature.push(data.qp_timestamp); // TimeStamp
              generatedSignature.push(data.qp_merchTxnId); // Transaction Id
              generatedSignature.push(data.qp_amount); // price.withDiscount.total
              generatedSignature.push(data.qp_amount); // price.withoutDiscount.total*
              generatedSignature.push(data.qp_currency); // Currency
              generatedSignature.push(data.qp_merchantAuthKey); // Authorization Key
              // ------------------------------------------

              // ------------------------------------------
              // GENERATE THE HASH (SHA256) AND THE RESULT ENCODE TO BASE64 IN ASCII MODE
              var rawSignature = generatedSignature.join('');
              var crypto = require('crypto');
              var hash = crypto.createHash('sha256').update(rawSignature).digest('base64');
              /* eslint-disable camelcase */
              data.qp_signature = hash;
              /* eslint-enable camelcase */
              // ------------------------------------------

              var template = Handlebars.compile(content);
              var html = template(data);
              resolve(html);
            }, (err) => {
              reject(err);
            });
        }, (err) => {
          reject(err);
        });
    });
  };

  /**
   * Approve payment
   * @param {String} paymentId Payment Token
   * @param {*} gatewayData Gateway Payment Data
   */
  approvePayment(paymentId, gatewayData) {
    return paymentClient
      .updateToPaidState(paymentId, gatewayData);
  };

  /**
   * Reject Payment
   * @param {String} paymentId Payment Token
   * @param {*} gatewayData Gateway payment data
   */
  rejectPayment(paymentId, gatewayData) {
    return paymentClient
      .updateToRejectState(paymentId, gatewayData);
  };
}

const client = new GatewayClient();
module.exports = client;
