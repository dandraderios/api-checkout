'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');
const paymentClient = require('../../../payments/clients/paymentClient');
const gatewayClient = require('../clients/gatewayClient');

function implementation(req, res, next) {
  var token = req.params.id;
  var gatewayData = JSON.parse(req.body.response);

  gatewayClient
    .approvePayment(req.params.id, gatewayData)
    .then((payment) => {
      switch (payment.state) {
        case 'paid':
          // ---------------------------------------------------------
          paymentClient
            .createSuccessRedirectionPage(payment, {
              'response_code': 0,
              'response_description': 'paid',
            })
            .then((html) => {
              res.setHeader('content-type', 'text/html');
              res.setHeader('cache-control', 'public, max-age=3600'); // 1hr cache control
              res.send(HttpStatus.OK, html);
              next();
            }, next);
          // ---------------------------------------------------------
          break;
        case 'reject':
        default:
          // ---------------------------------------------------------
          paymentClient
            .createErrorRedirectionPage(payment, {
              'error_code': gatewayData.responseCode,
              'error_message': gatewayData.responseDescription,
            })
            .then((html) => {
              res.setHeader('content-type', 'text/html');
              res.setHeader('cache-control', 'public, max-age=3600'); // 1hr cache control
              res.send(HttpStatus.OK, html);
              next();
            }, next);
          // ---------------------------------------------------------
          break;
      }
    }, next);
}

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.post({
    path: '/:id/execute',
    public: true,
  }, implementation);
};
