'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');
const _ = require('lodash');
const gatewayClient = require('../clients/gatewayClient');

function implementation(req, res, next) {
  gatewayClient
    .getApprovalDialog(req.params.id)
    .then((html) => {
      res.setHeader('content-type', 'text/html');
      res.setHeader('cache-control', 'public, max-age=3600'); // 1hr cache control
      res.send(HttpStatus.OK, html);
      next();
    }, next);
}

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.get({
    path: '/:id/pay',
    public: true,
  }, implementation);
};
