'use strict';
const fs = require('fs');
const Handlebars = require('handlebars');
const GatewayClientBase = require('./../../../../utils/GatewayClientBase');

const crypto = require('crypto');
const CONFIG = require('peanut-restify/config');
const paymentClient = require('../../../payments/clients/paymentClient');
const $configurations = require('../../../configurations/clients/$configurations');
const app = require('./../../../../server');

class GatewayClient extends GatewayClientBase {

  /**
   * Get Additional Links for the payment object
   * @param {String} payRoute base url to payment object
   */
  getAdditionalLinks(payRoute) {
    return [];
  }

  /**
   * Retrieve the gateway resume for the specific gateway
   * @param {any} payment Payment Document
   * @param {any} gatewayData Gateway Data, sended in the change state step
   * @returns {GatewayResume} gateway resume
   * @memberof GatewayClient
   */
  getGatewayResume(payment, gatewayData) {
    return new app.models.gatewayResume({
      response: {
        'code': parseInt(gatewayData.responseCode),
      },
      transaction: {
        'gateway_id': gatewayData.qpTxnId,
        'type': 'DEBIT',
        'date': (new Date()).toISOString(),
        'currency': payment.transaction.amount.currency,
        'buy_order': payment.transaction.gateway_order,
        'amount': payment.transaction.amount.total,
        'installments_number': (gatewayData.installments || 0),
        // In this payment method you pay with a Bank Account,
        // so we don't have any card number
        'card_number': {},
      },
      authorizations: {
        'code': gatewayData.authorizationCode,
      },
    });
  };

  /**
   * Get Approval Dialog
   * @param {String} paymentId Payment Token id
   * @param {*} req Http Request
   */
  getApprovalDialog(paymentId) {
    return new Promise((resolve, reject) => {
      paymentClient
        .getById(paymentId)
        .then((payment) => {
          $configurations
            .getConfiguration(
              payment.application,
              'QUICKPAY_DEBIT'
            ).then((configuration) => {
              var filePath = __dirname + '/../templates/quickpay_debit.html';
              var content = fs.readFileSync(filePath, 'utf8');

              // SOME OVERRIDES
              const metadata = payment.additional_attributes || {};

              var timestamp = new Date().toISOString().replace(/\..+/, '') + 'Z';
              var data = {
                'qp_timestamp': timestamp,
                'qp_merchTxnId': payment.transaction.gateway_order,
                'qp_currency': payment.transaction.amount.currency,
                'qp_amount': payment.transaction.amount.total,
                'qp_quickframeUrl': CONFIG.get('GATEWAY_QUICKPAY_DEBIT_ENDPOINT'),
                'qp_accessToken': 'qw',
                'qp_msgType': 'DebitRequest',
                'qp_merchantId': configuration.merchantId,
                'qp_branchId': (metadata.branch_id || configuration.branchId),
                'qp_terminalId': (metadata.terminal_id || configuration.terminalId),
                'qp_terminalCode': (metadata.terminal_code || configuration.terminalCode),
                'qp_localCode': (metadata.local_code || configuration.localCode),
                'qp_commerceCode': (metadata.commerce_code || configuration.commerceCode),
                'qp_channel': configuration.channel,
                'qp_merchantAuthKey': configuration.merchantAuthKey,
                'qp_returnUrl': payment.redirect_urls.return_url,
                'qp_cancelUrl': payment.redirect_urls.cancel_url,
                'qp_timeout': (app.get('is-dev-mode') ? 15000 : 50000), // DEFAULT TIME TO WAIT UNITL TIME OUT
                payment: payment,
              };

              // ------------------------------------------
              // Build the String to hash for signature
              var generatedSignature = [];
              generatedSignature.push(data.qp_msgType); // MsgType
              generatedSignature.push(data.qp_merchantId); // Merchant Id
              generatedSignature.push(data.qp_branchId); // Branch Id
              generatedSignature.push(data.qp_terminalId); // Terminal Id
              generatedSignature.push(data.qp_timestamp); // TimeStamp
              generatedSignature.push(data.qp_merchTxnId); // Transaction Id
              generatedSignature.push(data.qp_amount); // price.withDiscount.total
              generatedSignature.push(data.qp_amount); // price.withoutDiscount.total*
              generatedSignature.push(data.qp_currency); // Currency
              generatedSignature.push(data.qp_merchantAuthKey); // Authorization Key
              // ------------------------------------------

              // ------------------------------------------
              // GENERATE THE HASH (SHA256) AND THE RESULT ENCODE TO BASE64 IN ASCII MODE
              var rawSignature = generatedSignature.join('');
              var crypto = require('crypto');
              var hash = crypto.createHash('sha256').update(rawSignature).digest('base64');
              /* eslint-disable camelcase */
              data.qp_signature = hash;
              /* eslint-enable camelcase */
              // ------------------------------------------

              var template = Handlebars.compile(content);
              var html = template(data);
              resolve(html);
            }, (err) => {
              reject(err);
            });
        }, (err) => {
          reject(err);
        });
    });
  };

  /**
   * Approve payment
   * @param {String} paymentId Payment Token
   * @param {*} gatewayData Gateway Payment Data
   */
  approvePayment(paymentId, gatewayData) {
    return paymentClient
      .updateToPaidState(paymentId, gatewayData);
  };

  /**
   * Reject Payment
   * @param {String} paymentId Payment Token
   * @param {*} gatewayData Gateway payment data
   */
  rejectPayment(paymentId, gatewayData) {
    return paymentClient
      .updateToRejectState(paymentId, gatewayData);
  };
}

const client = new GatewayClient();
module.exports = client;
