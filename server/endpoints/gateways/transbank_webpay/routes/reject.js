'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');
const errors = require('./../../../../utils/errors');
const paymentClient = require('../../../payments/clients/paymentClient');
const gatewayClient = require('../clients/gatewayClient');

function implementation(req, res, next) {
  var token = req.params.id;
  var gatewayData = req.body;
  gatewayData.message = 'timeout/anuled error';

  paymentClient
    .getById(token)
    .then((payment) => {
      switch (payment.state) {
        case 'created':
          gatewayClient
            .rejectPayment(token, gatewayData)
            .then((payment) => {
              // ---------------------------------------------------------
              paymentClient
                .createErrorRedirectionPage(payment, {
                  'error_code': -1,
                  'error_message': gatewayData.message,
                })
                .then((html) => {
                  res.setHeader('content-type', 'text/html');
                  res.setHeader('cache-control', 'public, max-age=3600'); // 1hr cache control
                  res.send(HttpStatus.OK, html);
                  next();
                }, next);
              // ---------------------------------------------------------
            });
          break;
        default:
          // Log error
          new errors.PeinauError(
            'TRANSBANK_WEIRD_TRANSITION',
            'payment has indicated to be rejected but the payment document state in db has a weird state',
            {
              'document_id': payment.id,
              'payment_method': payment.payer.payment_method,
              'document_state': payment.state,
              'new_state': 'rejected',
            }
          );
          // ---------------------------------------------------------
          paymentClient
            .createErrorRedirectionPage(payment, {
              'error_code': -1,
              'error_message': 'rejected',
            })
            .then((html) => {
              res.setHeader('content-type', 'text/html');
              res.setHeader('cache-control', 'public, max-age=3600'); // 1hr cache control
              res.send(HttpStatus.OK, html);
              next();
            }, next);
          // ---------------------------------------------------------
          break;
      }
    }, next);
}

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.post({
    path: '/:id/reject',
    public: true,
  }, implementation);
};
