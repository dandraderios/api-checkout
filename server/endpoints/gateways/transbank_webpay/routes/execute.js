'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');

const paymentClient = require('../../../payments/clients/paymentClient');
const gatewayClient = require('../clients/gatewayClient');
const errors = require('./../../../../utils/errors');

function implementation(req, res, next) {
  if (!req.body || (req.body && !req.body.token_ws)) {
    return next(new errors.PeinauError(
      'TOKEN_WS_IS_REQUIRED',
      'token_ws for transbank is required', {
        'invalid_body': req.body,
      }
    ));
  }

  gatewayClient
    .processPayment(req.params.id, req.body.token_ws)
    .then((payment) => {
      switch (payment.state) {
        case 'paid':
          // ---------------------------------------------------------
          // Redireccionar al voucher
          var baseUrl = payment.gateway.urlRedirection;
          var content = require('fs').readFileSync(`${__dirname}/../templates/voucher.html`, 'utf8');
          var template = require('handlebars').compile(content);
          var html = template(payment.gateway);
          res.setHeader('content-type', 'text/html');
          res.setHeader('cache-control', 'public, max-age=3600'); // 1hr cache control
          res.send(HttpStatus.OK, html);
          next();
          // ---------------------------------------------------------
          break;
        case 'rejected':
          // ---------------------------------------------------------
          paymentClient
            .createErrorRedirectionPage(payment, {
              'error_code': -1,
              'error_message': 'rejected',
            })
            .then((html) => {
              res.setHeader('content-type', 'text/html');
              res.setHeader('cache-control', 'public, max-age=3600'); // 1hr cache control
              res.send(HttpStatus.OK, html);
              next();
            }, next);
          // ---------------------------------------------------------
          break;
        default:
          next(new errors.PeinauError(
            'INVALID_PAYMENT_STATE',
            'the document has a invalid payment state for process the redirection page', {
              'invalid_state': payment.state,
              'payment_method': payment.payer.payment_method,
              'document_id': payment.id,
            }));
      }
    }, next);
}

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.post({
    path: '/:id/execute',
    public: true,
  }, implementation);
};
