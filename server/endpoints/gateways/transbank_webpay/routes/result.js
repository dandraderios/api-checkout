'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');

const paymentClient = require('../../../payments/clients/paymentClient');

function implementation(req, res, next) {
  paymentClient
    .getById(req.params.id)
    .then((payment) => {
      switch (payment.state) {
        case 'paid':
          // ---------------------------------------------------------
          paymentClient
            .createSuccessRedirectionPage(payment, {
              'response_code': 0,
              'response_description': 'paid',
            })
            .then((html) => {
              res.setHeader('content-type', 'text/html');
              res.setHeader('cache-control', 'public, max-age=3600'); // 1hr cache control
              res.send(HttpStatus.OK, html);
              next();
            }, next);
          // ---------------------------------------------------------
          break;
        default:
          // ---------------------------------------------------------
          paymentClient
            .createErrorRedirectionPage(payment, {
              'error_code': -1,
              'error_message': 'rejected',
            })
            .then((html) => {
              res.setHeader('content-type', 'text/html');
              res.setHeader('cache-control', 'public, max-age=3600'); // 1hr cache control
              res.send(HttpStatus.OK, html);
              next();
            }, next);
          // ---------------------------------------------------------
          break;
      }
    }, next);
}

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.post({
    path: '/:id/result',
    public: true,
  }, implementation);
};
