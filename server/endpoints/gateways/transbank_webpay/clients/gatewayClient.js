'use strict';
const fs = require('fs');
const Handlebars = require('handlebars');
const transbank = require('transbank');

const GatewayClientBase = require('./../../../../utils/GatewayClientBase');
const CONFIG = require('peanut-restify/config');
const paymentClient = require('../../../payments/clients/paymentClient');
const $configurations = require('../../../configurations/clients/$configurations');
const app = require('./../../../../server');

class GatewayClient extends GatewayClientBase {

  /**
   * Get Additional Links for the payment object
   * @param {String} payRoute base url to payment object
   */
  getAdditionalLinks(payRoute) {
    return [];
  }

  /**
   * Retrieve the gateway resume for the specific gateway
   * @param {any} payment Payment Document
   * @param {any} gatewayData Gateway Data, sended in the change state step
   * @returns {GatewayResume} gateway resume
   * @memberof GatewayClient
   */
  getGatewayResume(payment, gatewayData) {
    const transbankDetails = gatewayData.detailOutput[0];
    return new app.models.gatewayResume({
      'response': {
        'code': parseInt(transbankDetails.responseCode),
      },
      'transaction': {
        'gateway_id': gatewayData.sessionId,
        'type': (transbankDetails.paymentTypeCode == 'VD' ? 'DEBIT' : 'CREDIT'),
        'date': (new Date()).toISOString(),
        'currency': 'CLP',
        'buy_order': transbankDetails.buyOrder,
        'amount': transbankDetails.amount,
        'installments_number': (transbankDetails.sharesNumber || 0),
      },
      'authorizations': {
        'code': transbankDetails.authorizationCode,
      },
      'card_number': {
        'pan_last4': gatewayData.cardDetail.cardNumber,
      },
    });
  };

  /**
   * Get Approval Dialog
   * @param {String} paymentId Payment Token id
   * @param {*} req Http Request
   */
  getApprovalDialog(paymentId, req) {
    return new Promise((resolve, reject) => {
      paymentClient
        .getById(paymentId)
        .then((payment) => {
          $configurations
            .getConfiguration(payment.application, 'TRANSBANK_WEBPAY')
            .then((configuration) => {
              var baseUrl = `${paymentClient.getBaseUrl(req)}/gateways/transbank/webpay/${payment.id}`;
              var content = fs.readFileSync(`${__dirname}/../templates/approval_dialog.html`, 'utf8');

              var tbkConfig = this.resolveTBKConfiguration(configuration);

              transbank
                .createClient(tbkConfig)
                .then((TBKClient) => {
                  TBKClient.initTransaction(
                    payment.transaction.amount.total,
                    payment.transaction.gateway_order,
                    payment.id,
                    `${baseUrl}/execute`,
                    `${baseUrl}/result`,
                    (error, result) => {
                      if (error) {
                        return reject(error);
                      }

                      var template = Handlebars.compile(content);
                      var html = template(result.return);
                      resolve(html);
                    });
                }, reject);
            }, reject);
        }, reject);
    });
  };

  /**
   * Approve payment
   * @param {String} paymentId Payment Token
   * @param {*} gatewayData Gateway Payment Data
   */
  processPayment(paymentId, tbkToken) {
    return new Promise((resolve, reject) => {
      paymentClient
        .getById(paymentId)
        .then((payment) => {
          $configurations
            .getConfiguration(payment.application, 'TRANSBANK_WEBPAY')
            .then((configuration) => {
              var tbkConfig = this.resolveTBKConfiguration(configuration);
              transbank
                .createClient(tbkConfig)
                .then((TBKClient) => {
                  try {
                    TBKClient
                      .getTransactionResult(tbkToken, (error, tbkResult) => {
                        if (error) {
                          return paymentClient
                            .updateToRejectState(paymentId, {
                              'failure_step': 'transaction_result',
                              'transaction_token': tbkToken,
                              error: error,
                            })
                            .then(resolve, reject);
                        }

                        var gatewayData = tbkResult.return;
                        TBKClient
                          .acknowledgeTransaction(tbkToken, (error, result) => {
                            if (error) {
                              return paymentClient
                                .updateToRejectState(paymentId, {
                                  'failure_step': 'acknowledge',
                                  'transaction_token': tbkToken,
                                  error: error,
                                })
                                .then(resolve, reject);
                            }

                            gatewayData['transaction_token'] = tbkToken;
                            // THE ONLY WAY TO 'KNOW' IF THE PAYMENT IS rejected,
                            // IS CHECKING THE URLREDIRECTION, AND IF THE URL ENDWITH 'VOUCHER'
                            // MEANS IT'S OK!.
                            if (gatewayData.urlRedirection.indexOf('voucher.cgi') > 0) {
                              return paymentClient
                                .updateToPaidState(paymentId, gatewayData)
                                .then(resolve, reject);
                            } else {
                              return paymentClient
                                .updateToRejectState(paymentId, gatewayData)
                                .then(resolve, reject);
                            }
                          });
                      });
                  } catch (ex) {
                    reject(ex);
                  }
                }, reject);
            }, reject);
        }, reject);
    });
  };

  /**
   * Reject Payment
   * @param {String} paymentId Payment Token
   * @param {*} gatewayData Gateway payment data
   */
  rejectPayment(paymentId, gatewayData) {
    return new Promise((resolve, reject) => {
      // TODO: Rethink this part....
      // Transbank call twice , if in the acknowledge fails, so if we change the state,
      // check if the error is because, a re-transition to the same state
      return paymentClient
        .updateToRejectState(paymentId, gatewayData)
        .then(resolve, (err) => {
          // by pass if the error is for re-transition!
          if (err &&
            err.error_code == 'TRANSITION_TO_NEW_STATE_IS_NOT_FOUND' &&
            err.meta_data.document_state == 'rejected' &&
            err.meta_data.new_state == 'rejected') {
            return resolve(gatewayData);
          }

          reject(err);
        });
    });
  };

  resolveTBKConfiguration(configuration) {
    let isProductionMode = CONFIG.get('GATEWAY_TRANSBANK_WEBPAY_ENVIRONMENT') == 'PRODUCCION';
    var cfg = {};

    // PRODUCTION MODE??
    if (isProductionMode) {
      // SET UI CONFIGURATED KEYS
      cfg.WEBPAY_KEY = `${__dirname}/../../../../config/certs/webpay_prod_cert.pem`;
    } else {
      // INTEGRATION DEFAULT CONFIGURATION
      cfg = Object.assign({}, transbank.config);
      cfg.DEBUG_LOGS = true;
    }

    cfg.COMMERCE_CODE = configuration.commerceCode;
    cfg.PRIVATE_KEY = configuration.PRIVATE_KEY;
    cfg.PUBLIC_KEY = configuration.PUBLIC_KEY;
    cfg.WSDL_PATH = CONFIG.get('GATEWAY_TRANSBANK_WEBPAY_ENDPOINT');

    return cfg;
  };
}

const client = new GatewayClient();
module.exports = client;
