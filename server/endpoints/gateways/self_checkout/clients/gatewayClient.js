'use strict';
const fs = require('fs');
const Handlebars = require('handlebars');
const request = require('request');
const lodash = require('lodash');
const CONFIG = require('peanut-restify/config');
const logger = require('peanut-restify/logger');
const app = require('./../../../../server');
const paymentClient = require('../../../payments/clients/paymentClient');
const $configurations = require('../../../configurations/clients/$configurations');
const GatewayClientBase = require('./../../../../utils/GatewayClientBase');
const AEScypher = require('./../third_party/AESCypher');

class GatewayClient extends GatewayClientBase {

  /**
   * Get Additional Links for the payment object
   * @param {String} payRoute base url to payment object
   */
  getAdditionalLinks(payRoute) {
    return [];
  }

  /**
   * Retrieve the gateway resume for the specific gateway
   * @param {any} payment Payment Document
   * @param {any} gatewayData Gateway Data, sended in the change state step
   * @returns {GatewayResume} gateway resume
   * @memberof GatewayClient
   */
  getGatewayResume(payment, gatewayData) {
    return new app.models.gatewayResume({
      response: {
        code: 0,
      },
      transaction: {
        'gateway_id': payment.id,
        'type': 'CREDIT',
        'date': (new Date()).toISOString(),
        'currency': 'CLP',
        'buy_order': payment.transaction.gateway_order,
        'amount': payment.transaction.amount.total,
        'installments_number': 0,
      },
      authorizations: {
        code: 1234,
      },
      'card_number': {
        'pan_first6': '500234',
        'pan_last4': '5672',
      },
    });
  }

  /**
   * Get Approval Dialog
   * @param {String} paymentId Payment Token id
   * @param {*} req Http Request
   */
  getApprovalDialog(paymentId, req) {
    return new Promise((resolve, reject) => {
      paymentClient
        .getById(paymentId)
        .then((payment) => {
          $configurations
            .getConfiguration(
              payment.application,
              'SELF_CHECKOUT'
            ).then((configuration) => {
              // Get some values
              const rut = payment.payer.payer_info.document_number;
              const run = rut.substring(0, rut.length - 1);
              const dv = rut.substring(rut.length - 1);

              const secretKey = configuration.secret_shared_key;
              const encryptedText = AEScypher.encrypt(secretKey, JSON.stringify({
                rut: run,
                dv: dv,
              }));

              // Get the card list
              request.post({
                url: CONFIG.get('GATEWAY_SELF_CHECKOUT_LIST_CARD_ENDPOINT'),
                json: true,
                body: {
                  data: encryptedText,
                },
                headers: {
                  'Content-Type': 'application/json',
                  'Api-Request-Channel': configuration.request_channel,
                  'Api-Merchant-Id': configuration.merchant_id,
                  'Api-Merchant-Channel': configuration.merchant_channel,
                  'Api-Merchant-Branch-Id': configuration.branch_id,
                  'Api-Merchant-Terminal-Id': configuration.terminal_id,
                  // doble encryption ¯\_(ツ)_/¯
                  'Authorization': AEScypher.encrypt(secretKey, encryptedText),
                },
              }, (error, response, body) => {
                if (error) {
                  logger.error('An error has ocurred in the self checkout list cards dialog payment');
                  logger.debug(error);
                  return reject(error);
                }

                // Has Errors?
                if (body.codigo !== 0) {
                  const error = new Error('BAD_RESPONSE');
                  error.type = error.message;
                  error.errors = body;

                  logger.error('And error has ocurred in the response payload for list card dialog payment');
                  logger.debug(error);
                  return reject(error);
                }

                // Resolve HTML
                let templateUI = 'default';
                if (payment.additional_attributes && payment.additional_attributes.UI) {
                  templateUI = payment.additional_attributes.UI.toLowerCase();
                }
                const filePath = `${__dirname}/../templates/${templateUI}/list_cards.hbs`;
                let content = 'TEMPLATE UI NOT FOUND';
                if (fs.existsSync(filePath)) {
                  content = fs.readFileSync(filePath, 'utf-8');
                }

                var timestamp = new Date().toISOString().replace(/\..+/, '') + 'Z';
                var data = {
                  'payment_token': payment.id,
                  'qp_timestamp': timestamp,
                  'qp_merchTxnId': payment.transaction.gateway_order,
                  'qp_currency': payment.transaction.amount.currency,
                  'qp_amount': payment.transaction.amount.total,
                  'qp_returnUrl': payment.redirect_urls.return_url,
                  'qp_cancelUrl': payment.redirect_urls.cancel_url,
                  'routePrefix': paymentClient.getBaseUrl(req),
                  // Only the active card (estadoTarjeta===5)
                  'cards': lodash.map(lodash.filter(body.tarjetas,
                    (c) => c.estadoTarjeta == 5),
                    (card) => {
                      // Get the card type
                      const type = ((type) => {
                        const lowerType = type.toLowerCase();
                        switch (lowerType) {
                          default: return lowerType;
                        };
                      })(card.tipoTarjeta);

                      return {
                        'type': type,
                        'card_holder': payment.payer.payer_info.full_name.toUpperCase(),
                        'first_segment': card.panFirst6.substring(0, 4),
                        'second_segment': card.panFirst6.substring(4),
                        'pan_first_6': card.panFirst6,
                        'pan_last_4': card.panLast4,
                        'card_id': card.qpCardId,
                        'contract_id': card.nroContrato,
                      };
                    }),
                };

                var template = Handlebars.compile(content);
                var html = template(data);
                resolve(html);
              });
            }, reject);
        }, reject);
    });
  };

  /**
   * Approve payment
   * @param {String} paymentId Payment Token
   * @param {*} gatewayData Gateway Payment Data
   */
  approvePayment(paymentId, gatewayData) {
    // Add some bussines logic to gateway data??
    return paymentClient.updateToPaidState(paymentId, gatewayData);
  };

  /**
   * Reject Payment
   * @param {String} paymentId Payment Token
   * @param {*} gatewayData Gateway payment data
   */
  rejectPayment(paymentId, gatewayData) {
    // Add some bussines logic to gateway data??
    return paymentClient.updateToRejectState(paymentId, gatewayData);
  };
}

const client = new GatewayClient();
module.exports = client;
