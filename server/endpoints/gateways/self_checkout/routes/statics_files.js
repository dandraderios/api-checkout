'use strict';
const app = require('../../../../server');

module.exports = (server, routePrefix) => {
  // Embedded UI style
  app.staticFile({
    route: `${routePrefix}/default/list_cards.min.css`,
    contentType: 'text/css',
    path: `${__dirname}/../templates/default/list_cards.min.css`,
  });

  // Embedded UI style
  app.staticFile({
    route: `${routePrefix}/copec/list_cards.min.css`,
    contentType: 'text/css',
    path: `${__dirname}/../templates/copec/list_cards.min.css`,
  });
};

