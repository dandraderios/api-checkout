'use strict';
const crypto = require('crypto');
const algorithm = 'aes-128-cbc';
const iv = new Buffer(16);
iv.fill(0);

class AESCypher {

  /**
   * Decrypt text
   * @param {String} password Shared Key
   * @param {String} encryptdata Encrypted Data
   */
  decrypt(password, encryptdata) {
    const hash = crypto.createHash('sha1').update(password, 'utf8').digest();
    const keyBuffer = new Buffer(hash.slice(0, 16));
    const textToDecrypt = new Buffer(encryptdata, 'base64').toString('binary');

    const decipher = crypto.createDecipheriv(algorithm, keyBuffer, iv);
    let decoded = decipher.update(textToDecrypt, 'binary', 'utf8');
    decoded += decipher.final('utf8');

    return decoded;
  }

  /**
   * Encrypt Text
   * @param {String} password Shared Key
   * @param {String} text Texto to encrypt
   */
  encrypt(password, text) {
    const hash = crypto.createHash('sha1').update(password, 'utf8').digest();
    const keyBuffer = new Buffer(hash.slice(0, 16));

    const encipher = crypto.createCipheriv(algorithm, keyBuffer, iv);
    let encryptdata = encipher.update(text, 'utf8', 'binary');
    encryptdata += encipher.final('binary');

    return new Buffer(encryptdata, 'binary').toString('base64');
  }
};

const cypher = new AESCypher();
module.exports = cypher;
