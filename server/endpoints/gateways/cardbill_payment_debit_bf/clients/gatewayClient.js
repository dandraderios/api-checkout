'use strict';
const GatewayClientBase = require('./../../../../utils/GatewayClientBase');
const fs = require('fs');
const handlebars = require('handlebars');
const request = require('request');
const lodash = require('lodash');

const CONFIG = require('peanut-restify/config');
const expr = require('peanut-restify/expressions');
const logger = require('peanut-restify/logger');
const paymentClient = require('../../../payments/clients/paymentClient');
const $configurations = require('../../../configurations/clients/$configurations');
const app = require('../../../../server');
const errors = require('../../../../utils/errors');

class GatewayClient extends GatewayClientBase {

  /**
   * Validate Model Against his legacy validation
   * @param {*} data Payment Model Data
   * @returns false if the model is Valid or an Error otherwise
   */
  isInvalid(data) {
    const model = new app.models.cardBillPaymentDebit(data);

    if (model.isInvalid()) {
      return model.getValidationErrors();
    }

    return false;
  };

  /**
   * Retrieve the gateway resume for the specific gateway
   * @param {any} payment Payment Document
   * @param {any} gatewayData Gateway Data, sended in the change state step
   * @returns {GatewayResume} gateway resume
   * @memberof GatewayClient
   */
  getGatewayResume(payment, gatewayData) {
    const billPaymentDetails = lodash.find(gatewayData.paymentTransactionDetails, (details) => {
      return details.type === 'CardBillPayment';
    });

    return new app.models.gatewayResume({
      'response': {
        'code': 0,
      },
      'transaction': {
        'gateway_id': gatewayData.uuid,
        'type': 'DEBIT',
        'date': (new Date()).toISOString(),
        'currency': 'CLP',
        'buy_order': payment.transaction.gateway_order,
        'amount': billPaymentDetails.amount,
        'installments_number': 0,
      },
      'authorizations': {
        'code': billPaymentDetails.authorizationCode,
      },
      'card_number': {
        'pan_last4': billPaymentDetails.cardLast4,
        'pan_first6': billPaymentDetails.cardFirst6,
      },
    });
  };

  /**
   * Get Approval Dialog
   * @param {String} paymentId Payment Token id
   * @param {*} req Http Request
   */
  getApprovalDialog(paymentId) {
    return new Promise((resolve, reject) => {
      paymentClient
        .getById(paymentId)
        .then((payment) => {
          $configurations
            .getConfiguration(payment.application, 'CARDBILL_PAYMENT_DEBIT_BF')
            .then((configuration) => {
              // Payment Data
              const timestamp = new Date().toISOString().replace(/\..+/, '') + 'Z';
              const payload = {
                signingMethod: 'jwt',
                payload: {
                  'msgType': 'CardBillPaymentRequest',
                  'cardInformation': {
                    'cardBin': payment.additional_attributes.card_bin,
                    'lastFourDigits': payment.additional_attributes.last_four_digits,
                  },
                  'merchant': {
                    'merchantId': configuration.merchantId,
                    'commerceCode': configuration.commerceCodeDebit,
                    'branchId': configuration.branchId,
                    'terminalId': configuration.terminalId,
                    'channel': configuration.channel,
                    'timestamp': timestamp,
                    'merchTxnId': payment.transaction.gateway_order,
                  },
                  'merchantCardBill': {
                    'merchantId': configuration.merchantId,
                    'commerceCode': configuration.commerceCodeCmr,
                    'branchId': configuration.branchId,
                    'terminalId': configuration.terminalId,
                    'channel': configuration.channel,
                    'timestamp': timestamp,
                    'merchTxnId': `${payment.transaction.gateway_order}-2`,
                  },
                  'user': [
                    {
                      'userType': 'cardbiller',
                      'documentCountry': payment.payer.payer_info.country,
                      'documentType': payment.payer.payer_info.document_type,
                      'documentId': payment.payer.payer_info.document_number,
                      'qpUserId': '',
                      'userEmailAddress': payment.payer.payer_info.email,
                      'userPhoneNumber': '',
                      'userName': payment.payer.payer_info.full_name,
                      'userSurname': payment.payer.payer_info.full_name,
                    },
                  ],
                  'purchaseOrder': {
                    'purchaseOrderId': payment.transaction.gateway_order,
                    'purchaseOrderDate': timestamp,
                    'currency': payment.transaction.amount.currency,
                  },
                  'price': {
                    'withDiscount': {
                      'amount': payment.transaction.amount.total,
                      'tax': 0,
                      'total': payment.transaction.amount.total,
                    },
                    'withoutDiscount': {
                      'amount': payment.transaction.amount.total,
                      'tax': 0,
                      'total': payment.transaction.amount.total,
                    },
                  },
                },
              };

              // ------------------------------------------
              // Build the String to hash for signature
              var generatedSignature = [];
              generatedSignature.push(JSON.stringify(payload)); // MsgType
              generatedSignature.push(configuration.merchantAuthKey); // Authorization Key
              // ------------------------------------------

              // ------------------------------------------
              // GENERATE THE HASH (SHA256) AND THE RESULT ENCODE TO BASE64 IN ASCII MODE
              var rawSignature = generatedSignature.join('');
              var sha256 = require('sha256');
              var hash = sha256(rawSignature);
              // ------------------------------------------

              // Create the payment
              request.post({
                url: CONFIG.get('GATEWAY_TRANSBANK_CARDBILL_PAYMENT_DEBIT_BF_CREATE_ENDPOINT'),
                json: true,
                body: payload,
                headers: {
                  'Content-Type': 'application/json',
                  'Cache-Control': 'no-cache',
                  'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcmltYXJ5c2lkIjoiYWNiMzYxY2MtZTRlNy0yNGZiLWQ0ZTItMTdiYjNhYTc0MDY2IiwidW5pcXVlX25hbWUiOiJQZWFudXQgSHViIEx0ZGEiLCJncm91cHNpZCI6IkFQUEwiLCJpc3MiOiJGYWxhYmVsbGEiLCJhdWQiOiJXZWIiLCJoYXNoIjoiQ0VCQzVGRjEzMjM5QTA2MzVFMTE1RTA2NTg5NjUwOTkiLCJpYXQiOjE0OTk3OTk1MDUsImV4cCI6Mjk5OTY4NTQxMH0.nBJPHOOS5UV9X8yBPt7yGfvB4wpFlyjIAcB2va0S8mYvP6MBc42hNB3fg2gaKF-YrPl_bvv2BPFlYqf1XJKmpr359Zt9IDBY2pr6fKBCVuSGxY4zVPaiL8Wn1mDbQfaIspGsuzudiMq4v0f75whTDzi2r17I9LgIOdQ8zU00QkNXNoynScwv5WI-Ut7NIc1zgUYI7omPzpZB_iXt36dNPnW5nlKZ6bYCOmapXMzRQVE98_R05DD70WtQrF4sdcrHJT6Z1WShRSt4DiMpUXkCoEM3DAylQ9wNreJFjpkOEquLYSe6ShgxUv-xrd5wffTZVAUJH_2Vj3q57tlahEbn2A',
                  'X-Signature': hash,
                },
              }, (error, response, body) => {
                if (error) {
                  logger.error('An error has ocurred in the card bill get dialog payment');
                  logger.debug(error);
                  return reject(error);
                }

                // Pass only if the operation response is created
                if (response.statusCode !== 200) {
                  const creationError = new Error('CARDBILL_PAYMENT_DEBIT_BF_CREATE_INTENTION_ERROR');
                  creationError.description = 'Can\'t create cardbill payment debit bf operation';
                  logger.error(creationError.description);
                  logger.debug(JSON.stringify(response));
                  return reject(creationError);
                }

                if (body == null || body.links == null || body.links.length == 0 || body.secureToken == null || body.logTrackId == null || body.uuid == null || body.links[0].href == null || body.links[0].href.length == 0) {
                  const creationError = new Error('INVALID_BODY_RESPONSE');
                  creationError.description = 'Can\'t create cardbill payment debit bf operation';
                  logger.error(creationError.description);
                  logger.debug(JSON.stringify(response));
                  return reject(creationError);
                }

                // Get template
                var content = fs.readFileSync(`${__dirname}/../templates/approval_dialog.hbs`, 'utf8');
                var template = handlebars.compile(content);

                const link = lodash.first(lodash.filter(body.links, {
                  rel: 'initiate',
                }));

                // Set some variables to the html
                var html = template({
                  iframeUrl: link.href,
                  secureToken: body.secureToken,
                  logTrackId: body.logTrackId,
                  transactionId: body.uuid,
                  qpTimeout: 7000,
                });
                resolve(html);
              }, (err) => {
                logger.error('Can\'t get configuration for CARDBILL_PAYMENT_DEBIT_BF');
                logger.debug(err);
                reject(err);
              });
            }, (err) => {
              logger.error('Can\'t get payment details for create approval dialog in CARDBILL_PAYMENT_DEBIT_BF');
              logger.debug(err);
              reject(err);
            });
        }, (err) => {
          logger.error('Can\'t generate approval dialog for CARDBILL_PAYMENT_DEBIT_BF');
          logger.debug(err);
          reject(err);
        });
    });
  };

  /**
   * Approve payment
   * @param {String} paymentId Payment Token
   * @param {*} gatewayData Gateway Payment Data
   */
  approvePayment(paymentId, gatewayData) {
    return new Promise((resolve, reject) => {
      try {
        // Create the payment
        const statusEndpoint = CONFIG.get('GATEWAY_TRANSBANK_CARDBILL_PAYMENT_DEBIT_BF_STATUS_ENDPOINT');
        request.get({
          url: `${statusEndpoint}/${gatewayData.transactionId}`,
          json: true,
          headers: {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
            'apikey': CONFIG.get('GATEWAY_TRANSBANK_CARDBILL_PAYMENT_DEBIT_BF_API_TOKEN'),
          },
        }, (error, response, body) => {
          if (error) {
            logger.error('An error has ocurred in the card bill approve payment');
            logger.debug(error);
            return reject(error);
          }

          // Execute, but the endpoints status say NO!!!??, cancel payment
          if (body.status !== 'DONE') {
            return this
              .rejectPayment(paymentId, body)
              .then(resolve, reject);
          }

          paymentClient
            .updateToPaidState(paymentId, {
              postMessage: gatewayData,
              status: body,
            })
            .then(resolve, reject);
        });
      } catch (ex) {
        reject(new errors.UnknowError(
          'An error has ocurred trying to get status for a payment in credit card bill payment',
          ex
        ));
      }
    });
  };

  /**
   * Reject Payment
   * @param {String} paymentId Payment Token
   * @param {*} gatewayData Gateway payment data
   */
  rejectPayment(paymentId, cancelData) {
    return paymentClient
      .updateToRejectState(paymentId, cancelData);
  };
}

const client = new GatewayClient();
module.exports = client;
