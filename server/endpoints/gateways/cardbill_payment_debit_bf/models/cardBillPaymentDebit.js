'use strict';
const mongoose = require('mongoose');
const validators = require('mongoose-validators');
/* eslint camelcase: ["error", {properties: "never"}] */
module.exports = new mongoose.Schema({
  card_bin: {
    type: String,
    required: true,
    minlength: 6,
    maxlength: 10,
  },
  last_four_digits: {
    type: String,
    required: true,
    maxlength: 4,
    minlength: 4,
  },
});
