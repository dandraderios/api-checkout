import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * Useful class for dynamically changing the classpath, adding classes during runtime. 
 */
public class interop {
    /**
     * Parameters of the method to add an URL to the System classes. 
     */
    private static final Class<?>[] parameters = new Class[]{URL.class};

    /**
     * Adds a file to the classpath.
     * @param s a String pointing to the file
     * @throws IOException
     */
    public static void addFile(String s) throws IOException {
        File f = new File(s);
        addFile(f);
    }

    /**
     * Adds a file to the classpath
     * @param f the file to be added
     * @throws IOException
     */
    public static void addFile(File f) throws IOException {
        addURL(f.toURI().toURL());
    }

    /**
     * Adds the content pointed by the URL to the classpath.
     * @param u the URL pointing to the content to be added
     * @throws IOException
     */
    public static void addURL(URL u) throws IOException {
        URLClassLoader sysloader = (URLClassLoader)ClassLoader.getSystemClassLoader();
        Class<?> sysclass = URLClassLoader.class;
        try {
            Method method = sysclass.getDeclaredMethod("addURL",parameters);
            method.setAccessible(true);
            method.invoke(sysloader,new Object[]{ u }); 
        } catch (Throwable t) {
            t.printStackTrace();
            throw new IOException("Error, could not add URL to system classloader");
        }        
    }

    public static void main(String args[]) throws IOException, SecurityException, ClassNotFoundException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException{
        String basePathToLoad = args[0];
        String methodToExecute = args[1];
        String textToEncrypt = args[2];

        addFile(basePathToLoad + "/CMRCipher.jar");
        Class<?> cs = ClassLoader.getSystemClassLoader().loadClass("cl.falabella.cmrwebpay.seguridad.CMRWPCipher");

        Method initMethod = cs.getDeclaredMethod("init",new Class[]{String.class});
        initMethod.invoke(cs, new Object[]{ basePathToLoad + "/" }); 

        Method encryptMethod = cs.getDeclaredMethod(methodToExecute,new Class[]{String.class});
        String result = (String)encryptMethod.invoke(cs, new Object[]{ textToEncrypt }); 
        System.out.println(result);
    }
}
// java interop "$(pwd)" "encrypt" "<Message><MessageId><Code>PAGCMR</Code><MsgDesc>Pago con CMR</MsgDesc><Version>1.10</Version><FromAddress>FalabellaPro</FromAddress><ToAddress>https://www.segurosfalabella.cl/web/seguros/pago?r=cmrpuntos</ToAddress><UrlWSInfoPago>http://www.segurosfalabella.com/wsmfppr/services/WS_INFO_PAGOSoap</UrlWSInfoPago><CodCom>10001553</CodCom><NumSec>55451440</NumSec><Date>06-02-2017</Date><Time>13:28:28</Time><Rut></Rut><FamProd>0002</FamProd></MessageId><SolicitudPago><Monto></Monto><CodMon>152</CodMon><TipoDoc>1</TipoDoc><Puntos>5000</Puntos><EAN>000000000009990086</EAN></SolicitudPago></Message>"
// java interop "$(pwd)" "decrypt" "N3ltEQmSY/C0qFiddlkrpug2ZNP6j2KeejnWHystrRkL0ALlNSTPcx/20AKrDq6YYfWkDYXpRCfG26qvVaUCHFJP/NfwOocaVWyhdqHDk77sn80OVpEVmS7o8rdwH1IAyIFropPwDqtSaFJiLxyRUntsfeCUpZ+I/dRfZTRbnvv42SuZhf835tG5cFv60yMewbf/ab3JI3fubrXJqj753uEqGKuXrvB5EyRjN1j2ccmDqRS+l4iTPuJ4x46QbR3x9qOcgB2YmmJ/kM0nsuLj6NRRy1Lg1tAi2gR+aoDZf3naS6yBBB/GfzsOgdpyn4zRZ+AaXgcTMcXdC+1JghCAc5Ui58921xHfM+1pnAmvmmtvoQm+kJuyrPjyiIVs1ifM+PCT/CnoiWZFa1wgORPiOtDaZIOGyryyEqYAfrOOtHRajElR5UirQbAbW8Df3N6lnmwPazCzG2Kw3598LcoQwH4l3auyjhgmQRZIHsaKIZwgajTItJX/ckS5jUPQwuY8PDWFu+xDWobQI2IaywvT4SJbb5WEOejsXCGPIihPMhSlxmTlfsnNoXo6wa/SfH/u4oLsGFe2I1TfYAo4JqbO8YGSDwGR/8nopYXFDvXEMB3cCHLf3jW43FgYNZq1gEXSSgOR8yyXDkLReAsJsj9GIldwqsveES+6umPb3NwR0rqYWOA/OChv1cf6I8if2FXCf6/MHrmAIQNpHUv0nS9Y4WaU7ytdq9mDBINsPRfZLMvEPdrxf+PeSnNvaxnKNVKW/UZ0K+OAXxtt8pSll3zYDUBHnloOAqIM"