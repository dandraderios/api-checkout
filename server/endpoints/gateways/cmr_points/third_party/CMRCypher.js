'use strict';
const exec = require('child_process').execSync;
const path = require('path');
class CMRCypher {

  __findJavaPath() {
    return new Promise((resolve, reject) => {
      require('find-java-home')(function (err, home) {
        if (err) {
          return reject(err);
        }

        resolve(path.join(home, 'bin', 'java'));
      });
    });
  }

  encrypt(textToEncrypt) {
    return new Promise((resolve, reject) => {
      this.__findJavaPath()
        .then((JAVA_PATH) => {
          const method = 'encrypt';
          const encryptedText = exec(`${JAVA_PATH} -cp "${__dirname}" interop "${__dirname}" "${method}" "${textToEncrypt}"`).toString('utf8');
          resolve(encryptedText.replace(/(\r\n|\n|\r)/gm, ''));
        }, reject);
    });
  }

  decrypt(textToDecrypt) {
    return new Promise((resolve, reject) => {
      this.__findJavaPath()
        .then((JAVA_PATH) => {
          const method = 'decrypt';
          const encryptedText = exec(`${JAVA_PATH} -cp "${__dirname}" interop "${__dirname}" "${method}" "${textToDecrypt}"`).toString('utf8');
          resolve(encryptedText);
        }, reject);
    });
  }
}
// java interop "$(pwd)" "encrypt" "<Message><MessageId><Code>PAGCMR</Code><MsgDesc>Pago con CMR</MsgDesc><Version>1.10</Version><FromAddress>FalabellaPro</FromAddress><ToAddress>https://www.segurosfalabella.cl/web/seguros/pago?r=cmrpuntos</ToAddress><UrlWSInfoPago>http://www.segurosfalabella.com/wsmfppr/services/WS_INFO_PAGOSoap</UrlWSInfoPago><CodCom>10001553</CodCom><NumSec>55451440</NumSec><Date>06-02-2017</Date><Time>13:28:28</Time><Rut></Rut><FamProd>0002</FamProd></MessageId><SolicitudPago><Monto></Monto><CodMon>152</CodMon><TipoDoc>1</TipoDoc><Puntos>5000</Puntos><EAN>000000000009990086</EAN></SolicitudPago></Message>"
const client = new CMRCypher();
module.exports = client;
