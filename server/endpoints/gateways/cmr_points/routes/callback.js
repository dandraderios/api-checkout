'use strict';

const HttpStatus = require('http-status-codes');
const paymentClient = require('../../../payments/clients/paymentClient');
const gatewayClient = require('../clients/gatewayClient');
const logger = require('peanut-restify/logger');

function implementation(req, res, next) {
  var token = req.params.id;
  const outXML = req.body.outXML;

  gatewayClient
    .processPayment(req.params.id, outXML)
    .then((payment) => {
      switch (payment.state) {
        case 'paid':
          // ---------------------------------------------------------
          paymentClient
            .createSuccessRedirectionPage(payment, {
              'response_code': 0,
              'response_description': 'paid',
            })
            .then((html) => {
              res.setHeader('content-type', 'text/html');
              res.setHeader('cache-control', 'public, max-age=3600'); // 1hr cache control
              res.send(HttpStatus.OK, html);
              next();
            }, next);
          // ---------------------------------------------------------
          break;
        case 'rejected':
          // ---------------------------------------------------------
          paymentClient
            .createErrorRedirectionPage(payment, {
              'error_code': -1,
              'error_message': 'rejected',
            })
            .then((html) => {
              res.setHeader('content-type', 'text/html');
              res.setHeader('cache-control', 'public, max-age=3600'); // 1hr cache control
              res.send(HttpStatus.OK, html);
              next();
            }, next);
          // ---------------------------------------------------------
          break;
      }
    }, next);
}

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.post({
    path: '/:id/callback',
    public: true,
  }, implementation);
};
