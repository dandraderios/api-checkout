'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');
const paymentClient = require('../../../payments/clients/paymentClient');
const gatewayClient = require('../clients/gatewayClient');
const logger = require('peanut-restify/logger');

function implementation(req, res, next) {
  var token = req.params.id;
  logger.info(token);
  logger.info(JSON.stringify(req.getQuery()));
  logger.info(JSON.stringify(req.body));
  const response = [
    '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">',
    '<soapenv:Body>',
    '<Cierre_PagoResponse xmlns="http://tempuri.org/portalcs/WS_INFO_PAGO">',
    '<Cierre_PagoResult>0</Cierre_PagoResult>',
    '</Cierre_PagoResponse>',
    '</soapenv:Body>',
    '</soapenv:Envelope>',
  ].join('');
  res.setHeader('content-type', 'text/xml');
  res.setHeader('cache-control', 'public, max-age=0'); // 0hr cache control
  res.status(200);
  res.end(response);
  next();
  return;
}

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.post({
    path: '/:id/execute',
    public: true,
  }, implementation);
};
