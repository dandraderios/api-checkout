'use strict';
const GatewayClientBase = require('./../../../../utils/GatewayClientBase');
const fs = require('fs');
const handlebars = require('handlebars');
const request = require('request');
const lodash = require('lodash');
const moment = require('moment');
const libxmljs = require('libxmljs');

const CONFIG = require('peanut-restify/config');
const expr = require('peanut-restify/expressions');
const logger = require('peanut-restify/logger');
const CMRCipher = require('./../third_party/CMRCypher');
const paymentClient = require('../../../payments/clients/paymentClient');
const $configurations = require('../../../configurations/clients/$configurations');
const app = require('../../../../server');
const errors = require('../../../../utils/errors');

class GatewayClient extends GatewayClientBase {

  /**
   * Validate Model Against his legacy validation
   * @param {*} data Payment Model Data
   * @returns false if the model is Valid or an Error otherwise
   */
  isInvalid(data) {
    const model = new app.models.cmrPoints(data);

    if (model.isInvalid()) {
      return model.getValidationErrors();
    }

    return false;
  };

  /**
  * Retrieve the gateway resume for the specific gateway
  * @param {any} payment Payment Document
  * @param {any} gatewayData Gateway Data, sended in the change state step
  * @returns {GatewayResume} gateway resume
  * @memberof GatewayClient
  */
  getGatewayResume(payment, gatewayData) {
    const pan = gatewayData.RespPago.NroTarjeta;
    return new app.models.gatewayResume({
      'response': {
        'code': 0,
      },
      'transaction': {
        'gateway_id': payment.invoice_number,
        'type': 'CREDIT',
        'date': (new Date()).toISOString(),
        'currency': 'CLP',
        'buy_order': payment.transaction.gateway_order,
        'amount': payment.transaction.amount.total,
        'installments_number': 0,
      },
      'authorizations': {
        'code': gatewayData.RespPago.CodAutPuntos,
      },
      'card_number': {
        'pan_last4': pan.substring(pan.length - 4),
        'pan_first6': pan.substring(0, 6),
      },
    });
  };

  /**
   * Get Approval Dialog
   * @param {String} paymentId Payment Token id
   * @param {*} req Http Request
   */
  getApprovalDialog(paymentId, req) {
    return new Promise((resolve, reject) => {
      try {
        paymentClient
          .getById(paymentId)
          .then((payment) => {
            $configurations
              .getConfiguration(payment.application, 'CMR_POINTS')
              .then((configuration) => {
                try {
                  // Payment Data
                  const now = moment(new Date());
                  const callback = paymentClient.getBaseUrl(req);
                  const wsUrl = configuration.ws_info_pago_url || `${callback}/gateways/cmr/points/${payment.id}/execute`;
                  let numsec = payment.invoice_number.replace('INPA-', '');
                  const firstTwoChar = parseInt(numsec.substring(0, 2));
                  const ordinal = parseInt(numsec.substring(2));
                  numsec = lodash.padEnd(`${firstTwoChar}`, 2, '0') + lodash.padStart(`${ordinal}`, 6, '0');

                  const soapMessage = [
                    '<Message>',
                    ' <MessageId>', //  WS_INFO_PAGO
                    '   <Code>PAGCMR</Code>',
                    '   <MsgDesc>Pago con CMR</MsgDesc>',
                    '   <Version>1.10</Version>',
                    `   <FromAddress>${configuration.company_name}</FromAddress>`,
                    `   <ToAddress>${callback}/gateways/cmr/points/${payment.id}/callback</ToAddress>`,
                    `   <UrlWSInfoPago>${wsUrl}</UrlWSInfoPago>`,
                    `   <CodCom>${configuration.commerce_code}</CodCom>`,
                    `   <NumSec>${numsec}</NumSec>`,
                    `   <Date>${now.format('DD-MM-YYYY')}</Date>`,
                    `   <Time>${now.format('HH:mm:ss')}</Time>`,
                    '   <Rut></Rut>', // OPCIONAL
                    `   <FamProd>${payment.additional_attributes.product_family}</FamProd>`,
                    ' </MessageId>',
                    ' <SolicitudPago>',
                    '   <Monto></Monto>',
                    `   <CodMon>${payment.additional_attributes.currency_code}</CodMon>`,
                    `   <TipoDoc>${payment.additional_attributes.document_type}</TipoDoc>`,
                    `   <Puntos>${payment.additional_attributes.total_points}</Puntos>`,
                    `   <EAN>${lodash.padStart(payment.additional_attributes.ean, 18, '0')}</EAN>`,
                    ' </SolicitudPago>',
                    '</Message>',
                  ].join('');

                  CMRCipher
                    .encrypt(soapMessage)
                    .then((encriptedMessage) => {
                      // Get template
                      var content = fs.readFileSync(`${__dirname}/../templates/approval_dialog.hbs`, 'utf8');
                      var template = handlebars.compile(content);

                      // Set some variables to the html
                      var html = template({
                        iframeUrl: `${CONFIG.get('GATEWAY_CMR_POINTS_ENDPOINT')}?action=start`,
                        inXML: encriptedMessage,
                      });
                      resolve(html);
                    }, reject);
                } catch (ex) {
                  logger.error('And error has ocurred trying yo generate the approval dialog for CMR points');
                  logger.debug(ex);
                  reject(ex);
                }
              }, (err) => {
                logger.error('Can\'t get payment details for create approval dialog in CMR_POINTS');
                logger.debug(err);
                reject(err);
              });
          }, (err) => {
            logger.error('Can\'t generate approval dialog for CMR_POINTS');
            logger.debug(err);
            reject(err);
          });
      } catch (ex) {
        logger.error('And error has ocurred trying yo create the approval dialog for CMR points');
        logger.debug(ex);
        reject(ex);
      }
    });
  };

  /**
   * Check the XML in , for the CMR and callback an approve process or reject
   * depending in his response
   * @param {String} paymentId Payment ID
   * @param {String} encriptedXML Encripted XML from CMR
   * @returns {Promise} Promise
   * @memberof GatewayClient
   */
  processPayment(paymentId, encriptedXML) {
    return new Promise((resolve, reject) => {
      try {
        // xpath queries
        CMRCipher
          .decrypt(encriptedXML)
          .then((decryptedXML) => {
            // decryptedXML = "<Message><MessageId><Code>RSPPAG</Code><MsgDesc>Respuesta de Pago en CMR</MsgDesc><Version>1.10</Version><FromAddress>WebCMR</FromAddress><ToAddress>http://localhost:8081/payments/gateways/cmr/points/638158fb-edb9-431b-96c2-16a83c495421/callback</ToAddress><UrlWSInfoPago>http://www.segurosfalabella.com/wsmfppr/services/WS_INFO_PAGOSoap</UrlWSInfoPago><CodCom>10001553</CodCom><NumSec>55451440</NumSec><Date>21-02-2018</Date><Time>14:59:29</Time></MessageId><RespPago><Monto>000000000000000000</Monto><CodMon>152</CodMon><CodAut>170455958384</CodAut><CodAutPuntos>170455958384</CodAutPuntos><CodRspta>00</CodRspta><RutPagador>0-0</RutPagador><NroTarjeta>4653751012357273</NroTarjeta></RespPago></Message>";
            var xmlDoc = libxmljs.parseXml(decryptedXML);
            const gatewayData = this.__parseXML(xmlDoc.find('//Message'), {});

            // Obfuscate PAN...
            const pan = gatewayData.RespPago.NroTarjeta;
            const last4 = pan.substring(pan.length - 4);
            const first6 = pan.substring(0, 6);
            gatewayData.RespPago.NroTarjeta = `${first6}XXXXXX${last4}`;

            if (!gatewayData.RespPago) {
              return paymentClient
                .updateToRejectState(paymentId, new errors.PeinauUnloggedError(
                  'CMR_INVALID_RESPONSE',
                  'Invalid XML in CMR Response', {
                    'invalid_xml': encriptedXML,
                  }
                )).then(resolve, reject);
            }

            if (gatewayData.RespPago.CodRspta !== '00') {
              return paymentClient
                .updateToRejectState(paymentId, new errors.PeinauUnloggedError(
                  'CMR_INVALID_CODE_RESPONSE',
                  'Invalid CMR response code', {
                    'cmr_response': gatewayData,
                  }
                ))
                .then(resolve, reject);
            }

            return paymentClient
              .updateToPaidState(paymentId, gatewayData)
              .then(resolve, reject);
          }, reject);
      } catch (ex) {
        reject(new errors.UnknowError(
          'An error has ocurred trying to get process payment in cmr points',
          ex
        ));
      }
    });
  }

  /**
   * Approve payment
   * @param {String} paymentId Payment Token
   * @param {*} gatewayData Gateway Payment Data
   */
  approvePayment(paymentId, gatewayData) {
    return paymentClient
      .updateToPaidState(paymentId, gatewayData);
  };

  /**
   * Reject Payment
   * @param {String} paymentId Payment Token
   * @param {*} gatewayData Gateway payment data
   */
  rejectPayment(paymentId, gatewayData) {
    return paymentClient
      .updateToPaidState(paymentId, gatewayData);
  };

  /**
   * Parse Cybersource XML result
   * @param {any} nodes XML Nodes
   * @param {any} source XML source node
   * @returns {Object} Parsed XML into a JSON plain object
   * @memberof GatewayClient
   */
  __parseXML(nodes, source) {
    var response = (source || {});

    if (nodes.length > 0) {
      var children = lodash.first(nodes).childNodes();
      lodash.each(children, (child) => {
        if (child.childNodes().length > 0 && child.childNodes()[0].type() !== 'text') {
          var childObj = {};
          lodash.each(child.childNodes(), (schild) => {
            childObj[schild.name()] = schild.text();
          });
          response[child.name()] = childObj;
        } else {
          response[child.name()] = child.text();
        }
      });
    }
    return response;
  };
}

const client = new GatewayClient();
module.exports = client;
