'use strict';
const mongoose = require('mongoose');
const validators = require('mongoose-validators');
/* eslint camelcase: ["error", {properties: "never"}] */
module.exports = new mongoose.Schema({
  user_identifier: {
    type: String,
    required: false,
  },
  currency_code: {
    type: Number,
    required: true,
    maxlength: 3,
    minlength: 3,
  },
  document_type: {
    type: Number,
    required: true,
    maxlength: 1,
    minlength: 1,
  },
  product_family: {
    type: String,
    required: true,
    maxlength: 3,
    minlength: 3,
  },
  ean: {
    type: String,
    required: true,
  },
  total_points: {
    type: Number,
    required: true,
  },
});
