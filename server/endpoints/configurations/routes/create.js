'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');
const $configurations = require('../clients/$configurations');
/**
 * @swagger
 * /{app}/configurations/{payment_method}:
 *   post:
 *     tags:
 *       - Configurations
 *     summary: Create or Replace a gateway configuration
 *     description:
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: JWT Authorization (acquired = require(' Single Sign On)
 *         required: true
 *         type: string
 *         default: 'Bearer '
 *       - name: app
 *         description: Application Id
 *         in: path
 *         required: true
 *         type: string
 *       - name: payment_method
 *         description: Payment Method
 *         in: path
 *         required: true
 *         type: string
 *       - name: payload
 *         in: body
 *         description: New Configuration
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/newConfiguration'
 *     responses:
 *       201:
 *         description: Configuration Created
 *       400:
 *         description: Bad Request
 */
function implementation(req, res, next) {
  // Add some values
  req.body.application = req.params.application;
  /* eslint-disable camelcase */
  req.body.payment_method = req.params.payment_method;
  /* eslint-enable camelcase */

  $configurations
    .updateConfiguration(req.body)
    .then((configuration) => {
      res.send(HttpStatus.CREATED, configuration.toJSON());
      next();
    }, (err) => {
      res.send(HttpStatus.INTERNAL_SERVER_ERROR, err);
      next();
    });
};

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.post({
    path: '',
  }, implementation);
};
