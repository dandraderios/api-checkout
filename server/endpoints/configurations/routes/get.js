'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');
const $configurations = require('../clients/$configurations');

/**
 * @swagger
 * /{app}/configurations/{payment_method}:
 *   get:
 *     tags:
 *       - Configurations
 *     summary: Get the configurations = require(' a specific gateways
 *     description:
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: JWT Authorization (acquired = require(' Single Sign On)
 *         required: true
 *         type: string
 *         default: 'Bearer '
 *       - name: app
 *         description: Application Id
 *         in: path
 *         required: true
 *         type: string
 *       - name: payment_method
 *         description: Payment Method
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: List of Settings
 *       404:
 *         description: Application Not Found
 */
function implementation(req, res, next) {
  $configurations
    .getConfiguration(req.params.application, req.params.payment_method, req.query.skipErrorOnDisable)
    .then((configuration) => {
      var config = (configuration == null ? {} : configuration.toJSON());

      res.send(HttpStatus.OK, config);
      next();
    }, (err) => {
      res.send(HttpStatus.INTERNAL_SERVER_ERROR, err);
      next();
    });
};

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.get({
    path: '',
  }, implementation);
};
