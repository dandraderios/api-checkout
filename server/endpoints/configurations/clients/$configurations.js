'use strict';
const _ = require('lodash');
const mongoose = require('mongoose');
const DB = require('peanut-restify/db');
const app = require('../../../server');

// CONSTANT'S
const collectionLink = 'dbs/app-checkout-quickpay/colls/application_gateways_configuration';

function toJSON(model) {
  var self = model;
  // remove all _ properties
  Object.keys(self).forEach((key) => {
    if (key.startsWith('_')) {
      delete self[key];
    }
  });
  return self;
};

function getGatewaysAvailability(applicationId) {
  return new Promise((resolve, reject) => {
    var query = {
      query: 'SELECT c.payment_method,c.enabled FROM coll c WHERE c.application=@id',
      parameters: [{
        name: '@id',
        value: applicationId,
      }],
    };

    var options = {
      enableCrossPartitionQuery: true,
    };

    var client = DB.getConnection();
    client
      .queryDocuments(collectionLink, query, options)
      .toArray((error, items) => {
        if (error) {
          return reject(error);
        }
        resolve(items);
      });
  });
};

function getConfiguration(applicationId, paymentMethod, skipErrors = false) {
  return new Promise((resolve, reject) => {
    var query = {
      query: 'SELECT * FROM coll c WHERE c.application=@id and c.payment_method=@paymentMethod',
      parameters: [{
        name: '@id',
        value: applicationId,
      },
      {
        name: '@paymentMethod',
        value: paymentMethod,
      }],
    };

    var client = DB.getConnection();
    client
      .queryDocuments(collectionLink, query)
      .toArray((error, items) => {
        if (error) {
          return reject(error);
        }

        if (items.length == 0) {
          if (skipErrors === false) {
            return reject('GATEWAY_NOT_CONFIGURED');
          } else {
            return resolve(null);
          }
        }

        var item = _.first(items);

        if (skipErrors === false && item.enabled === false) {
          return reject('GATEWAY_IS_DISABLED');
        }

        item.toJSON = () => {
          return toJSON(item);
        };

        resolve(item);
      });
  });
};

function updateConfiguration(baseModel) {
  return new Promise((resolve, reject) => {
    try {
      // CHOOSE SCHEMA TO VALIDATE ACCORD HIS PAYMENT_METHOD
      var modelType = app.models[baseModel.payment_method];
      let model = new modelType(baseModel);

      if (model.isInvalid()) {
        const error = new Error('INVALID_MODEL');
        error.errors = model.getValidationErrors();
        return reject(error);
      }

      getConfiguration(model.application, model.payment_method, true)
        .then((configuration) => {
          if (configuration == null) {
            // CREATE CONFIGURATION
            create(model)
              .then((newConfiguration) => {
                resolve(newConfiguration);
              }, (err) => {
                reject(err);
              });
          } else {
            // REPLACE THE PROPERTIES WHICH CAN BE REPLACED!
            _.forOwn(model.toJSON(), (value, key) => {
              if (!key.startsWith('_')) {
                configuration[key] = value;
              }
            });

            // UPDATE CONFIGURATION
            update(configuration)
              .then((newConfiguration) => {
                resolve(newConfiguration);
              }, (err) => {
                reject(err);
              });
          }
        }, (err) => {
          reject(err);
        });
    } catch (ex) {
      reject(ex);
    }
  });
};

function create(model) {
  return new Promise((resolve, reject) => {
    var client = DB.getConnection();
    client.createDocument(
      collectionLink,
      model.toJSON(), {},
      (error, newDocument) => {
        if (error) {
          return reject(error);
        }

        newDocument.toJSON = () => {
          return toJSON(newDocument);
        };

        resolve(newDocument);
      });
  });
};

function update(model) {
  return new Promise((resolve, reject) => {
    var client = DB.getConnection();
    client.replaceDocument(
      model._self,
      model,
      (err, replaced) => {
        if (err) {
          reject(err);
        } else {
          replaced.toJSON = () => {
            return toJSON(replaced);
          };
          resolve(replaced);
        }
      });
  });
};

module.exports = {
  getGatewaysAvailability: getGatewaysAvailability,
  getConfiguration: getConfiguration,
  updateConfiguration: updateConfiguration,
};
