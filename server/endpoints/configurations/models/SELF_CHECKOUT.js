'use strict';
const mongoose = require('mongoose');
const validators = require('mongoose-validators');
/* eslint camelcase: ["error", {properties: "never"}] */
module.exports = new mongoose.Schema({
  application: {
    type: String,
    required: true,
    maxlength: 36,
    minlength: 36,
  },
  client_id: {
    type: String,
    required: true,
    maxlength: 21,
    minlength: 21,
  },
  payment_method: {
    required: true,
    type: String,
    minlength: 4,
  },
  enabled: {
    type: Boolean,
    required: true,
  },
  secret_shared_key: {
    type: String,
    required: true,
    maxlength: 50,
    minlength: 4,
  },
  merchant_id: {
    type: String,
    required: true,
    trim: true,
  },
  branch_id: {
    type: String,
    required: true,
    trim: true,
  },
  terminal_id: {
    type: String,
    required: true,
    trim: true,
  },
  merchant_channel: {
    type: String,
    required: true,
    trim: true,
  },
  request_channel: {
    type: String,
    required: true,
    trim: true,
  },
});
