'use strict';
const mongoose = require('mongoose');
const validators = require('mongoose-validators');
/* eslint camelcase: ["error", {properties: "never"}] */
module.exports = new mongoose.Schema({
  application: {
    type: String,
    required: true,
    maxlength: 36,
    minlength: 36,
  },
  client_id: {
    type: String,
    required: true,
    maxlength: 21,
    minlength: 21,
  },
  payment_method: {
    required: true,
    type: String,
    minlength: 4,
  },
  enabled: {
    type: Boolean,
    required: true,
  },
  company_name: {
    type: String,
    required: true,
    maxlength: 50,
    minlength: 4,
  },
  commerce_code: {
    type: String,
    required: true,
    maxlength: 10,
    minlength: 8,
  },
  ws_info_pago_url: {
    type: String,
    required: false,
  },
});
