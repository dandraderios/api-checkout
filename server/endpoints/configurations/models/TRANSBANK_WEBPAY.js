'use strict';
const mongoose = require('mongoose');
const validators = require('mongoose-validators');
const transbank = require('transbank');
const CONFIG = require('peanut-restify/config');
const fileSystem = require('fs');
/* eslint camelcase: ["error", {properties: "never"}] */
module.exports = new mongoose.Schema({
  application: {
    type: String,
    required: true,
    maxlength: 36,
    minlength: 36,
  },
  client_id: {
    type: String,
    required: true,
    maxlength: 21,
    minlength: 21,
  },
  payment_method: {
    required: true,
    type: String,
    minlength: 4,
  },
  enabled: {
    type: Boolean,
    required: true,
  },
  PRIVATE_KEY: {
    required: true,
    type: String,
    minlength: 30,
    default: () => {
      if (CONFIG.get('GATEWAY_TRANSBANK_WEBPAY_ENVIRONMENT') !== 'PRODUCCION') {
        return fileSystem.readFileSync(transbank.config.PRIVATE_KEY);
      }
      return null;
    },
  },
  PUBLIC_KEY: {
    required: true,
    type: String,
    minlength: 30,
    default: () => {
      if (CONFIG.get('GATEWAY_TRANSBANK_WEBPAY_ENVIRONMENT') !== 'PRODUCCION') {
        return fileSystem.readFileSync(transbank.config.PUBLIC_KEY);
      }
      return null;
    },
  },
  commerceCode: {
    required: true,
    type: Number,
    default: () => {
      if (CONFIG.get('GATEWAY_TRANSBANK_WEBPAY_ENVIRONMENT') !== 'PRODUCCION') {
        return transbank.config.COMMERCE_CODE;
      }
      return null;
    },
  },
});
