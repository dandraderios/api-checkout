'use strict';
const mongoose = require('mongoose');
const validators = require('mongoose-validators');
/* eslint camelcase: ["error", {properties: "never"}] */
module.exports = new mongoose.Schema({
  application: {
    type: String,
    required: true,
    maxlength: 36,
    minlength: 36,
  },
  client_id: {
    type: String,
    required: true,
    maxlength: 21,
    minlength: 21,
  },
  payment_method: {
    required: true,
    type: String,
    minlength: 4,
  },
  enabled: {
    type: Boolean,
    required: true,
  },
  merchantAuthKey: {
    type: String,
    required: true,
    trim: true,
  },
  merchantId: {
    type: String,
    required: true,
    trim: true,
  },
  branchId: {
    type: String,
    required: true,
    trim: true,
  },
  terminalId: {
    type: String,
    required: true,
    trim: true,
  },
  commerceCodeDebit: {
    type: String,
    required: true,
    trim: true,
  },
  commerceCodeCmr: {
    type: String,
    required: true,
    trim: true,
  },
  channel: {
    type: String,
    required: true,
    trim: true,
  },
});
