'use strict';
const mongoose = require('mongoose');
const validators = require('mongoose-validators');
/* eslint camelcase: ["error", {properties: "never"}] */
module.exports = new mongoose.Schema({
  application: {
    type: String,
    required: true,
    maxlength: 36,
    minlength: 36,
  },
  client_id: {
    type: String,
    required: true,
    maxlength: 21,
    minlength: 21,
  },
  payment_method: {
    required: true,
    type: String,
    minlength: 4,
  },
  enabled: {
    type: Boolean,
    required: true,
  },
  merchant: {
    required: true,
    type: String,
    minlength: 4,
  },
  webServiceUsername: {
    required: true,
    type: String,
    minlength: 4,
  },
  webServicePassword: {
    required: true,
    type: String,
    minlength: 4,
  },
  decisionManager_enabled: {
    required: false,
    type: Boolean,
    default: false,
  },
  decisionManager_threshold: {
    required: false,
    type: Number,
    default: 70,
    min: 0,
    max: 100,
  },
});
