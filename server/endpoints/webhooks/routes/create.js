'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');
const app = require('./../../../server');
const $webhooks = require('../clients/$webhooks');

/**
 * @swagger
 * /{app}/webhooks:
 *   post:
 *     tags:
 *       - Webhooks
 *     summary: Create an application web hook
 *     description:
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: JWT Authorization (acquired = require(' Single Sign On)
 *         required: true
 *         type: string
 *         default: 'Bearer '
 *       - name: app
 *         description: Application Id
 *         in: path
 *         required: true
 *         type: string
 *       - name: payload
 *         in: body
 *         description: New WebHook
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/newWebhook'
 *     responses:
 *       201:
 *         description: Webhook Created
 *       400:
 *         description: Bad Request
 */
function implementation(req, res, next) {
  // add some values
  let model = new app.models.newWebhook(req.body);
  model.application = req.params.app;
  $webhooks
    .create(model)
    .then((newWebhook) => {
      res.send(HttpStatus.CREATED, newWebhook.toJSON());
      next();
    }, (err) => {
      res.send(HttpStatus.INTERNAL_SERVER_ERROR, err);
      next();
    });
}

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.post({
    path: '/',
  }, implementation);
};
