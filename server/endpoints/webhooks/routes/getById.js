'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');
const $webhooks = require('../clients/$webhooks');
/**
 * @swagger
 * /{app}/webhooks/{id}:
 *   get:
 *     tags:
 *       - Webhooks
 *     summary: Find a Webhook by ID
 *     description:
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: JWT Authorization (acquired = require(' Single Sign On)
 *         required: true
 *         type: string
 *         default: 'Bearer '
 *       - name: id
 *         description: Webhook Id
 *         in: path
 *         required: true
 *         type: string
 *       - name: app
 *         description: Application Id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Finded Document
 *       404:
 *         description: Document Not Found
 */
function implementation(req, res, next) {
  $webhooks
    .getById(req.params.id)
    .then((item) => {
      res.send(HttpStatus.OK, item.toJSON());
      next();
    }, (err) => {
      res.send(HttpStatus.INTERNAL_SERVER_ERROR, err);
      next();
    });
};

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.get({
    path: '/:id',
  }, implementation);
};
