'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');
const $webhooks = require('../clients/$webhooks');
/**
 * @swagger
 * /{app}/webhooks:
 *   get:
 *     tags:
 *       - Webhooks
 *     summary: Get the webhooks = require(' a specific application
 *     description:
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: JWT Authorization (acquired = require(' Single Sign On)
 *         required: true
 *         type: string
 *         default: 'Bearer '
 *       - name: app
 *         description: Application Id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: List of Webhooks
 *       404:
 *         description: Application Not Found
 */
function implementation(req, res, next) {
  $webhooks
    .getByApplication(req.params.app)
    .then((items) => {
      // Clean
      items.forEach((webhook) => {
        delete webhook.application;
        webhook.toJSON();
      });

      res.send(HttpStatus.OK, items);
      next();
    }, (err) => {
      res.send(HttpStatus.INTERNAL_SERVER_ERROR, err);
      next();
    });
};

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.get({
    path: '',
  }, implementation);
};
