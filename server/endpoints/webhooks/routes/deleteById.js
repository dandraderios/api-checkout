'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');
const $webhooks = require('../clients/$webhooks');
/**
 * @swagger
 * /webhooks/{id}:
 *   delete:
 *     tags:
 *       - Webhooks
 *     summary: Delete the webhook by ID
 *     description:
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: JWT Authorization (acquired = require(' Single Sign On)
 *         required: true
 *         type: string
 *         default: 'Bearer '
 *       - name: id
 *         description: Webhook Id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       401:
 *         description: Unauthorized
 *       200:
 *         description: Webhook Updated
 *       404:
 *         description: Webhook Not Found
 */
function implementation(req, res, next) {
  // SET SOME VARIABLES
  var token = req.params.id;
  $webhooks
    .del(token)
    .then(() => {
      res.send(HttpStatus.NO_CONTENT);
      next();
    }, (err) => {
      res.send(HttpStatus.INTERNAL_SERVER_ERROR, err);
      next();
    });
};

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.del({
    path: '/:id',
  }, implementation);
};
