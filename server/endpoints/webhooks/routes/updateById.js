'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');
const app = require('../../../server');
const $webhooks = require('../clients/$webhooks');
/**
 * @swagger
 * /{app}/webhooks/{id}:
 *   put:
 *     tags:
 *       - Webhooks
 *     summary: Update an application web hook
 *     description:
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: JWT Authorization (acquired = require(' Single Sign On)
 *         required: true
 *         type: string
 *         default: 'Bearer '
 *       - name: id
 *         description: Web hook Id
 *         in: path
 *         required: true
 *         type: string
 *       - name: app
 *         description: Application Id
 *         in: path
 *         required: true
 *         type: string
 *       - name: payload
 *         in: body
 *         description: Updated WebHook
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/updatedWebhook'
 *     responses:
 *       204:
 *         description: Webhook Updated
 *       400:
 *         description: Bad Request
 */
function implementation(req, res, next) {
  // add some values
  req.body.application = req.params.appId;
  let model = new app.models.updatedWebhook(req.body);

  // SET SOME VARIABLES
  var token = req.params.id;

  $webhooks
    .updateById(token, model)
    .then((updated) => {
      delete updated.application;

      res.send(HttpStatus.PARTIAL_CONTENT, updated.toJSON());
      next();
    }, (err) => {
      res.send(HttpStatus.INTERNAL_SERVER_ERROR, err);
      next();
    });
};

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.put({
    path: '/:id',
  }, implementation);
};
