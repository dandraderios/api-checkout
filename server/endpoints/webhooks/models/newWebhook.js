'use strict';
const mongoose = require('mongoose');
const validators = require('mongoose-validators');

/**
 * @swagger
 * definitions:
 *   newWebhook:
 *     type: object
 *     required:
 *       - name
 *       - url
 *       - events
 *       - active
 *     properties:
 *       name:
 *         type: string
 *         format: string
 *       url:
 *         type: string
 *         format: string
 *       events:
 *         type: array
 *         items:
 *           type: string
 *       active:
 *         type: boolean
 *         default: true
 */
/* eslint camelcase: ["error", {properties: "never"}] */
module.exports = new mongoose.Schema({
  url: {
    type: String,
    required: true,
    trim: true,
    maxlength: 2000,
    minlength: 20,
    validate: validators.isURL(),
  },
  name: {
    type: String,
    required: true,
    trim: true,
    maxlength: 50,
    minlength: 1,
  },
  application: {
    type: String,
    required: true,
  },
  active: {
    type: Boolean,
    required: true,
  },
  events: [{
    type: String,
    required: true,
    enum: [
      'payment_created',
      'payment_paid',
      'payment_rejected',
      'payment_refunded',
      'payment_accounted',
      'payment_edited',
    ],
  }],
});
