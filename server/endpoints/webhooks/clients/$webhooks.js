'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');
const _ = require('lodash');
const request = require('request');

const DB = require('peanut-restify/db');

// CONSTANT'S
const collectionLink = 'dbs/app-checkout-quickpay/colls/application_webhooks';

function toJSON(model) {
  var self = model;
  // remove all _ properties
  Object.keys(self).forEach((key) => {
    if (key.startsWith('_')) {
      delete self[key];
    }
  });
  return self;
};

function getByApplication(applicationId) {
  return new Promise((resolve, reject) => {
    var query = {
      query: 'SELECT * FROM coll c WHERE c.application=@id',
      parameters: [{
        name: '@id',
        value: applicationId,
      }],
    };

    var client = DB.getConnection();
    client
      .queryDocuments(collectionLink, query, {
        enableCrossPartitionQuery: true,
      })
      .toArray((err, items) => {
        if (err) {
          err.type = 'DOCUMENTDB_ERROR';
          return reject(err);
        }

        items.forEach((item) => {
          item.toJSON = () => {
            return toJSON(item);
          };
        });

        resolve(items);
      });
  });
};

function getById(token) {
  return new Promise((resolve, reject) => {
    var query = {
      query: 'SELECT * FROM coll c WHERE c.id=@id',
      parameters: [{
        name: '@id',
        value: token,
      }],
    };

    var client = DB.getConnection();
    client
      .queryDocuments(collectionLink, query, {
        enableCrossPartitionQuery: true,
      })
      .toArray((err, items) => {
        if (err) {
          err.type = 'DOCUMENTDB_ERROR';
          return reject(err);
        }

        if (items.length == 0) {
          var error = new Error('NOT_FOUND');
          error.type = 'NOT_FOUND';
          return reject(error);
        }

        var item = _.first(items);
        item.toJSON = () => {
          return toJSON(item);
        };

        resolve(item);
      });
  });
};

function getByEvent(applicationId, eventName) {
  return new Promise((resolve, reject) => {
    var query = {
      query: 'SELECT c.url FROM coll c WHERE c.application=@id \
                    AND c.active=true \
                    AND ARRAY_CONTAINS(c.events, @event)',
      parameters: [{
        name: '@id',
        value: applicationId,
      },
      {
        name: '@event',
        value: eventName,
      }],
    };

    var client = DB.getConnection();
    client
      .queryDocuments(collectionLink, query, {
        enableCrossPartitionQuery: true,
      })
      .toArray((err, items) => {
        if (err) {
          err.type = 'DOCUMENTDB_ERROR';
          return reject(err);
        }

        resolve(items);
      });
  });
};

function create(model) {
  return new Promise((resolve, reject) => {
    try {
      if (model.isInvalid()) {
        var error = new Error('INVALID_MODEL');
        error.errors = model.getValidationErrors();
        error.type = 'INVALID_MODEL';
        return reject(error);
      }

      var client = DB.getConnection();
      client.createDocument(
        collectionLink,
        model.toJSON(), {
        },
        (err, webhook) => {
          if (err) {
            err.type = 'DOCUMENTDB_ERROR';
            return reject(err);
          }

          webhook.toJSON = () => {
            return toJSON(webhook);
          };

          resolve(webhook);
        });
    } catch (ex) {
      reject(ex);
    }
  });
}

function updateById(token, model) {
  return new Promise((resolve, reject) => {
    if (model.isInvalid()) {
      var error = new Error('INVALID_MODEL');
      error.errors = model.getValidationErrors();
      return reject(error);
    }

    var _update = (docToUpdate) => {
      var client = DB.getConnection();
      client
        .replaceDocument(docToUpdate._self, docToUpdate, {
          enableCrossPartitionQuery: true,
        }, (err, replaced) => {
          if (err) {
            reject(err);
          } else {
            replaced.toJSON = () => {
              return toJSON(replaced);
            };
            resolve(replaced);
          }
        });
    };

    // ALREADY HAVE THE SELF LINK (FULL DOCUMENT)??
    if (model._self) {
      _update(model); // JUST UPDATE!
    } else {
      getById(token)
        .then((itemToUpdate) => {
          // REPLACE THE PROPERTIES WHICH CAN BE REPLACED!
          ((props) => {
            _.each(props, (prop) => {
              if (typeof model[prop] !== 'undefined') {
                itemToUpdate[prop] = model[prop];
              }
            });
          })([
            'url',
            'name',
            'active',
            'events',
          ]);

          // console.log(app);
          _update(itemToUpdate);
        }, (err) => {
          reject(err);
        });
    }
  });
};

function del(token) {
  return new Promise((resolve, reject) => {
    getById(token)
      .then((document) => {
        var client = DB.getConnection();
        client.deleteDocument(document._self, {
          partitionKey: document.application,
        },
          (err) => {
            if (err) {
              err.type = 'DOCUMENTDB_ERROR';
              return reject(err);
            }
            resolve();
          });
      }, (err) => {
        reject(err);
      });
  });
};

module.exports = {
  getByApplication: getByApplication,
  getByEvent: getByEvent,
  getById: getById,
  create: create,
  updateById: updateById,
  del: del,
};
