'use strict';
const lodash = require('lodash');

const DB = require('peanut-restify/db');
const CONFIG = require('peanut-restify/config');
const logger = require('peanut-restify/logger');
const app = require('./../../../server');

// CONSTANT'S
const collectionLink = 'dbs/app-checkout-quickpay/colls/user_tokenizations';

class PaymentClient {

  /**
   * Get a Capture Token by his Id
   * @param {String} token Capture Token
   */
  getById(token) {
    return new Promise((resolve, reject) => {
      try {
        var query = {
          query: 'SELECT * FROM T WHERE T.id=@id',
          parameters: [{
            name: '@id',
            value: token,
          }],
        };

        var client = DB.getConnection();
        client
          .queryDocuments(collectionLink, query, {
            enableCrossPartitionQuery: true,
          })
          .toArray((err, items) => {
            if (err) {
              logger.error('Exception has occurred trying to get a tokenization capture by id in the checkout');
              logger.debug(err);
              return reject(err.body);
            }

            if (items.length == 0) {
              return reject('DOCUMENT_NOT_FOUND');
            }

            var item = lodash.first(items);
            resolve(item);
          });
      } catch (err) {
        logger.error('General exception has occurred trying to get a tokenization capture by id in the checkout');
        logger.debug(err);
        reject(err);
      }
    });
  };

  /**
   * Create a tokenization capture for checkout isolation (resilence)
   * @param {Object} data Capture Data
   */
  create(data) {
    return new Promise((resolve, reject) => {
      try {
        var client = DB.getConnection();
        client
          .createDocument(collectionLink, data,
          (err, document) => {
            if (err) {
              err.type = 'DOCUMENTDB_ERROR';
              logger.error('Exception has occurred trying to create a tokenization capture in the checkout');
              logger.debug(err);
              return reject(err);
            }

            resolve(document);
          });
      } catch (err) {
        logger.error('General exception has occurred trying to create a tokenization capture in the checkout');
        logger.debug(err);
        reject(err);
      }
    });
  }

}

const client = new PaymentClient();
module.exports = client;
