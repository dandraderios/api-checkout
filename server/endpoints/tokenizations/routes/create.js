'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');
const app = require('../../../server');

const tokenizationClient = require('../clients/tokenizationClient');
const CONFIG = require('peanut-restify/config');

function implementation(req, res, next) {
  tokenizationClient
    .create(req.body)
    .then((intention) => {
      res.send(HttpStatus.OK);
      next();
    }, (err) => {
      res.send(HttpStatus.INTERNAL_SERVER_ERROR, err);
      next();
    });
};

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.post({
    path: '/',
    public: true,
  }, implementation);
};
