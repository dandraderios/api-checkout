'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');

const $configurations = require('../../configurations/clients/$configurations');
/**
 * @swagger
 * /{app}/availability/gateways:
 *   get:
 *     tags:
 *       - Availability
 *     summary: Get the availabilities for each gateways configured in the application
 *     description:
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: app
 *         description: Application Id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: List of Gateways configured for the App
 *       404:
 *         description: Application Not Found
 */
function implementation(req, res, next) {
  $configurations
    .getGatewaysAvailability(req.params.application)
    .then((availability) => {
      res.send(HttpStatus.OK, availability);
      next();
    }, (err) => {
      res.send(HttpStatus.INTERNAL_SERVER_ERROR, err);
      next();
    });
};

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.get({
    path: '/gateways',
    public: true,
  }, implementation);
};
