'use strict';
const mongoose = require('mongoose');
const validators = require('mongoose-validators');

/* eslint camelcase: ["error", {properties: "never"}] */
module.exports = new mongoose.Schema({
  intent: {
    type: String,
    required: true,
    trim: true,
    maxlength: 10,
    minlength: 4,
  },
  application: {
    type: String,
    required: true,
    maxlength: 36,
    minlength: 36,
  },
  links: [{
    href: {
      type: String,
      required: true,
      trim: true,
      maxlength: 250,
      minlength: 4,
      validate: validators.isURL(),
    },
    rel: {
      type: String,
      required: true,
    },
    method: {
      type: String,
      required: true,
    },
  }],
  payer: {
    payment_method: {
      required: true,
      type: String,
    },
    payer_info: {
      type: Object,
      required: true,
      country: {
        required: true,
        type: String,
        maxlength: 2,
        minlength: 2,
      },
      document_number: {
        required: false,
        type: String,
        maxlength: 50,
      },
      document_type: {
        required: false,
        enum: ['RUT', 'DNI', 'RUN'],
        type: String,
      },
      email: {
        required: true,
        type: String,
        validate: validators.isEmail(),
      },
      full_name: {
        required: true,
        type: String,
      },
    },
  },
  transaction: {
    reference_id: {
      required: false,
      type: String,
    },
    gateway_order: {
      required: false,
      type: String,
    },
    description: {
      required: true,
      type: String,
    },
    soft_descriptor: {
      required: true,
      type: String,
    },
    amount: {
      currency: {
        required: true,
        type: String,
        minlength: 3,
        maxlength: 3,
      },
      total: {
        required: true,
        type: Number,
      },
      details: {
        subtotal: {
          required: true,
          type: Number,
        },
        tax: {
          required: true,
          type: Number,
        },
        shipping: {
          required: true,
          type: Number,
        },
        shipping_discount: {
          required: true,
          type: Number,
        },
      },
    },
    item_list: {
      shipping_address: {
        line1: {
          required: true,
          type: String,
        },
        city: {
          required: true,
          type: String,
        },
        country_code: {
          required: true,
          type: String,
          minlength: 2,
          maxlength: 2,
        },
        phone: {
          required: true,
          type: String,
        },
        type: {
          required: true,
          enum: ['HOME_OR_WORK'],
          type: String,
        },
        recipient_name: {
          required: true,
          type: String,
        },
      },
      shipping_method: {
        required: true,
        enum: ['DIGITAL', 'DELIVERY', 'PICK_IN_PLACE'],
        type: String,
      },
      items: [{
        thumbnail: {
          required: false,
          type: String,
        },
        sku: {
          required: true,
          type: String,
        },
        name: {
          required: true,
          type: String,
        },
        description: {
          required: true,
          type: String,
        },
        quantity: {
          required: true,
          type: Number,
        },
        price: {
          required: true,
          type: Number,
        },
        tax: {
          required: true,
          type: Number,
        },
      }],
    },
  },
  redirect_urls: {
    return_url: {
      type: String,
      required: true,
      trim: true,
      maxlength: 250,
      minlength: 4,
    },
    cancel_url: {
      type: String,
      required: true,
      trim: true,
      maxlength: 250,
      minlength: 4,
      validate: validators.isURL(),
    },
  },
  settings: {
    UI: {
      type: String,
      required: false,
      trim: true,
      maxlength: 40,
      minlength: 4,
    },
  },
  additional_attributes: {
    type: Object,
    required: false,
  },
  meta_data: {
    type: Object,
    required: false,
  },
});
