'use strict';
const mongoose = require('mongoose');
const validators = require('mongoose-validators');

/* eslint camelcase: ["error", {properties: "never"}] */
module.exports = new mongoose.Schema({
  transaction: {
    reference_id: {
      required: false,
      type: String,
    },
    amount: {
      currency: {
        required: false,
        type: String,
        minlength: 3,
        maxlength: 3,
      },
      total: {
        required: false,
        type: Number,
      },
      details: {
        subtotal: {
          required: false,
          type: Number,
        },
        tax: {
          required: false,
          type: Number,
        },
        shipping: {
          required: false,
          type: Number,
        },
        shipping_discount: {
          required: false,
          type: Number,
        },
      },
    },
    item_list: {
      shipping_address: {
        line1: {
          required: false,
          type: String,
        },
        city: {
          required: false,
          type: String,
        },
        country_code: {
          required: false,
          type: String,
          minlength: 2,
          maxlength: 2,
        },
        phone: {
          required: false,
          type: String,
        },
        type: {
          required: false,
          enum: ['HOME_OR_WORK'],
          type: String,
        },
        recipient_name: {
          required: false,
          type: String,
        },
      },
      shipping_method: {
        required: false,
        enum: ['DIGITAL', 'DELIVERY', 'PICK_IN_PLACE'],
        type: String,
      },
      items: [{
        thumbnail: {
          required: false,
          type: String,
        },
        sku: {
          required: true,
          type: String,
        },
        name: {
          required: true,
          type: String,
        },
        description: {
          required: true,
          type: String,
        },
        quantity: {
          required: true,
          type: Number,
        },
        price: {
          required: true,
          type: Number,
        },
        tax: {
          required: true,
          type: Number,
        },
      }],
    },
  },
  meta_data: {
    type: Object,
    required: false,
  },
});
