'use strict';
const mongoose = require('mongoose');
const validators = require('mongoose-validators');

/* eslint camelcase: ["error", {properties: "never"}] */
module.exports = new mongoose.Schema({
  response: {
    code: {
      required: true,
      type: Number,
    },
  },
  transaction: {
    gateway_id: {
      required: true,
      type: String,
    },
    type: {
      required: true,
      enum: ['CREDIT', 'DEBIT', 'POINTS'],
      type: String,
    },
    date: {
      required: true,
      type: Date,
    },
    currency: {
      required: true,
      type: String,
      maxlength: 3,
      minlength: 3,
    },
    buy_order: {
      required: true,
      type: String,
    },
    amount: {
      type: Number,
      required: true,
    },
    installments_number: {
      type: Number,
      required: true,
    },
  },
  authorizations: {
    code: {
      required: true,
      type: String,
    },
  },
  card_number: {
    pan_last4: {
      required: false,
      type: String,
      maxlength: 4,
      minlength: 4,
    },
    pan_first6: {
      required: false,
      type: String,
      maxlength: 6,
      minlength: 6,
    },
  },
});
