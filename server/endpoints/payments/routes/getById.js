'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');

const client = require('../clients/paymentClient');
const errors = require('./../../../utils/errors');
/**
 * @swagger
 * /payments/{id}:
 *   get:
 *     tags:
 *       - Payments
 *     summary: Find a Payments by ID
 *     description:
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: JWT Authorization (acquired = require(' Single Sign On)
 *         required: true
 *         type: string
 *         default: 'Bearer '
 *       - name: id
 *         description: Application Id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Finded Document
 *       404:
 *         description: Document Not Found
 */
function implementation(req, res, next) {
  // Switch between two method (by token id or by gateway order)
  const method = (req.params.id.length == 36) ? client.getById(req.params.id) : client.getByGatewayOrder(req.params.id);
  method
    .then((app) => {
      res.send(HttpStatus.OK, app.toJSON());
      next();
    }, next);
};

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.get({
    path: '/:id',
  }, implementation);
};
