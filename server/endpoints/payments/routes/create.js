'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');
const app = require('../../../server');

const paymentClient = require('../clients/paymentClient');
const CONFIG = require('peanut-restify/config');
const logger = require('peanut-restify/logger');
/**
 * @swagger
 * /payments:
 *   post:
 *     tags:
 *       - Payments
 *     summary: Create an payment intention
 *     description:
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: JWT Authorization (acquired = require(' Single Sign On)
 *         required: true
 *         type: string
 *         default: 'Bearer '
 *       - name: payload
 *         in: body
 *         description: New Intention
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/newIntention'
 *     responses:
 *       201:
 *         description: Document Created
 *       400:
 *         description: Bad Request
 */
function implementation(req, res, next) {
  // add some values
  req.body.application = req.user.primarysid;

  paymentClient
    .create(req.body, paymentClient.getBaseUrl(req))
    .then((intention) => {
      res.send(HttpStatus.CREATED, intention.toJSON());
      next();
    }, next);
};

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.post({
    path: '/',
  }, implementation);
};
