'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');

const paymentClient = require('../clients/paymentClient');

function implementation(req, res, next) {
  paymentClient
    .updatePartial(req.params.id, req.body)
    .then((updatedPayment) => {
      res.send(HttpStatus.OK, updatedPayment);
      next();
    }, next);
};

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.put({
    path: '/:id/edit',
  }, implementation);
};
