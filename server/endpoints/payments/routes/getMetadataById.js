'use strict';
const restify = require('restify');
const HttpStatus = require('http-status-codes');

const paymentClient = require('../clients/paymentClient');

/**
 * @swagger
 * /payments/{id}:
 *   get:
 *     tags:
 *       - Payments
 *     summary: Find a Payments by ID
 *     description:
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: JWT Authorization (acquired = require(' Single Sign On)
 *         required: true
 *         type: string
 *         default: 'Bearer '
 *       - name: id
 *         description: Application Id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Finded Document
 *       404:
 *         description: Document Not Found
 */
function implementation(req, res, next) {
  paymentClient
    .getMetadataById(req.params.id)
    .then((metadata) => {
      res.send(HttpStatus.OK, metadata.toJSON());
      next();
    }, next);
};

// ADD ROUTE AND DEFINITION'S
module.exports = (server) => {
  server.get({
    path: '/:id/metadata',
  }, implementation);
};
