'use strict';

const states = {
  'CREATED': 'created',
  'PAID': 'paid',
  'REJECTED': 'rejected',
  'REFUNDED': 'refunded',
  'PARTIALLY_REFUNDED': 'partially_refunded',
  'METADA_CHANGED': 'metadata_changed', // Efimeral (not updated in the db)
  'CANCELED': 'canceled',
};

// Check for some constraints (BPM)
// Conditions:
//   - Apply this rules , only when a change state is raised
// Rules:
//   - 1: A payment 'paid' document can be updated his state (sealed state)
//   - 2: A payment 'reject' document can be rejected his state (sealed state)
const transitions = (() => {
  const transitions = {};
  transitions[states.CREATED] = [states.PAID, states.REJECTED, states.CANCELED];
  transitions[states.PAID] = [states.PARTIALLY_REFUNDED, states.REFUNDED];
  transitions[states.PARTIALLY_REFUNDED] = [states.PARTIALLY_REFUNDED, states.REFUNDED];
  transitions[states.REJECTED] = [];
  transitions[states.CANCELED] = [];
  return transitions;
})();

class BPMClient {

  /**
   * Return all posible transitions payments
   * @returns {Object} Object with a list of availables states
   * @memberof BPMClient
   */
  getStates() {
    return states;
  };

  /**
   * Get Available State for a specific state
   * @memberof BPMClient
   */
  getAvailableStates(state) {
    return transitions[state] || [];
  };

  /**
   * Check if a transition is acceptable to pass the transition update
   * @param {String} currentState Current State for the document
   * @param {String} newState New State for the document to transition
   * @returns {Boolean} Return if a transition can be
   * @memberof BPMClient
   */
  canTransitionToState(currentState, newState) {
    const availableStates = this.getAvailableStates(currentState);
    return availableStates.indexOf(newState) >= 0;
  }
}
const client = new BPMClient();
module.exports = client;
