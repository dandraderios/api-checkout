'use strict';
const _ = require('lodash');
const moment = require('moment');
const fs = require('fs');
const Handlebars = require('handlebars');
const utils = require('util');
const lodash = require('lodash');

const DB = require('peanut-restify/db');
const CONFIG = require('peanut-restify/config');
const logger = require('peanut-restify/logger');
const expr = require('peanut-restify/expressions');
const errors = require('../../../utils/errors');
const notifications = require('sdk-notification-client');
const app = require('./../../../server');
const $webhooks = require('../../webhooks/clients/$webhooks');
const uuid = require('uuid/v4');
const BPMClient = require('./BPMClient');

// CONSTANT'S
const collectionLink = 'dbs/app-checkout-quickpay/colls/payment_intentions';

class PaymentClient {

  /**
   * Return the object witouth any underscore variable (to the client)
   * @param {Object} model JSON to process
   * @returns {Object} object witouth any underscore variable
   * @memberof PaymentClient
   */
  toJSON(model) {
    var self = model;
    // remove all _ properties
    Object.keys(self).forEach((key) => {
      if (key.startsWith('_')) {
        delete self[key];
      }
    });
    return self;
  };

  /**
   * Send webhooks asociated to state finded by his event
   * @param {String} appId Application Id
   * @param {String} eventName Event name
   * @param {Object} payload Payload to send
   * @memberof PaymentClient
   */
  sendWebHook(appId, eventName, payload) {
    // NOTIFICATION CLIENT
    var client = new notifications.NotificationClient(
      CONFIG.get('NOTIFICATION_ENDPOINT'),
      CONFIG.get('NOTIFICATION_AUTH_KEY')
    );
    $webhooks
      .getByEvent(appId, eventName)
      .then((webhooks) => {
        if (webhooks.length === 0) {
          return;
        }
        // URL WEB HOOK
        client.triggerWebHooks(
          webhooks,
          eventName,
          payload
        ).then(() => {
          // Do Nothing
        }, (err) => {
          logger.error('An error has ocurred trying to send hooks via notification SDK');
          logger.debug(err);
        });
      }, (err) => {
        logger.error('An error has ocurred trying to get the event hook for payment');
        logger.debug(err);
      });
  }

  /**
   * Get a Payment by his id
   * @param {String} token Gateway Token to find
   * @returns {Object} Payment Object
   * @memberof PaymentClient
   */
  getById(token) {
    return new Promise((resolve, reject) => {
      try {
        var query = {
          query: 'SELECT * FROM payment_intentions n WHERE n.id=@id',
          parameters: [{
            name: '@id',
            value: token,
          }],
        };

        var client = DB.getConnection();
        client
          .queryDocuments(collectionLink, query)
          .toArray((err, payments) => {
            if (err) {
              return reject(new errors.DatabaseError(
                'unknown error has ocurred when trying to get a payment by his id',
                new Error(err.body)
              ));
            }

            if (payments.length == 0) {
              return reject(new errors.DocumentNotFoundError(
                token,
                collectionLink
              ));
            }

            var payment = _.first(payments);

            // Wee need to send "reference function" and not arrow function
            var self = this;
            payment.toJSON = function () {
              return self.toJSON(this);
            };

            resolve(payment);
          });
      } catch (ex) {
        reject(new errors.UnknowError(
          'An error has ocurred trying to find a payment by Id',
          ex
        ));
      }
    });
  };

  /**
   * Get a Payment by his gateway_order
   * (Can be the INPA or the value sended for the merchant in the transaction.gateway_order field)
   * @param {String} gatewayOrder Gateway Order to find
   * @returns {Object} Payment Object
   * @memberof PaymentClient
   */
  getByGatewayOrder(gatewayOrder) {
    return new Promise((resolve, reject) => {
      // TODO: REMOVE find by invoice
      var query = {
        query: 'SELECT * FROM payment_intentions n WHERE n.transaction.gateway_order=@gatewayOrder OR n.invoice_number=@gatewayOrder',
        parameters: [{
          name: '@gatewayOrder',
          value: gatewayOrder,
        }],
      };

      var client = DB.getConnection();
      client
        .queryDocuments(collectionLink, query)
        .toArray((err, payments) => {
          if (err) {
            return reject(new errors.DatabaseError(
              'unknown error has ocurred when trying to get a payment by his gateway order',
              new Error(err.body)
            ));
          }

          if (payments.length == 0) {
            return reject(new errors.DocumentNotFoundError(
              gatewayOrder,
              collectionLink
            ));
          }

          var payment = _.first(payments);
          // Wee need to send "reference function" and not arrow function
          var self = this;
          payment.toJSON = function () {
            return self.toJSON(this);
          };

          resolve(payment);
        });
    });
  };

  /**
   * Get Metadata for a Payment Document
   * @param {any} token Payment Token
   * @returns {Object} Metada Object
   * @memberof PaymentClient
   */
  getMetadataById(token) {
    return new Promise((resolve, reject) => {
      var query = {
        query: 'SELECT T.meta_data FROM T WHERE T.id=@id',
        parameters: [{
          name: '@id',
          value: token,
        }],
      };

      var client = DB.getConnection();
      client
        .queryDocuments(collectionLink, query)
        .toArray((err, items) => {
          if (err) {
            return reject(new errors.DatabaseError(
              'unknown error has ocurred when trying to get a payment meta_data',
              new Error(err.body)
            ));
          }

          if (items.length == 0) {
            return reject(new errors.DocumentNotFoundError(
              token,
              collectionLink
            ));
          }
          var metadata = (_.first(items)).meta_data;
          metadata.toJSON = () => {
            return this.toJSON(metadata);
          };

          resolve(metadata);
        });
    });
  };

  /**
   * Create a Payment in his initial state
   * @param {any} data Merchant data sended for creating the payment
   * @param {any} baseUrl Server Path mount
   * @returns {Object} New Payment Document
   * @memberof PaymentClient
   */
  create(data, baseUrl) {
    return new Promise((resolve, reject) => {
      try {
        let model = new app.models.newIntention(data);

        // Check if the model is valid
        if (model.isInvalid()) {
          return reject(new errors.InvalidModelError(model));
        }

        // Check if the payment exist
        const gatewayDefinition = gatewaysSupported[model.payer.payment_method.toLowerCase()];
        if (!gatewayDefinition) {
          return reject(new errors.PeinauUnloggedError(
            'PAYMENT_METHOD_NOT_SUPPORTED',
            `the payment_method ${model.payer.payment_method} is not supported`
          ));
        }

        // GET THE PAYMENT CLIENT FOR THE SPECIFIC PAYMENT_METHOD
        const contextClient = require(`${gatewayDefinition.path}/clients/gatewayClient`);

        // CALL FOR SOME ADDITIONALS VALIDATION METHOD'S
        const contextValidationErrors = contextClient.isInvalid(data.additional_attributes || {});
        if (contextValidationErrors) {
          return reject(new errors.PeinauUnloggedError(
            'INVALID_MODEL',
            'the requested model in additional_attributes has invalid properties',
            {
              validations: contextValidationErrors,
            }
          ));
        }

        const newPayment = model.toJSON();
        // Add if and some link before create
        newPayment.id = uuid();

        // ADD SOME HATEOAS LINKS
        const payRoute = `${baseUrl}/gateways/${newPayment.payer.payment_method.toLowerCase().replace('_', '/')}/${newPayment.id}`;
        newPayment.links = [];

        // ADD DETAILS DOCUMENT
        newPayment.links.push({
          href: `${baseUrl}/${newPayment.id}`,
          rel: 'self',
          security: ['ApiKey'],
          method: 'GET',
        });

        // ADD APPROVAL URL
        newPayment.links.push({
          href: `${payRoute}/pay`,
          rel: 'approval_url',
          method: 'REDIRECT',
        });

        // ADD EDIT URL (Partial Update)
        newPayment.links.push({
          href: `${baseUrl}/${newPayment.id}/edit`,
          rel: 'update_url',
          method: 'PUT',
        });

        contextClient
          .getAdditionalLinks(payRoute)
          .forEach((link) => {
            newPayment.links.push(link);
          });

        var client = DB.getConnection();
        client.createDocument(
          collectionLink,
          newPayment, {
            preTriggerInclude: 'CREATE_AUTOINCREMENT',
          },
          (err, document) => {
            if (err) {
              return reject(new errors.DatabaseError(
                'Can\'t create a valid payment intention document',
                err
              ));
            }

            // ADD DETAILS DOCUMENT BY GATEWAY_ORDER (AFTER GET INPA)
            document.links.push({
              href: `${baseUrl}/${document.transaction.gateway_order}`,
              rel: 'self_by_gateway_order',
              security: ['ApiKey'],
              method: 'GET',
            });

            document.toJSON = () => {
              document.transaction.item_list.items.forEach((item) => {
                Object.keys(item).forEach((key) => {
                  if (key.startsWith('_')) {
                    delete item[key];
                  }
                });
              });

              return this.toJSON(document);
            };

            // NOTIFICATION CLIENT
            this.sendWebHook(
              document.application,
              'payment_created',
              document.toJSON()
            );

            resolve(document);
          });
      } catch (err) {
        return reject(new errors.UnknowError(
          'Unknow error when trying to create a payment intention',
          err
        ));
      }
    });
  }

  /**
  * Update Payment to PAID state
  * @param {String} token Payment Token
  * @param {Object} gatewayData Updated Payment Document
  * @returns {Object} Updated Payment Document
  * @memberof PaymentClient
  */
  updateToPaidState(token, gatewayData) {
    return this.__updateToState(
      token,
      BPMClient.getStates().PAID,
      // Pass function to get gatewayData
      (paymentDocument) => {
        try {
          // Check if the resume object is OK, If not set the 'error' , but not throw any expection
          const gatewayDefinition = gatewaysSupported[paymentDocument.payer.payment_method.toLowerCase()];
          const contextClient = require(`${gatewayDefinition.path}/clients/gatewayClient`);
          let resume = contextClient.getGatewayResume(paymentDocument, gatewayData);

          // If the model is Invalid??
          if (resume.isInvalid()) {
            resume = {
              description: 'ERROR: Gateway Resume is invalid',
              errors: resume.getValidationErrors(),
            };
          }
          gatewayData.resume = resume;
        } catch (ex) {
          gatewayData.resume = ex;
        }

        return gatewayData;
      });
  };

  /**
  * Update Payment to REJECTED state
  * @param {String} token Payment Token
  * @param {Object} gatewayData Updated Payment Document
  * @returns {Object} Updated Payment Document
  * @memberof PaymentClient
  */
  updateToRejectState(token, gatewayData) {
    return this.__updateToState(
      token,
      BPMClient.getStates().REJECTED,
      gatewayData
    );
  };

  /**
  * Update Payment to REFUNDED state
  * @param {String} token Payment Token
  * @param {Object} gatewayData Updated Payment Document
  * @returns {Object} Updated Payment Document
  * @memberof PaymentClient
  */
  updateToRefundedState(token, gatewayData) {
    return this.__updateToState(
      token,
      (paymentDocument) => {
        // Get the refunded_amount (validate in specific )
        if (gatewayData.refunded_amount < paymentDocument.transaction.amount.total) {
          return BPMClient.getStates().PARTIALLY_REFUNDED;
        }
        return BPMClient.getStates().REFUNDED;
      },
      gatewayData
    );
  };

  /**
   * Update Payment in the DB
   * @param {String} token Payment Token
   * @param {String} newState State to transition
   * @param {Object|Function} gatewayDataOrFunction Updated Payment Document
   * @returns {Object} Updated Payment Document
   * @memberof PaymentClient
   */
  __updateToState(token, newStateOrFunction, gatewayDataOrFunction) {
    return new Promise((resolve, reject) => {
      const defers = [
        this.getById(token),
      ];

      Promise
        .all(defers)
        .then((resolves) => {
          const fullDocument = resolves[0];

          let newState = newStateOrFunction;
          if (typeof newStateOrFunction === 'function') {
            newState = newStateOrFunction(fullDocument);
          }

          // Can transition to new state??
          if (!BPMClient.canTransitionToState(fullDocument.state, newState)) {
            return reject(new errors.LocalizedError('TRANSITION_TO_NEW_STATE_IS_NOT_FOUND', {
              'document_id': fullDocument.id,
              'payment_method': fullDocument.payer.payment_method,
              'document_state': fullDocument.state,
              'new_state': newState,
              'available_transitions': BPMClient.getAvailableStates(fullDocument.state),
            }));
          }

          // gatewayData is a function , call it to obtain the gatewayData Object
          let gatewayData = gatewayDataOrFunction;
          if (typeof gatewayDataOrFunction === 'function') {
            gatewayData = gatewayDataOrFunction(fullDocument);  // Call function to get "dynamic gateway"
          }

          fullDocument.gateway = gatewayData; // Attach metada data
          fullDocument.state = newState;  // Pass validation , then update!

          this.__update(fullDocument)
            .then(resolve, reject);
        }, reject);
    });
  }

  __update(fullDocument, overrideStateForWebhook) {
    return new Promise((resolve, reject) => {
      // Update updated time in the document
      fullDocument['update_time'] = (new Date()).toISOString(); // UPDATE TIME

      var client = DB.getConnection();
      client
        .replaceDocument(fullDocument._self, fullDocument, (err, replaced) => {
          if (err) {
            return reject(new errors.DatabaseError(
              'database error has ocurred when trying to update a payment document',
              new Error(err.body)
            ));
          }

          // Hook in this place , because the hook is raised when any change is made it
          this.sendWebHook(
            replaced.application,
            'payment_' + (overrideStateForWebhook || replaced.state),
            this.toJSON(replaced)
          );

          resolve(replaced);
        });
    });
  };

  /**
   * Partial Update the payment intention
   * @param {*} token Payment Token
   * @param {*} data Fields to update
   */
  updatePartial(token, data) {
    return new Promise((resolve, reject) => {
      try {
        // If user send, item_list, but is empty, throw error
        if (data && data.transaction && data.transaction.item_list &&
          data.transaction.item_list.items &&
          data.transaction.item_list.items.length == 0) {
          return reject(new errors.PeinauError(
            'ITEM_LIST_CAN_BE_EMPTY',
            'You need at least 1 items in the shopping cart'
          ));
        }

        // Check for validation
        const model = app.models.updatedIntention(data);
        if (model.isInvalid()) {
          return reject(new errors.InvalidModelError(
            model
          ));
        }

        this.getById(token)
          .then((payment) => {
            /*
            TODO: ASK this, why only in the create state can be edit???!!!
            // Only be able to edit in created state
            if (app.state !== 'created') {
              const err = new Error('PAYMENT_INTENTION_CANT_BE_UPDATED');
              err.type = err.message;
              err.description = 'A payment intention update is only permitted in created state';
              return reject(err);
            }
            */

            // Update only "some" fields (merge with data , not model)
            const dataToUpdate = model.toJSON();
            delete dataToUpdate._id; // Remove mongoose _id for correct merge;
            const mergedData = lodash.defaultsDeep(dataToUpdate, payment);

            // Set manually array's because defaultsDeep not work as 'expected'
            mergedData.transaction.item_list.items = model.transaction.item_list.items;

            this.__update(mergedData, 'edited')
              .then(resolve, reject);
          }, reject);
      } catch (ex) {
        return reject(new errors.UnknowError(
          'An error has ocurred when trying to update partial payment by id',
          ex
        ));
      }
    });
  };

  /**
   * Create a Generic HTML callback success payment
   * @param {Object} payment Payment Object
   * @param {Object} valuesToSendInQuery JSON Object with the values to send to merchant
   * @returns {String} HTML fragment
   * @memberof PaymentClient
   */
  createSuccessRedirectionPage(payment, valuesToSendInQuery) {
    return new Promise((resolve, reject) => {
      try {
        var values = {
          'payment_state': 'payment_paid',
          'payment_id': payment.id,
        };
        utils._extend(values, valuesToSendInQuery);

        var filePath = __dirname + '/../../payments/templates/payment_redirection.html';
        var html = fs.readFileSync(filePath, 'utf8');

        var template = Handlebars.compile(html);
        var result = template({
          'payment_redirectionURL': payment.redirect_urls.return_url,
          'payment_values': JSON.stringify(values),
        });

        resolve(result);
      } catch (ex) {
        return reject(new errors.UnknowError(
          'An error has ocurred trying to create the success redirect page',
          ex
        ));
      }
    });
  };

  /**
   * Create a Generic HTML callback failure payment
   * @param {Object} payment Payment Object
   * @param {Object} valuesToSendInQuery JSON Object with the values to send to merchant
   * @returns {String} HTML fragment
   * @memberof PaymentClient
   */
  createErrorRedirectionPage(payment, valuesToSendInQuery) {
    return new Promise((resolve, reject) => {
      try {
        var values = {
          'payment_state': 'payment_error',
          'payment_id': payment.id,
        };
        utils._extend(values, valuesToSendInQuery);

        var filePath = __dirname + '/../../payments/templates/payment_redirection.html';
        var html = fs.readFileSync(filePath, 'utf8');

        var template = Handlebars.compile(html);
        var result = template({
          'payment_redirectionURL': payment.redirect_urls.cancel_url,
          'payment_values': JSON.stringify(values),
        });

        resolve(result);
      } catch (ex) {
        return reject(new errors.UnknowError(
          'An error has ocurred trying to create the error redirect page',
          ex
        ));
      }
    });
  };

  /**
   * Internal Method , for host header extraction (useful for build HateOAS Link)
   * @param {any} req Http Request
   * @returns {String} Server mount path
   * @memberof PaymentClient
   */
  getBaseUrl(req) {
    // THERE IS NO WAY TO GET IF HTTP OR HTTPS FROM THE REQUEST OBJECT....
    // TRY , req.isSecure(), req.protocol, req.connection.encrypted...
    // THE 'ONLY' SOLUTION IS TO TRY TO GET THE ENVIRONMENT SELECTED
    var protocol = (app.get('is-dev-mode') ? 'http' : 'https');

    // API GATEWAY
    var prefix = '';
    if (app.getSettings().routePrefix) {
      prefix = app.getSettings().routePrefix;
    }
    var baseUrl = `${protocol}://${req.headers.host}${prefix}/payments`;

    return baseUrl;
  }
}

// Get all payments folder for fasting cache :P
const gatewaysSupported = {};
(() => {
  const path = require('path');
  const gatewaysPath = `${__dirname}/../../gateways`;
  if (!fs.existsSync(gatewaysPath)) {
    return {};
  }

  return fs
    .readdirSync(gatewaysPath)
    .filter((file) => {
      const fullPath = path.join(gatewaysPath, file);
      if (fs
        .statSync(fullPath)
        .isDirectory()) {
        gatewaysSupported[file] = {
          name: file.replace(/\.js/, ''),
          path: fullPath,
          client: 'gatewayClient',
        };
      }
    });
})();

const client = new PaymentClient();
module.exports = client;
