'use strict';
const core = require('peanut-restify');
const logger = require('peanut-restify/logger');
const CONFIG = require('peanut-restify/config');
const expr = require('peanut-restify/expressions');
const app = module.exports = core.app();

app.start = () => {
  // start the web server
  return app.listen(() => {
    const baseUrl = app.get('url').replace(/\/$/, '');

    logger.info(`Server listening at: ${baseUrl} [${app.getSettings().env}]`);

    // Swagger Docs extension is Loaded?
    expr.whenTrue(app.get('extension-swagger-docs'), () => {
      const explorerPath = app.get('extension-swagger-docs').path;
      logger.info(`Browse your REST API at ${baseUrl}${explorerPath}`);
    });

    app.emit('started', app.getServer());

    if (process.env['NODE_LOG_LEVEL']) {
      const level = process.env['NODE_LOG_LEVEL'].toLowerCase();
      logger.info(`Set default log level to: ${level}`);
      logger.setLevel(level);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
app.boot = () => {
  core
    .boot(app, {
      debug: (process.env.NODE_ENV === 'DEV' ? true : false),
      routePrefix: process.env.ROUTE_PREFIX,
    }, (err, result) => {
      // throw error if exists
      if (err) {
        app.emit('boot failed', err);
        throw err;
      }

      app.emit('booted');

      // ACTIVATE
      app.enableCORS();
      app.addCustomFormatters();
      app.addStandardPlugins();
      app.addJWTSecurity({
        enableApiKeys: true,
        publicKeyPath: `${__dirname}/config/certs/falabella_rsa.pub`,
      });
      app.addSwaggerDocs();
      app.discoverEndpoints();
      app.addHealthStatus();
      app.addPingEndpoint();

      expr.whenTrue(app.get('is-prod-mode'), () => {
        // Only connect to Kibana if PROD
        expr.whenTrue(CONFIG.exists('LOGGING_UDP_ENDPOINT'), () => {
          app.addUdpLoggerToWinston({
            port: CONFIG.get('LOGGING_UDP_PORT'),
            host: CONFIG.get('LOGGING_UDP_ENDPOINT'),
            packageJSON: `${__dirname}/../package.json`,
          });
        });

        // Only connect to socket commander if PROD
        expr.whenTrue(CONFIG.exists('SOCKET_TCP_HOST'), () => {
          app.enableSocketListener({
            host: CONFIG.get('SOCKET_TCP_HOST'),
            port: CONFIG.get('SOCKET_TCP_PORT'),
            packageJSON: `${__dirname}/../package.json`,
          });
        });
      });

      app.emit('loaded');

      // start the server if `$ node server/server.js`
      if (require.main === module) {
        app.start();
      }
    });
};

app.boot();
