'use strict';

/**
 * Gateway base for Gateway Providers client's
 */
class Localization {

  /**
   * Constructor
   */
  constructor() {
    this.locales = {
      'en-US': require('./../config/locales/en-US.json'),
    };
  }

  /**
   * Get Text for the localization
   */
  getText(locale, culture, defaultString) {
    const isoCulture = (culture || 'en-US');
    const text = this.locales[isoCulture][locale];
    if (!text) {
      if (defaultString) {
        return defaultString;
      }
      throw new Error(`LOCALIZED_LABEL_NOT_FOUND: ${locale}(${isoCulture})`);
    }

    return text;
  };

};

module.exports = new Localization();
