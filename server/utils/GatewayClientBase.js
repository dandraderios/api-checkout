'use strict';
/**
 * Gateway base for Gateway Providers client's
 */
class GatewayClientBase {
  /**
   * Get Additional Links for a creation payment
   */
  getAdditionalLinks() {
    return [];
  };

  /**
  * Retrieve the gateway resume for the specific gateway
  * @param {any} payment Payment Document
  * @param {any} gatewayData Gateway Data, sended in the change state step
  * @returns {GatewayResume} gateway resume
  * @memberof GatewayClient
  */
  getGatewayResume(payment, gatewayData) {
    throw new Error('NOT_IMPLEMENTED_EXCEPTION');
  }

  /**
   * Validate Model Against his legacy validation
   * @param {*} model Payment Model Data
   * @returns false if the model is Valid or an Error otherwise
   */
  isInvalid(model) {
    return false;
  };

};

module.exports = GatewayClientBase;
