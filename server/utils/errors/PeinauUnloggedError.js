'use strict';
const PeinauAbstractError = require('./PeinauAbstractError');

/**
 * Define a Generic Error
 */
class PeinauUnloggedError extends PeinauAbstractError {
  /**
   * Create a Peinau Generic Error
   * @param {String} errorCode Error code for the exception (useful for localization independency error)
   * @param {String} errorDescription Error message description for verbose description
   * @param {Object} metaData single object with additional data to attach in the exception
   */
  constructor(errorCode, errorDescription, metaData) {
    super(errorCode, errorDescription, metaData);
  }
}

module.exports = PeinauUnloggedError;
