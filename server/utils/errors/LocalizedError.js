'use strict';
const PeinauAbstractError = require('./PeinauAbstractError');
const logger = require('peanut-restify/logger');
const locale = require('./../Localization');

/**
 * Define a Generic Error
 */
class LocalizedError extends PeinauAbstractError {
  /**
   * Create a Database Error
   * @param {String} errorDescription Error message description for verbose description
   * @param {Error} error Error associated with the error
   */
  constructor(errorCode, metadata) {
    super(errorCode, errorCode, {});

    // Check
    if (!errorCode) {
      throw new Error('the parameter errorCode in LocalizedError cant be null');
    }

    // Add associated error
    this.message = locale.getText(`ERRORS.${errorCode}`, null, `Localization label not found: ERRORS.${errorCode}`);
    this['error_description'] = this.message;
    this['meta_data'] = metadata;

    logger.error(this.message);
    logger.debug(this);
  }
}

module.exports = LocalizedError;
