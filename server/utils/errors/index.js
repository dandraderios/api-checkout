'use strict';

const PeinauError = require('./PeinauError');
const InvalidModelError = require('./InvalidModelError');
const DatabaseError = require('./DatabaseError');
const UnknowError = require('./UnknowError');
const PeinauUnloggedError = require('./PeinauUnloggedError');
const DocumentNotFoundError = require('./DocumentNotFoundError');
const LocalizedError = require('./LocalizedError');

module.exports = {
  PeinauError: PeinauError,
  InvalidModelError: InvalidModelError,
  DatabaseError: DatabaseError,
  UnknowError: UnknowError,
  PeinauUnloggedError: PeinauUnloggedError,
  DocumentNotFoundError: DocumentNotFoundError,
  LocalizedError: LocalizedError,
};
