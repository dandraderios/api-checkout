'use strict';

module.exports = {
  getConnection: () => {
    throw new Error('YOU MUST OVERRIDE getConnection METHOD FOR TESTING');
  },
};
