'use strict';

module.exports = {
  post: () => {
    throw new Error('YOU MUST OVERRIDE request POST FOR TESTING');
  },
  get: () => {
    throw new Error('YOU MUST OVERRIDE request GET FOR TESTING');
  },
};
