'use strict';

module.exports = {
  /* eslint-disable max-len */
  setEnvironment: () => {
    // GENERAL PROPERTIES
    process.env.NODE_PORT = '8700';
    process.env.NODE_ENV = 'TEST';
    process.env.ROUTEPREFIX = '';

    // DATASOURCE CONFIGURATION
    process.env.DATASOURCE_TYPE = 'documentDB';
    process.env.DATASOURCE_CONNECTION_STRING = 'AccountEndpoint=https://app-sso-quickpay.documents.azure.com:443/;AccountKey=YX0BcnTGiIUy62QvUtE6o98lj88I9DsY9oKCmhGUSBxspyOCoxMJ6S7WSlTUwE0ohLzdPosC24NNnqOQkYsF3A==;';

    // AZURE STORAGE CONFIGURATION
    process.env.AZURE_STORAGE_CONNECTION_STRING = 'DefaultEndpointsProtocol=https;AccountName=quickpaystorage;AccountKey=io3x3tQ/cE7h6+OiWCLLSTHaC4BXYQTJRb3tO3oNlS14rt5gn/XI51+Xn95UGB/4zidHOdrp69NrLJZJTwUhfQ==;EndpointSuffix=core.windows.net';

    // GOOGLE OAUTH FLOW CONFIGURATION
    process.env.GOOGLE_GMAIL_CLIENT_ID = '908413886979-ketdl3mq3lhukj26ci0sapsi32moof01.apps.googleusercontent.com';
    process.env.GOOGLE_GMAIL_CLIENT_SECRET = 'ipfMFqFbZCDSbWGAwKJamoGz';
    process.env.GOOGLE_GMAIL_TOKEN_URL = 'https://www.googleapis.com/oauth2/v4/token';
    process.env.GOOGLE_GMAIL_DIALOG_URL = 'https://accounts.google.com/o/oauth2/v2/auth';

    // LOGGING ENDPOINTS
    process.env.LOGGING_UDP_ENDPOINT = 'weblog.sandbox.connect.fif.tech';
    process.env.LOGGING_UDP_PORT = '2290';

    // REDIS SOCKET LISTENER
    process.env.SOCKET_TCP_HOST = 'localhost';
    process.env.SOCKET_TCP_PORT = 6379;

    // GATEWAYS_CONFIGURATION: CARDBILL PAYMENTS DEBIT BF
    process.env.GATEWAY_TRANSBANK_CARDBILL_PAYMENT_DEBIT_BF_CREATE_ENDPOINT = 'https://quickpay-qa.fif.tech/api/controller/v1/operations';
    process.env.GATEWAY_TRANSBANK_CARDBILL_PAYMENT_DEBIT_BF_STATUS_ENDPOINT = 'https://api.sandbox.connect.fif.tech/operationStatus/v1/operation';
    process.env.GATEWAY_TRANSBANK_CARDBILL_PAYMENT_DEBIT_BF_API_TOKEN = 'xxXUk6vOXUGW6Kd21VVwUVm1yDMKyxuM';
    process.env.GATEWAY_QUICKPAY_TOKEN_ENDPOINT = 'http://localhost:8090/captures';
    process.env.GATEWAY_QUICKPAY_TOKEN_MASTERKEY = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcmltYXJ5c2lkIjoiMWY5ZDUxYjItZGRjOC0zYjQ1LWRlZDItOGU3MzMwYTBkOWViIiwidW5pcXVlX25hbWUiOiJQZWFudXQgSHViIEUtY29tbWVyY2UiLCJncm91cHNpZCI6IkFQUEwiLCJpc3MiOiJGYWxhYmVsbGEiLCJhdWQiOiJTZXJ2ZXIiLCJ0eXBlIjoiQXBpS2V5Iiwic2NvcGUiOltdLCJpYXQiOjE1MTE4OTIyMTUsImV4cCI6MTU3NTAwNzQxNX0.VuZcviQAYnCfLAU4MSUXoQinYPy8gwrFO0jmAg8F_xB2GsQm2DosuJ-6GqTiNXzJa5Q1j7p-vT9YI3-1B9gxiWaiOPuhehUGTprpCbUoXJ93bZO7HIgU0Dx2aZyfq7u9Z6xWEv7k0DXtbTrpii3s2bJ_I8BBdmUGlAFDKb14wtWde65TR-mZH-tlPvYnOhJxkhE3j1_KilU7ONw2bkEFwiq4-VU6m_Qn7bL_WIy_QSuaso51AIa_cP7JiiBgzKhJyZPIP70M32Q6xm8K-c7d-AiyWf-d82j9EKY9wBRBz_bslLbUlFC-O0NeW2k1TNmNQydbCBQy5i67xmmCE3vA-g';
    process.env.GATEWAY_CYBERSOURCE_TOKEN_ENDPOINT = 'https://ics2wstesta.ic3.com/commerce/1.x/transactionProcessor';
    process.env.GATEWAY_CYBERSOURCE_TIMEOUT = 50;
    process.env.GATEWAY_CMR_POINTS_ENDPOINT = 'https://wp.falabella.cl/cmrwpswpr/servlet/cmr';
    process.env.GATEWAY_QUICKPAY_CREDIT_ENDPOINT = 'https://quickpay-integracion.falabella.com/ui/quickpay/quickframe/';
    process.env.GATEWAY_QUICKPAY_DEBIT_ENDPOINT = 'https://quickpay-integracion.falabella.com/ui/quickpay/quickframe/';
    process.env.GATEWAY_SELF_CHECKOUT_LIST_CARD_ENDPOINT = 'https://api.sandbox.connect.fif.tech/customers/v1/consultaCuposTarjetas?apikey=xxXUk6vOXUGW6Kd21VVwUVm1yDMKyxuM';
    process.env.GATEWAY_TRANSBANK_WEBPAY_ENVIRONMENT = 'PRODUCTION';
    process.env.GATEWAY_TRANSBANK_WEBPAY_ENDPOINT = 'DUMMY_ENDPOINT';
    // NOTIFICATION CONFIGURATION
    process.env.NOTIFICATION_ENDPOINT = 'http://localhost:8085/';
    process.env.NOTIFICATION_AUTH_KEY = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcmltYXJ5c2lkIjoiZjkzZTg0MWUtMGI0OC0xNjk3LTc5ZWUtYjc2NmE4MDI5NDI5IiwidW5pcXVlX25hbWUiOiJQZWFudXQgSHViIEUtY29tbWVyY2UiLCJncm91cHNpZCI6IkFQUEwiLCJpc3MiOiJGYWxhYmVsbGEiLCJhdWQiOiJTZXJ2ZXIiLCJ0eXBlIjoiQXBpS2V5Iiwic2NvcGUiOltdLCJpYXQiOjE1MTAwMjg2NjcsImV4cCI6MTU3MzE0Mzg2N30.rn7_5zT_VA1T9KuPKLsVWzY79Pihr-13TWoPo27QIh-9_OmGRi9PLj1H3IzDElDLxxqITT2RDKK0Vrsn9EyCiofyR4gnFWnyI7QNUZW9Xvx2FU1PDga9Z3UNop1PdP9iqBUxbIBWa1JH-VAKeBxVKzQVRFOJ54TXukO18dzF_S8D7nZk0j--QfI291yaJdtP51AkJ0HVkiCsiZjrcWjz9m4qWP9u9uOC8yBdRdWbK9o_r94kQUMaxQBF26yAcfZpkeG2_piwKbztdFnLGR26D0Sxq3XEDdQlvWEImrK13R9InAhePrR1Z3JtmUAwmrZGZ07EP58fRsVkbUSpBcCnUw';
  },
  getValidJwt: () => {
    return 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcmltYXJ5c2lkIjoiYTBkMmJjYTUtMzhhOS0yMDE2LTJlNTQtNTA0MzJhYWEwMGM2IiwidW5pcXVlX25hbWUiOiJEYXZpZCBNdcOxb3oiLCJlbWFpbCI6ImRhdmlkLm11bm96QHBlYW51dGh1Yi5jbCIsImNvdW50cnkiOiJDTCIsImF2YXRhciI6Imh0dHBzOi8vbGg0Lmdvb2dsZXVzZXJjb250ZW50LmNvbS8tTUExNmloMzdfc0EvQUFBQUFBQUFBQUkvQUFBQUFBQUFBQmMvUHZqaFAtSVB2Uncvczk2LWMvcGhvdG8uanBnIiwiZ3JvdXBzaWQiOiJVU0VSIiwiaXNzIjoiRmFsYWJlbGxhIiwidHlwZSI6IkJlYXJlciIsImF1ZCI6IldlYiIsInNjb3BlIjpbXSwiaWF0IjoxNTEzMDA1OTUwLCJleHAiOjE4MTMwOTIzNTB9.w-e0ZbwYl0zijI9xePZ49EpuIeHXxantDLcKiqNPjYut856VtViAIIrt46-gMZILyTwlwDbJwmlTouilkQU94K7S4CIgOjZdTdk8ZdI0HmkIB7AoDT7mYsGIjdjSnDu3HcLLpr95bCN-6tG-QZ5J4x5xU6jp3uvKGHIE0NBNI-Z7NxtU4PXMb0KoBx3qsDih0nz8-Ym5p-4HR7WdB-eMPnEjA79AaCCHVEXcBJUy10X9p2THv4TnNqgL6s4DJq2VfzhQeiIu_Cuca17wkNTE4jL9XrryqmrU7l9soqFWrfms8ApI4o1oX9wTjcMgV_A3D93-I0hrwqz12-pBCNXB-A';
  },
};
