'use strict';

module.exports = {
  VALID_GATEWAY_CARDBILL_PAYMENT_DEBIT_BF() {
    return {
      'client_id': '744261341508722449548',
      'enabled': true,
      'merchantAuthKey': 'f1f_fa1ab311aQQQ',
      'merchantId': '009580120',
      'branchId': '101',
      'terminalId': '101',
      'commerceCode': '101',
      'channel': 'WEB',
      'application': '1f9d51b2-ddc8-3b45-ded2-8e7330a0d9eb',
      'payment_method': 'CARDBILL_PAYMENT_DEBIT_BF',
      'id': '7c33f2c9-fe53-500e-2619-74423bff3031',
      'commerceCodeDebit': '003',
      'commerceCodeCmr': '10001804',
      '_rid': 'QloVAM86GwEgAAAAAAAAAA==',
      '_self': 'dbs/QloVAA==/colls/QloVAM86GwE=/docs/QloVAM86GwEgAAAAAAAAAA==/',
      '_etag': '\'49004072-0000-0000-0000-5a3d51180000\'',
      '_attachments': 'attachments/',
      '_ts': 1513967952,
    };
  },
  VALID_GATEWAY_QUICKPAY_CREDIT() {
    return {
      'client_id': '744261341508722449548',
      'enabled': true,
      'merchantAuthKey': 'd37364233286541fc0436381040fcb13d27f34afaf477a02ae67be4dc12ddc67',
      'merchantId': '009884664',
      'branchId': '401',
      'terminalId': '700',
      'commerceCode': '10001805',
      'channel': 'WEB',
      'localCode': '402',
      'terminalCode': '700',
      'application': '1f9d51b2-ddc8-3b45-ded2-8e7330a0d9eb',
      'payment_method': 'QUICKPAY_CREDIT',
      'id': '7eb373fc-b316-6162-1566-fc43bbdd01ee',
      '_rid': 'QloVAM86GwEfAAAAAAAAAA==',
      '_self': 'dbs/QloVAA==/colls/QloVAM86GwE=/docs/QloVAM86GwEfAAAAAAAAAA==/',
      '_etag': '\'49004072-0000-0000-0000-5a3d51180000\'',
      '_attachments': 'attachments/',
      '_ts': 1513967896,
    };
  },
  VALID_TRANSBANK_WEBPAY() {
    return {
      'enabled': true,
      'application': '13a8ecb2-eafb-a3a5-f23d-7647ba2cc381',
      'payment_method': 'TRANSBANK_WEBPAY',
      'commerceCode': 597020000541,
      'PUBLIC_KEY': '-----BEGIN CERTIFICATE-----\nMIIDujCCAqICCQCZ42cY33KRTzANBgkqhkiG9w0BAQsFADCBnjELMAkGA1UEBhMC\nQ0wxETAPBgNVBAgMCFNhbnRpYWdvMRIwEAYDVQQKDAlUcmFuc2JhbmsxETAPBgNV\nBAcMCFNhbnRpYWdvMRUwEwYDVQQDDAw1OTcwMjAwMDA1NDExFzAVBgNVBAsMDkNh\nbmFsZXNSZW1vdG9zMSUwIwYJKoZIhvcNAQkBFhZpbnRlZ3JhZG9yZXNAdmFyaW9z\nLmNsMB4XDTE2MDYyMjIxMDkyN1oXDTI0MDYyMDIxMDkyN1owgZ4xCzAJBgNVBAYT\nAkNMMREwDwYDVQQIDAhTYW50aWFnbzESMBAGA1UECgwJVHJhbnNiYW5rMREwDwYD\nVQQHDAhTYW50aWFnbzEVMBMGA1UEAwwMNTk3MDIwMDAwNTQxMRcwFQYDVQQLDA5D\nYW5hbGVzUmVtb3RvczElMCMGCSqGSIb3DQEJARYWaW50ZWdyYWRvcmVzQHZhcmlv\ncy5jbDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANApVXB/EQtbviqQ\nj1J82EiHJslyPO0RLAZEM2gNUCXuMPwe4OirylfQ2qn5zYTL3ff8qZhn9EQ0D4ZQ\nWxpc8pis9cYdJUAQ/vTGa4PCbvP3dZNKSG9UC0A54UYEdk9eJ4F6T+DyECrauw7H\nRwcmxVOb7wClanR7yJmRc6nsW2Y11scYU/v7BnA+FbOu933Ogfl49lKyhe0MleaT\nA6tBwgi0zJmn1gJjax8peopkDPm6gtdoJxBABJKyYkjuj/9tZvYgybOSkGp6SW8t\nGhlvJqHGERwzB2Y7U4iD9PiQCYC/SrB/q13ZNm0+2nq3u08ziv2xMIZGBF5zDLf+\nub1egH8CAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAdgNpIS2NZFx5PoYwJZf8faze\nNmKQg73seDGuP8d8w/CZf1Py/gsJFNbh4CEySWZRCzlOKxzmtPTmyPdyhObjMA8E\nAdps9DtgiN2ITSF1HUFmhMjI5V7U2L9LyEdpUaieYyPBfxiicdWz2YULVuOYDJHR\nn05jlj/EjYa5bLKs/yggYiqMkZdIX8NiLL6ZTERIvBa6azDKs6yDsCsnE1M5tzQI\nVVEkZtEfil6E1tz8v3yLZapLt+8jmPq1RCSx3Zh4fUkxBTpUW/9SWUNEXbKK7bB3\nzfB3kGE55K5nxHKfQlrqdHLcIo+vdShATwYnmhUkGxUnM9qoCDlB8lYu3rFi9w==\n-----END CERTIFICATE-----',
      'PRIVATE_KEY': '-----BEGIN RSA PRIVATE KEY-----\nMIIEpQIBAAKCAQEA0ClVcH8RC1u+KpCPUnzYSIcmyXI87REsBkQzaA1QJe4w/B7g\n6KvKV9DaqfnNhMvd9/ypmGf0RDQPhlBbGlzymKz1xh0lQBD+9MZrg8Ju8/d1k0pI\nb1QLQDnhRgR2T14ngXpP4PIQKtq7DsdHBybFU5vvAKVqdHvImZFzqexbZjXWxxhT\n+/sGcD4Vs673fc6B+Xj2UrKF7QyV5pMDq0HCCLTMmafWAmNrHyl6imQM+bqC12gn\nEEAEkrJiSO6P/21m9iDJs5KQanpJby0aGW8mocYRHDMHZjtTiIP0+JAJgL9KsH+r\nXdk2bT7aere7TzOK/bEwhkYEXnMMt/65vV6AfwIDAQABAoIBAHnIlOn6DTi99eXl\nKVSzIb5dA747jZWMxFruL70ifM+UKSh30FGPoBP8ZtGnCiw1ManSMk6uEuSMKMEF\n5iboVi4okqnTh2WSC/ec1m4BpPQqxKjlfrdTTjnHIxrZpXYNucMwkeci93569ZFR\n2SY/8pZV1mBkZoG7ocLmq+qwE1EaBEL/sXMvuF/h08nJ71I4zcclpB8kN0yFrBCW\n7scqOwTLiob2mmU2bFHOyyjTkGOlEsBQxhtVwVEt/0AFH/ucmMTP0vrKOA0HkhxM\noeR4k2z0qwTzZKXuEZtsau8a/9B3S3YcgoSOhRP/VdY1WL5hWDHeK8q1Nfq2eETX\njnQ4zjECgYEA7z2/biWe9nDyYDZM7SfHy1xF5Q3ocmv14NhTbt8iDlz2LsZ2JcPn\nEMV++m88F3PYdFUOp4Zuw+eLJSrBqfuPYrTVNH0v/HdTqTS70R2YZCFb9g0ryaHV\nTRwYovu/oQMV4LBSzrwdtCrcfUZDtqMYmmZfEkdjCWCEpEi36nlG0JMCgYEA3r49\no+soFIpDqLMei1tF+Ah/rm8oY5f4Wc82kmSgoPFCWnQEIW36i/GRaoQYsBp4loue\nvyPuW+BzoZpVcJDuBmHY3UOLKr4ZldOn2KIj6sCQZ1mNKo5WuZ4YFeL5uyp9Hvio\nTCPGeXghG0uIk4emSwolJVSbKSRi6SPsiANff+UCgYEAvNMRmlAbLQtsYb+565xw\nNvO3PthBVL4dLL/Q6js21/tLWxPNAHWklDosxGCzHxeSCg9wJ40VM4425rjebdld\nDF0Jwgnkq/FKmMxESQKA2tbxjDxNCTGv9tJsJ4dnch/LTrIcSYt0LlV9/WpN24LS\n0lpmQzkQ07/YMQosDuZ1m/0CgYEAu9oHlEHTmJcO/qypmu/ML6XDQPKARpY5Hkzy\ngj4ZdgJianSjsynUfsepUwK663I3twdjR2JfON8vxd+qJPgltf45bknziYWvgDtz\nt/Duh6IFZxQQSQ6oN30MZRD6eo4X3dHp5eTaE0Fr8mAefAWQCoMw1q3m+ai1PlhM\nuFzX4r0CgYEArx4TAq+Z4crVCdABBzAZ7GvvAXdxvBo0AhD9IddSWVTCza972wta\n5J2rrS/ye9Tfu5j2IbTHaLDz14mwMXr1S4L39UX/NifLc93KHie/yjycCuu4uqNo\nMtdweTnQt73lN2cnYedRUhw9UTfPzYu7jdXCUAyAD4IEjFQrswk2x04=\n-----END RSA PRIVATE KEY-----',
      'id': '9172af78-938d-81f6-7677-87cab55f52a2',
      '_rid': 'QloVAM86GwEIAAAAAAAAAA==',
      '_self': 'dbs/QloVAA==/colls/QloVAM86GwE=/docs/QloVAM86GwEIAAAAAAAAAA==/',
      '_etag': '\'040053e9-0000-0000-0000-59f1036d0000\'',
      '_attachments': 'attachments/',
      '_ts': 1508967277,
    };
  },
  VALID_GATEWAY_QUICKPAY_TOKEN() {
    return {
      'enabled': true,
      'merchant': 'falabella2',
      'webServiceUsername': 'falabella2',
      'webServicePassword': '5hQftD1hdpkjCbmXUS0cG/oMd99jzr4x9MzGZDjPCSwOGUVY8cBb53on1JBKTWpRs3TVaRxOUDpncP2lar9JDhTngG3P/iYzXznKv39N/zEjku8Im7n+jTdS2oYVjZNfvYKzyAgXtZQvqd1txbYdB3/cTqG/mC8ukE76V78lxhreHWWv/pqKdE+PqioarRMZ5EVyPtEFXOv1WmIDiwgmUGc4le58JSjDIYvFfEH44atJ8zXhBbudYpLcO4T4Rvh4DFc2/IXgttm6/HU0OubnzY4TO8kD46QJU7Yvqm6uM/2Sk27NbWPPqyhzEZgLHK1SzeCHgJ8sWJAOvBbL2o0zWA==',
      'decisionManager_enabled': true,
      'decisionManager_threshold': 100,
      'application': '1f9d51b2-ddc8-3b45-ded2-8e7330a0d9eb',
      'payment_method': 'QUICKPAY_TOKEN',
      'quickpay_token': '494aA161D561',
      'id': 'ab408715-bb20-e599-cd1b-97fb36ae4c25',
      'client_id': '744261341508722449548',
      '_rid': 'QloVAM86GwEGAAAAAAAAAA==',
      '_self': 'dbs/QloVAA==/colls/QloVAM86GwE=/docs/QloVAM86GwEGAAAAAAAAAA==/',
      '_etag': '\'49004372-0000-0000-0000-5a3d51a00000\'',
      '_attachments': 'attachments/',
      '_ts': 1513968032,
      'capture_token': '58d191c8-66d8-75bb-5c85-281ffb1a4068',
    };
  },
  VALID_GATEWAY() {
    return {
      'payment_flow': 'with_token',
      'installments_number': '71',
      'merchantReferenceCode': 'INPA-50000001220',
      'requestID': '5144077587426532204007',
      'decision': 'ACCEPT',
      'reasonCode': '100',
      'requestToken': 'Ahj//wSTFtxqz0X23R3niiDBlYjWmcWPKgzaTaNUnpb43rpNwClvjeuk3EwNjKOYMMmkmXoxXL8sBgTkxbcas9F9t0d5wAAAxSAp',
      'purchaseTotals': {
        'currency': 'CLP',
      },
      'ccAuthReply': {
        'reasonCode': '100',
        'amount': '4500',
        'authorizationCode': '570110',
        'avsCode': '1',
        'authorizedDateTime': '2017-12-27T20:49:19Z',
        'processorResponse': '1',
        'reconciliationID': '02XFZ3EGJAMR6FTO',
        'paymentNetworkTransactionID': '111222',
        'ownerMerchantID': 'falabella2',
        'processorTransactionID': '49cde21b223749c58dafdbb1c4251787',
      },
      'ccCaptureReply': {
        'reasonCode': '100',
        'requestDateTime': '2017-12-27T20:49:19Z',
        'amount': '4500',
        'reconciliationID': '02XFZ3EGJAMR6FTO',
      },
      'additionalProcessorResponse': '5f711c59-3b6a-45d2-aca1-2c2890523988',
      'resume': {
        '_id': '5a44074f1f1acf000fd544b1',
        'card_number': {
          'pan_last4': 1111,
          'pan_first6': 411111,
        },
        'authorizations': {
          'code': '570110',
        },
        'transaction': {
          'gateway_id': '5144077587426532204007',
          'type': 'CREDIT',
          'date': '2017-12-27T20:49:19.189Z',
          'currency': 'CLP',
          'buy_order': 'INPA-50000001220',
          'amount': 4500,
          'installments_number': 1,
        },
        'response': {
          'code': 100,
        },
      },
      'capture_token': '58d191c8-66d8-75bb-5c85-281ffb1a4068',
    };
  },
  VALID_GATEWAY_SELF_CHECKOUT() {
    return {
      'client_id': '641281901508761220281',
      'enabled': true,
      'merchant_id': '008598176',
      'request_channel': 'QuickPayCore',
      'merchant_channel': 'WEB',
      'branch_id': '101',
      'terminal_id': '101',
      'secret_shared_key': '43qyu3qwjaw8ga5azbro00ig',
      'application': '28adb999-7a2e-70b8-c092-e4c16a9e9e0a',
      'payment_method': 'SELF_CHECKOUT',
      '_id': '5a3d189dfb92f9000f86c045',
      'id': '0e9d5b5d-a6ea-6718-d181-bfb247f5bd21',
      '_rid': 'QloVAM86GwFJAAAAAAAAAA==',
      '_self': 'dbs/QloVAA==/colls/QloVAM86GwE=/docs/QloVAM86GwFJAAAAAAAAAA==/',
      '_etag': '\'49007771-0000-0000-0000-5a3d189d0000\'',
      '_attachments': 'attachments/',
      '_ts': 1513953437,
    };
  },
};
