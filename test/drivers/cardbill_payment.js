'use strict';

module.exports = {
  INVALID_MODEL() {
    return {};
  },
  VALID_MODEL() {
    return {
      'application': '91468344-801d-e74f-82d7-8f0269b57176',
      'additional_attributes': {
      },
      'payer': {
        'payer_info': {
          'documentNumber': '253798858',
          'documentType': 'RUT',
          'full_name': 'Gustavo Gustavo',
          'email': 'dummy_user6@gmail.com',
        },
        'payment_method': 'CARDBILL_PAYMENT_DEBIT_BF',
      },
      'intent': 'sale',
      'links': [],
      'redirect_urls': {
        'return_url': 'https://ebctest.cybersource.com/',
        'cancel_url': 'http://localhost:8000',
      },
      'last_four_digits': 1234,
      'card_bin': 123456,
    };
  },
  PAID_OBJECT() {
    return {
      'intent': 'sale',
      'application': 'f93e841e-0b48-1697-79ee-b766a8029429',
      'redirect_urls': {
        'return_url': 'http://localhost:8000',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'thumbnail': 'http://localhost:8000/bundles/app/css/images/e-commerce-demo/product-icon.png',
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '5a022b43695c440fbf48c7c8',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 1000,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'documentType': 'RUT',
          'documentNumber': '107872388',
          'country': 'CL',
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'CARDBILL_PAYMENT_DEBIT_BF',
      },
      'links': [],
      'id': 'a5187413-1fa8-b998-c047-91e7a95c3661',
      'create_time': '2017-11-07T21:53:10.642Z',
      'update_time': '2017-11-07T21:54:15.754Z',
      'state': 'paid',
      'invoice_number': 'INPA-0000002313',
      'gateway': {
        'msgType': 'CardBillResponse',
        'responseCode': 0,
        'responseDescription': 'Success',
        'responseSignature': '',
        'payerUserProfile': {
          'country': 'CL',
          'documentType': 'RUT',
          'documentId': '367845414',
          'category': 'Normal',
          'isEmploye': false,
        },
        'logTrackId': '#LGID=c839268d2ad900e4a5e36907098b144a#MID=009580120#CHID=WEB#PYMT=D#DT=RUT#DID=139405919#UUID=7b2589e2-bd56-4fe7-8bca-43d47f45ce9e#TID=INPA-0000002313#',
        'qpTxnId': 2436249,
        'authorizationCode': '168817482026',
        'amount': 1000,
      },
      '_rid': 'C5Y3AKyhegANCQAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegANCQAAAAAAAA==/',
      '_etag': '\'0000f639-0000-0000-0000-5a022b880000\'',
      '_attachments': 'attachments/',
      '_ts': 1510091656,
    };
  },
  CREATED_OBJECT() {
    return {
      'intent': 'sale',
      'additional_attributes': {
        'last_four_digits': 3038,
        'card_bin': 548740,
      },
      'application': 'f93e841e-0b48-1697-79ee-b766a8029429',
      '_id': '5a0328dea1ffc65735cf0569',
      'redirect_urls': {
        'return_url': 'http://localhost:8000',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'thumbnail': 'http://localhost:8000/bundles/app/css/images/e-commerce-demo/product-icon.png',
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '5a0328dea1ffc65735cf056a',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 1000,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'documentType': 'RUT',
          'documentNumber': '367845414',
          'country': 'CL',
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'CARDBILL_PAYMENT_DEBIT_BF',
      },
      'links': [],
      'id': '43c89a60-8bab-8f1a-d67d-b699fbb1a9c5',
      'create_time': '2017-11-08T15:55:14.899Z',
      'update_time': '2017-11-08T15:55:14.899Z',
      'state': 'created',
      'invoice_number': 'INPA-0000002367',
      '_rid': 'C5Y3AKyhegBDCQAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegBDCQAAAAAAAA==/',
      '_etag': '\'0000e685-0000-0000-0000-5a0328e20000\'',
      '_attachments': 'attachments/',
      '_ts': 1510156514,
    };
  },
  REJECTED_PAYMENT_OBJECT() {
    return {
      'intent': 'sale',
      'additional_attributes': {
        'last_four_digits': 3038,
        'card_bin': 548740,
      },
      'application': 'f93e841e-0b48-1697-79ee-b766a8029429',
      'redirect_urls': {
        'return_url': 'http://localhost:8000',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'thumbnail': 'http://localhost:8000/bundles/app/css/images/e-commerce-demo/product-icon.png',
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '5a559daf8bab1b3104a3a465',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 1000,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'document_type': 'RUT',
          'document_number': '367845414',
          'country': 'CL',
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'CARDBILL_PAYMENT_DEBIT_BF',
      },
      'links': [],
      'id': '938af7d5-d409-bbd2-fb59-38a10a679a00',
      'create_time': '2018-01-10T04:59:27.566Z',
      'update_time': '2018-01-10T04:59:59.766Z',
      'state': 'rejected',
      'invoice_number': 'INPA-0000003375',
      'gateway': {
        'msgType': 'CardBillPaymentResponse',
        'responseCode': 28,
        'responseDescription': 'Error communicating with the ESB',
        'UUID': 'c1524c1f-ff5a-4041-bd84-4e1994c2f06a',
        'transactionId': 'c1524c1f-ff5a-4041-bd84-4e1994c2f06a',
      },
      '_rid': 'C5Y3AKyhegAzDQAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegAzDQAAAAAAAA==/',
      '_etag': '\'0500a8bc-0000-0000-0000-5a559dd00000\'',
      '_attachments': 'attachments/',
      '_ts': 1515560400,
    };
  },
  isValidModel: (document) => {
    expect(document).toBeDefined();
    expect(document).toHaveProperty('application');
    expect(document).toHaveProperty('additional_attributes');
    expect(document).toHaveProperty('payer');
    expect(document.payer).toHaveProperty('payer_info');
    expect(document.payer).toHaveProperty('payment_method');
    expect(document).toHaveProperty('intent');
    expect(document).toHaveProperty('links');
    expect(document).toHaveProperty('redirect_urls');
    expect(document).toHaveProperty('invoice_number');
    expect(document).toHaveProperty('transaction');
    expect(document.transaction).toHaveProperty('item_list');
    expect(document.transaction).toHaveProperty('amount');
    expect(document.transaction).toHaveProperty('soft_descriptor');
    expect(document).toHaveProperty('state');
  },
};
