'use strict';

module.exports = {
  INVALID_MODEL() {
    return {};
  },
  VALID_MODEL() {
    return {
      'application': '91468344-801d-e74f-82d7-8f0269b57176',
      'additional_attributes': {
        'user_identifier': '',
        'ordinal_number': 1519228822977,
        'currency_code': '152',
        'document_type': '1',
        'product_family': '002',
        'ean': '9990086',
        'total_points': 4000,
      },
      'payer': {
        'payer_info': {
          'documentNumber': '253798858',
          'documentType': 'RUT',
          'full_name': 'Gustavo Gustavo',
          'email': 'dummy_user6@gmail.com',
        },
        'payment_method': 'QUICKPAY_TOKEN',
      },
      'intent': 'sale',
      'links': [],
      'redirect_urls': {
        'return_url': 'https://ebctest.cybersource.com/',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'item_list': {
          'items': [
            {
              'price': 500,
              'name': 'Destornillador 2344',
              'tax': 0,
              'description': 'Destornillador 2344',
              'quantity': 3,
              'sku': '1231232',
              'thumbnail': '',
            },
          ],
          'shipping_address': {
            'country_code': 'CL',
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'phone': '+56 9 4578 8862',
            'recipient_name': 'Alejandro David',
            'type': 'HOME_OR_WORK',
          },
          'shipping_method': 'DIGITAL',
        },
        'amount': {
          'currency': 'CLP',
          'total': 2000,
          'details': {
            'shipping_discount': 0,
            'shipping': 500,
            'tax': 0,
            'subtotal': 1500,
          },
        },
        'soft_descriptor': 'Transaction Short description',
        'description': 'description',
      },
    };
  },
  CREATED_OBJECT() {
    return {
      'application': '91468344-801d-e74f-82d7-8f0269b57176',
      'additional_attributes': {
        'capture_token': 'a669a9a0-3cde-cf21-7f92-3b066d71855e',
      },
      'meta_data': {
        'thumbnail': 'dd',
      },
      'create_time': '2017-12-04T19:02:05.558Z',
      'payer': {
        'payer_info': {
          'documentNumber': '253798858',
          'documentType': 'RUT',
          'full_name': 'Gustavo Gustavo',
          'email': 'dummy_user6@gmail.com',
          'country': 'CL',
        },
        'payment_method': 'QUICKPAY_TOKEN',
      },
      'update_time': '2017-12-04T19:02:05.558Z',
      'intent': 'sale',
      'links': [],
      'redirect_urls': {
        'return_url': 'https://ebctest.cybersource.com/',
        'cancel_url': 'http://localhost:8000',
      },
      'invoice_number': 'INPA-50000000847',
      'id': '31109ad9-971f-5eba-5a7c-7a71f7ffc544',
      'transaction': {
        'description': 'Transaction detailed description',
        'item_list': {
          'items': [
            {
              'price': 500,
              'name': 'Destornillador 2344',
              'tax': 0,
              'description': 'Destornillador 2344',
              'quantity': 3,
              'sku': '1231232',
              'thumbnail': '',
            },
          ],
          'shipping_address': {
            'country_code': 'CL',
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'phone': '+56 9 4578 8862',
            'recipient_name': 'Alejandro David',
            'type': 'HOME_OR_WORK',
          },
          'shipping_method': 'DIGITAL',
        },
        'amount': {
          'currency': 'CLP',
          'total': 2000,
          'details': {
            'shipping_discount': 0,
            'shipping': 500,
            'tax': 0,
            'subtotal': 1500,
          },
        },
        'soft_descriptor': 'Transaction Short description',
      },
      'state': 'created',
    };
  },
  PAID_OBJECT() {
    const created = this.CREATED_OBJECT();
    created.state = 'paid';
    return created;
  },
  OBJECT_WITH_ERROR_CODE() {
    return {
      'error_code': '500',
      'application': '91468344-801d-e74f-82d7-8f0269b57176',
      'additional_attributes': {
        'capture_token': 'a669a9a0-3cde-cf21-7f92-3b066d71855e',
      },
      'create_time': '2017-12-04T19:02:05.558Z',
      'payer': {
        'payer_info': {
          'documentNumber': '253798858',
          'documentType': 'RUT',
          'full_name': 'Gustavo Gustavo',
          'email': 'dummy_user6@gmail.com',
          'country': 'CL',
        },
        'payment_method': 'QUICKPAY_TOKEN',
      },
      'update_time': '2017-12-04T19:02:05.558Z',
      'intent': 'sale',
      'links': [],
      'redirect_urls': {
        'return_url': 'https://ebctest.cybersource.com/',
        'cancel_url': 'http://localhost:8000',
      },
      'invoice_number': 'INPA-50000000847',
      'id': '31109ad9-971f-5eba-5a7c-7a71f7ffc544',
      'transaction': {
        'description': 'Transaction detailed description',
        'item_list': {
          'items': [
            {
              'price': 500,
              'name': 'Destornillador 2344',
              'tax': 0,
              'description': 'Destornillador 2344',
              'quantity': 3,
              'sku': '1231232',
              'thumbnail': '',
            },
          ],
          'shipping_address': {
            'country_code': 'CL',
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'phone': '+56 9 4578 8862',
            'recipient_name': 'Alejandro David',
            'type': 'HOME_OR_WORK',
          },
          'shipping_method': 'DIGITAL',
        },
        'amount': {
          'currency': 'CLP',
          'total': 2000,
          'details': {
            'shipping_discount': 0,
            'shipping': 500,
            'tax': 0,
            'subtotal': 1500,
          },
        },
        'soft_descriptor': 'Transaction Short description',
      },
      'state': 'created',
    };
  },
  CANCEL_PAYMENT_OBJECT() {
    return {
      'application': '91468344-801d-e74f-82d7-8f0269b57176',
      'additional_attributes': {
        'capture_token': 'a669a9a0-3cde-cf21-7f92-3b066d71855e',
      },
      'create_time': '2017-12-04T19:02:05.558Z',
      'gateway': 'CANCEL_DATA',
      'payer': {
        'payer_info': {
          'documentNumber': '253798858',
          'documentType': 'RUT',
          'full_name': 'Gustavo Gustavo',
          'email': 'dummy_user6@gmail.com',
          'country': 'CL',
        },
        'payment_method': 'QUICKPAY_TOKEN',
      },
      'update_time': '2017-12-04T19:02:05.558Z',
      'intent': 'sale',
      'links': [],
      'redirect_urls': {
        'return_url': 'https://ebctest.cybersource.com/',
        'cancel_url': 'http://localhost:8000',
      },
      'invoice_number': 'INPA-50000000847',
      'id': '31109ad9-971f-5eba-5a7c-7a71f7ffc544',
      'transaction': {
        'description': 'Transaction detailed description',
        'item_list': {
          'items': [
            {
              'price': 500,
              'name': 'Destornillador 2344',
              'tax': 0,
              'description': 'Destornillador 2344',
              'quantity': 3,
              'sku': '1231232',
              'thumbnail': '',
            },
          ],
          'shipping_address': {
            'country_code': 'CL',
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'phone': '+56 9 4578 8862',
            'recipient_name': 'Alejandro David',
            'type': 'HOME_OR_WORK',
          },
          'shipping_method': 'DIGITAL',
        },
        'amount': {
          'currency': 'CLP',
          'total': 2000,
          'details': {
            'shipping_discount': 0,
            'shipping': 500,
            'tax': 0,
            'subtotal': 1500,
          },
        },
        'soft_descriptor': 'Transaction Short description',
      },
      'state': 'canceled',
    };
  },
  isValidModel: (document) => {
    expect(document).toBeDefined();
    expect(document).toHaveProperty('application');
    expect(document).toHaveProperty('additional_attributes');
    expect(document).toHaveProperty('payer');
    expect(document.payer).toHaveProperty('payer_info');
    expect(document.payer).toHaveProperty('payment_method');
    expect(document).toHaveProperty('intent');
    expect(document).toHaveProperty('links');
    expect(document).toHaveProperty('redirect_urls');
    expect(document).toHaveProperty('invoice_number');
    expect(document).toHaveProperty('transaction');
    expect(document.transaction).toHaveProperty('item_list');
    expect(document.transaction).toHaveProperty('amount');
    expect(document.transaction).toHaveProperty('soft_descriptor');
    expect(document).toHaveProperty('state');
  },
};
