'use strict';

module.exports = {
  CREATED_OBJECT() {
    return {
      'invoice_number': 'INPA-00000003731',
      'update_time': '2017-06-20T01:45:04.702Z',
      'create_time': '2017-06-20T01:45:04.702Z',
      'state': 'created',
      'intent': 'sale',
      'redirect_urls': {
        'return_url': 'http://www.google.cl/success',
        'cancel_url': 'http://www.google.cl/cancel',
      },
      'transaction': {
        'reference_id': 'APP-28262',
        'description': 'POLIZA-8272',
        'soft_descriptor': 'POL8722',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'sku': 'SEG102',
              'name': 'SEG-01012',
              'description': 'SEGURO VIAJE SIN LIMITES',
              'quantity': 1,
              'price': 100,
              'tax': 0,
              '_id': '59487e2048271645a67064b6',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 7030, Depto 302B',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 6898 7440',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'David Antonio Muñoz Gaete',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 300,
          'details': {
            'subtotal': 80,
            'tax': 0,
            'shipping': 30,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'full_name': 'David Antonio Muñoz Gaete',
          'email': 'dmunozgaete@gmail.com',
        },
        'payment_method': 'QUICKPAY_CREDIT',
      },
      'links': [],
      'id': '8142f1d5-bb74-bb68-6bcf-7ccd92fa9ca5',
      'test1': 'DUMMMY_PROCUIDE',
      '_rid': 'C5Y3AKyhegADAAAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegADAAAAAAAAAA==/',
      '_etag': '\'2b004fef-0000-0000-0000-59487e250000\'',
      '_attachments': 'attachments/',
      '_ts': 1497923108,
    };
  },
  VALID_PAYMENT_PAID() {
    return {
      'invoice_number': 'INPA-00000003731',
      'update_time': '2017-06-20T01:45:04.702Z',
      'create_time': '2017-06-20T01:45:04.702Z',
      'state': 'paid',
      'intent': 'sale',
      'redirect_urls': {
        'return_url': 'http://www.google.cl/success',
        'cancel_url': 'http://www.google.cl/cancel',
      },
      'transaction': {
        'reference_id': 'APP-28262',
        'description': 'POLIZA-8272',
        'soft_descriptor': 'POL8722',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'sku': 'SEG102',
              'name': 'SEG-01012',
              'description': 'SEGURO VIAJE SIN LIMITES',
              'quantity': 1,
              'price': 100,
              'tax': 0,
              '_id': '59487e2048271645a67064b6',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 7030, Depto 302B',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 6898 7440',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'David Antonio Muñoz Gaete',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 300,
          'details': {
            'subtotal': 80,
            'tax': 0,
            'shipping': 30,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'full_name': 'David Antonio Muñoz Gaete',
          'email': 'dmunozgaete@gmail.com',
        },
        'payment_method': 'QUICKPAY_CREDIT',
      },
      'links': [],
      'id': '8142f1d5-bb74-bb68-6bcf-7ccd92fa9ca5',
      'test1': 'DUMMMY_PROCUIDE',
      '_rid': 'C5Y3AKyhegADAAAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegADAAAAAAAAAA==/',
      '_etag': '\'2b004fef-0000-0000-0000-59487e250000\'',
      '_attachments': 'attachments/',
      '_ts': 1497923108,
    };
  },
  VALID_PAYMENT_CANCELED() {
    return {
      'intent': 'sale',
      'additional_attributes': {
        'last_four_digits': 3038,
        'card_bin': 548740,
      },
      'redirect_urls': {
        'return_url': 'http://localhost:8000',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '59501333d7907137238de38c',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 1000,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'QUICKPAY_CREDIT',
      },
      'links': [],
      'id': 'dcee507d-f526-078b-82b7-65d05dfdbe6a',
      'create_time': '2017-06-25T19:47:03.405Z',
      'update_time': '2017-06-25T19:47:09.077Z',
      'state': 'canceled',
      'invoice_number': 'INPA-0000000092',
      'gateway': {
        'msgType': 'CreditResponse',
        'responseCode': 59,
        'responseDescription': 'Customer Abandonment',
        'responseSignature': 'XFsgmf/ILNHV1gdguKaO0yd0YPC6cB9S1b/MEJ+Smas=',
        'payerUserProfile': null,
        'logTrackId': '#LGID=83484f2fe57a16c5817bb6c42aed9b3a#MID=006261519#CID=WEB#PYMT=C#DT=#DID=#',
      },
      '_rid': 'C5Y3AKyhegBgAAAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegBgAAAAAAAAAA==/',
      '_etag': '\'0600edb1-0000-0000-0000-5950133e0000\'',
      '_attachments': 'attachments/',
      '_ts': 1498420013,
    };
  },
  REJECTED_PAYMENT() {
    return {
      'intent': 'sale',
      'application': 'f93e841e-0b48-1697-79ee-b766a8029429',
      'redirect_urls': {
        'return_url': 'http://localhost:8000',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'thumbnail': 'http://localhost:8000/bundles/app/css/images/e-commerce-demo/product-icon.png',
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '5a559ccf8bab1b3104a3a459',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 1000,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'document_type': 'RUT',
          'document_number': '107872388',
          'country': 'CL',
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'QUICKPAY_CREDIT',
      },
      'links': [],
      'id': '8f09cacc-64c7-c139-91cf-753639418a85',
      'create_time': '2018-01-10T04:55:47.115Z',
      'update_time': '2018-01-10T04:56:02.757Z',
      'state': 'rejected',
      'invoice_number': 'INPA-0000003371',
      'gateway': {
        'responseCode': 408,
        'responseDescription': 'timeout error',
        'amount': 1000,
      },
      '_rid': 'C5Y3AKyhegAvDQAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegAvDQAAAAAAAA==/',
      '_etag': '\'05001bb7-0000-0000-0000-5a559ce30000\'',
      '_attachments': 'attachments/',
      '_ts': 1515560163,
    };
  },
  isValidModel: (document) => {
    expect(document).toBeDefined();
    expect(document).toHaveProperty('payer');
    expect(document.payer).toHaveProperty('payer_info');
    expect(document.payer).toHaveProperty('payment_method');
    expect(document).toHaveProperty('intent');
    expect(document).toHaveProperty('links');
    expect(document).toHaveProperty('redirect_urls');
    expect(document).toHaveProperty('invoice_number');
    expect(document).toHaveProperty('transaction');
    expect(document.transaction).toHaveProperty('item_list');
    expect(document.transaction).toHaveProperty('amount');
    expect(document.transaction).toHaveProperty('soft_descriptor');
    expect(document).toHaveProperty('state');
  },
};
