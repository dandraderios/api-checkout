'use strict';

module.exports = {
  INVALID_NEW_MODEL() {
    return {
      'event': 'dummy_event',
    };
  },
  DB_VALID_MODEL() {
    return {
      'capture_method': 'TOKENIZATION',
      'update_time': '2018-01-05T19:46:48.888Z',
      'create_time': '2018-01-05T19:46:48.888Z',
      'cardholder': {
        'name': 'Gustavo Munoz',
        'country': 'CL',
        'reference_id': 'Merchant_id_reference',
        'email': 'gimunoz6@falabella.cl',
      },
      'billing': {
        'city': 'Santiago',
        'line1': 'Miraflores 222',
        'state': 'Region Metropolitana',
        'country': 'CL',
      },
      'application': '1c0776b0-fd95-e8ac-b29f-fd16f82384db',
      'capture_number': 'INCA-0000002394',
      'redirect_urls': {
        'cancel_url': 'http://www.lun.cl',
        'return_url': 'http://www.lin.cl/',
      },
      'id': '5aa635d1-ac8b-41bc-c9ad-dda3e6ed0190',
      'capture': 'CREDIT_CARD',
      'state': 'created',
      'links': [
        {
          'rel': 'self',
          'href': 'http://localhost:8090/captures/5aa635d1-ac8b-41bc-c9ad-dda3e6ed0190',
          'method': 'GET',
        },
        {
          'method': 'REDIRECT',
          'href': 'http://localhost:8090/captures/gateways/credit/card/5aa635d1-ac8b-41bc-c9ad-dda3e6ed0190/capture',
          'rel': 'capture_url',
        },
      ],
    };
  },
};
