'use strict';

module.exports = {
  CREATED_OBJECT() {
    return {
      'intent': 'sale',
      'application': 'acb361cc-e4e7-24fb-d4e2-17bb3aa74066',
      'redirect_urls': {
        'return_url': 'http://localhost:8000',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'thumbnail': 'http://localhost:8000/bundles/app/css/images/e-commerce-demo/product-icon.png',
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '599ba176c7ce536b70155b1f',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 1000,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'documentType': 'RUT',
          'documentNumber': '107872388',
          'country': 'CL',
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'QUICKPAY_TOKEN',
        'capture_token': 'cb3e99a9-aade-7209-e4be-f756303c1626',
      },
      'links': [],
      'id': '7e5cf9ab-6b51-39d9-36fc-60c8b2731bcb',
      'create_time': '2017-08-22T03:13:58.337Z',
      'update_time': '2017-08-22T03:13:58.337Z',
      'state': 'created',
      'invoice_number': 'INPA-0000001035',
      '_rid': 'C5Y3AKyhegAPBAAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegAPBAAAAAAAAA==/',
      '_etag': '\'500034e1-0000-0000-0000-599ba1760000\'',
      '_attachments': 'attachments/',
      '_ts': 1503371623,
    };
  },
  VALID_PAYMENT_REFUNDED() {
    return {
      'intent': 'sale',
      'application': 'acb361cc-e4e7-24fb-d4e2-17bb3aa74066',
      'redirect_urls': {
        'return_url': 'http://localhost:8000',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'thumbnail': 'http://localhost:8000/bundles/app/css/images/e-commerce-demo/product-icon.png',
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '599e528189648ee9c714f7b5',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 1500,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'documentType': 'RUT',
          'documentNumber': '107872388',
          'country': 'CL',
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'QUICKPAY_TOKEN',
        'capture_token': '091a5324-547f-d345-bc29-357a9d0ce1ca',
      },
      'links': [],
      'id': '8e746297-f634-d60c-aed9-81302664b506',
      'create_time': '2017-08-24T04:13:58.881Z',
      'update_time': '2017-08-24T04:14:30.015Z',
      'state': 'refunded',
      'invoice_number': 'INPA-0000001127',
      'gateway': {
        'reason': 'Porque se me da la gana!',
        'merchantReferenceCode': 'INPA-0000001127',
        'requestID': '5035480589616569604008',
        'decision': 'ACCEPT',
        'reasonCode': '100',
        'requestToken': 'Ahj7/wSTEPlUxSL+K1eoik/q1bO60AU/o+NbHWJgXdxxDhk0ky3SA4C+08CcmIfKon2f+5FxSAAA+yDF',
        'purchaseTotals': {
          'currency': 'CLP',
        },
        'voidReply': {
          'reasonCode': '100',
          'requestDateTime': '2017-08-24T04:14:19Z',
          'amount': '1500.',
          'currency': 'CLP',
        },
        'message': 'payment voided',
      },
      '_rid': 'C5Y3AKyhegBrBAAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegBrBAAAAAAAAA==/',
      '_etag': '\'6d009d90-0000-0000-0000-599e52a50000\'',
      '_attachments': 'attachments/',
      '_ts': 1503548049,
    };
  },
  VALID_PAYMENT_PAID() {
    return {
      'intent': 'sale',
      'application': 'acb361cc-e4e7-24fb-d4e2-17bb3aa74066',
      'redirect_urls': {
        'return_url': 'http://localhost:8000',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'thumbnail': 'http://localhost:8000/bundles/app/css/images/e-commerce-demo/product-icon.png',
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '599e1881c113b7c47cd5ba32',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 1000,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'documentType': 'RUT',
          'documentNumber': '107872388',
          'country': 'CL',
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'QUICKPAY_TOKEN',
        'capture_token': 'e5597716-a0e1-9aec-edf3-f403310d8340',
      },
      'links': [],
      'id': '44e7b7bf-8a12-1c17-9396-e92d59fe05d4',
      'create_time': '2017-08-24T00:06:25.772Z',
      'update_time': '2017-08-24T00:06:29.738Z',
      'state': 'paid',
      'invoice_number': 'INPA-0000001097',
      'gateway': {
        'merchantReferenceCode': 'INPA-0000001097',
        'requestID': '5035331899856632004010',
        'decision': 'ACCEPT',
        'reasonCode': '100',
        'requestToken': 'Ahj7/wSTEPdEhJZ7Wb2qik/rxB1j3AU/rxB1j3JgV9DiHDJpJlukBwF9oYE5MQ90SElntZvaoAAAyQ48',
        'purchaseTotals': {
          'currency': 'CLP',
        },
        'ccAuthReply': {
          'reasonCode': '100',
          'amount': '1000',
          'authorizationCode': '570110',
          'avsCode': '1',
          'authorizedDateTime': '2017-08-24T00:06:30Z',
          'processorResponse': '1',
          'paymentNetworkTransactionID': '111222',
          'ownerMerchantID': 'falabella',
          'processorTransactionID': '00770f3d5d11423db9e883b5b7d68cf2',
        },
        'ccCaptureReply': {
          'reasonCode': '100',
          'requestDateTime': '2017-08-24T00:06:30Z',
          'amount': '1000',
        },
        'additionalProcessorResponse': '90cc47a7-5e52-424e-a77b-880ada70544e',
        'capture_token': 'e5597716-a0e1-9aec-edf3-f403310d8340',
      },
      '_rid': 'C5Y3AKyhegBNBAAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegBNBAAAAAAAAA==/',
      '_etag': '\'6c003e55-0000-0000-0000-599e18850000\'',
      '_attachments': 'attachments/',
      '_ts': 1503533170,
    };
  },
  REJECTED_PAYMENT_OBJECT() {
    return {
      'intent': 'sale',
      'additional_attributes': {
        'capture_token': '0d62565e-4fd1-0d7f-ccf9-362c5a680b55',
        'remember_capture': false,
      },
      'application': 'f93e841e-0b48-1697-79ee-b766a8029429',
      'redirect_urls': {
        'return_url': 'http://localhost:8000',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'thumbnail': 'http://localhost:8000/bundles/app/css/images/e-commerce-demo/product-icon.png',
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '5a559ea48bab1b3104a3a475',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 1000,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'document_type': 'RUT',
          'document_number': '107872388',
          'country': 'CL',
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'QUICKPAY_TOKEN',
      },
      'links': [],
      'id': '9e3b1c90-a888-d185-9aa5-a41462b3c5c9',
      'create_time': '2018-01-10T05:03:32.403Z',
      'update_time': '2018-01-10T05:03:34.117Z',
      'state': 'rejected',
      'invoice_number': 'INPA-0000003380',
      'gateway': {
        'message': 'user cancel error',
        'amount': 1000,
        'capture_token': '0d62565e-4fd1-0d7f-ccf9-362c5a680b55',
      },
      '_rid': 'C5Y3AKyhegA4DQAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegA4DQAAAAAAAA==/',
      '_etag': '\'05000fc2-0000-0000-0000-5a559ea60000\'',
      '_attachments': 'attachments/',
      '_ts': 1515560614,
    };
  },
  VALID_REFUNDED_AMOUNT() {
    return {
      'refunded_amount': 1000,
    };
  },
  INVALID_TEXT_REFUNDED_AMOUNT() {
    return {
      'refunded_amount': 'ABC',
    };
  },
  INVALID_NUMERIC_REFUNDED_AMOUNT() {
    return {
      'refunded_amount': -1000,
    };
  },
  isValidModel: (document) => {
    expect(document).toBeDefined();
    expect(document).toHaveProperty('payer');
    expect(document.payer).toHaveProperty('payer_info');
    expect(document.payer).toHaveProperty('payment_method');
    expect(document).toHaveProperty('intent');
    expect(document).toHaveProperty('links');
    expect(document).toHaveProperty('redirect_urls');
    expect(document).toHaveProperty('invoice_number');
    expect(document).toHaveProperty('transaction');
    expect(document.transaction).toHaveProperty('item_list');
    expect(document.transaction).toHaveProperty('amount');
    expect(document.transaction).toHaveProperty('soft_descriptor');
    expect(document).toHaveProperty('state');
  },
};
