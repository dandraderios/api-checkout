'use strict';

module.exports = {
  INVALID_NEW_CONFIGURATION_MODEL() {
    return {
      'payment_method': 'CARDBILL_PAYMENT_DEBIT_BF',
    };
  },
  VALID_CONFIGURATION_OBJECT() {
    return {
      'client_id': '797775211512568418442',
      'enabled': true,
      'merchantAuthKey': 'f1f_fa1ab311a',
      'merchantId': '009580120',
      'branchId': '101',
      'terminalId': '101',
      'commerceCodeDebit': '001',
      'commerceCodeCmr': '10001804',
      'channel': 'WEB',
      'application': '06941d8d-2fea-6df6-9fd5-accdf669c1d7',
      'payment_method': 'CARDBILL_PAYMENT_DEBIT_BF',
      'secret_shared_key': 'AS12DD12F12F412F12F',
    };
  },
  VALID_CONFIGURATION_SELF_CHECKOUT() {
    return {
      'client_id': '797775211512568418442',
      'enabled': true,
      'merchantAuthKey': 'f1f_fa1ab311a',
      'merchant_id': '009580120',
      'branch_id': '101',
      'terminal_id': '101',
      'commerceCodeDebit': '001',
      'commerceCodeCmr': '10001804',
      'request_channel': 'WEB',
      'application': '06941d8d-2fea-6df6-9fd5-accdf669c1d7',
      'payment_method': 'SELF_CHECKOUT',
      'secret_shared_key': '  AD1156H8RL48YY',
      'merchant_channel': 'falabella',
    };
  },
  VALID_CONFIGURATION_DECISION_MANAGER() {
    return {
      'client_id': '797775211512568418442',
      'enabled': false,
      'merchantAuthKey': 'f1f_fa1ab311a',
      'merchantId': '009580120',
      'branchId': '101',
      'terminalId': '101',
      'commerceCodeDebit': '001',
      'commerceCodeCmr': '10001804',
      'channel': 'WEB',
      'application': '06941d8d-2fea-6df6-9fd5-accdf669c1d7',
      'payment_method': 'CARDBILL_PAYMENT_DEBIT_BF',
      'id': '4d2799a5-6bcb-6117-cee3-1d2249f89d97',
      'decisionManager_enabled': 1,
    };
  },
  isValidModel: (document) => {
    expect(document).toBeDefined();
    expect(document).toHaveProperty('client_id');
    expect(document).toHaveProperty('enabled');
    expect(document).toHaveProperty('merchantAuthKey');
    expect(document).toHaveProperty('merchantId');
    expect(document).toHaveProperty('branchId');
    expect(document).toHaveProperty('terminalId');
    expect(document).toHaveProperty('commerceCodeDebit');
    expect(document).toHaveProperty('commerceCodeCmr');
    expect(document).toHaveProperty('channel');
    expect(document).toHaveProperty('application');
    expect(document).toHaveProperty('payment_method');
    expect(document).toHaveProperty('decisionManager_enabled');
  },
};
