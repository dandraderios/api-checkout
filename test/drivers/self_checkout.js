'use strict';

module.exports = {
  CREATED_OBJECT() {
    return {
      'intent': 'sale',
      'application': '5f1611fe-a61c-9e48-2850-42bae1388f02',
      '_id': '5a39b7c53f3217402a574512',
      'redirect_urls': {
        'return_url': 'http://localhost:8000',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'thumbnail': 'http://localhost:8000/bundles/app/css/images/e-commerce-demo/product-icon.png',
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '5a39b7c53f3217402a574513',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 1000,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'document_type': 'RUT',
          'document_number': '107872388',
          'country': 'CL',
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'SELF_CHECKOUT',
      },
      'links': [],
      'id': '0c568114-2a84-6e9e-d04d-e0d4effcc04d',
      'create_time': '2017-12-20T01:07:19.688Z',
      'update_time': '2017-12-20T01:07:19.688Z',
      'state': 'created',
      'invoice_number': 'INPA-0000003210',
      '_rid': 'C5Y3AKyhegCODAAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegCODAAAAAAAAA==/',
      '_etag': '\'3a005326-0000-0000-0000-5a39b7c70000\'',
      '_attachments': 'attachments/',
      '_ts': 1513732039,
    };
  },
  VALID_PAYMENT_PAID() {
    return {
      'intent': 'sale',
      'application': 'f93e841e-0b48-1697-79ee-b766a8029429',
      'redirect_urls': {
        'return_url': 'http://localhost:8000',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'thumbnail': 'http://localhost:8000/bundles/app/css/images/e-commerce-demo/product-icon.png',
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '5a39b7e53f3217402a574517',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 1000,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'document_type': 'RUT',
          'document_number': '107872388',
          'country': 'CL',
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'SELF_CHECKOUT',
      },
      'links': [],
      'id': '01a68f1d-92eb-3e64-f2f4-d78a0cfbbada',
      'create_time': '2017-12-20T01:07:49.888Z',
      'update_time': '2017-12-20T05:13:50.297Z',
      'state': 'paid',
      'invoice_number': 'INPA-0000003212',
      'gateway': {
        'dummy': true,
        'amount': 1000,
        'resume': {},
      },
      '_rid': 'C5Y3AKyhegCQDAAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegCQDAAAAAAAAA==/',
      '_etag': '\'3a00d026-0000-0000-0000-5a39f18f0000\'',
      '_attachments': 'attachments/',
      '_ts': 1513746831,
    };
  },
  REJECTED_PAYMENT_OBJECT() {
    return {
      'intent': 'sale',
      'additional_attributes': {
        'last_four_digits': 3038,
        'card_bin': 548740,
      },
      'application': 'f93e841e-0b48-1697-79ee-b766a8029429',
      'redirect_urls': {
        'return_url': 'http://localhost:8000',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'thumbnail': 'http://localhost:8000/bundles/app/css/images/e-commerce-demo/product-icon.png',
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '5a559daf8bab1b3104a3a465',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 1000,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'document_type': 'RUT',
          'document_number': '367845414',
          'country': 'CL',
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'SELF_CHECKOUT',
      },
      'links': [],
      'id': '938af7d5-d409-bbd2-fb59-38a10a679a00',
      'create_time': '2018-01-10T04:59:27.566Z',
      'update_time': '2018-01-10T04:59:59.766Z',
      'state': 'rejected',
      'invoice_number': 'INPA-0000003375',
      'gateway': {
        'msgType': 'CardBillPaymentResponse',
        'responseCode': 28,
        'responseDescription': 'Error communicating with the ESB',
        'UUID': 'c1524c1f-ff5a-4041-bd84-4e1994c2f06a',
        'transactionId': 'c1524c1f-ff5a-4041-bd84-4e1994c2f06a',
      },
      '_rid': 'C5Y3AKyhegAzDQAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegAzDQAAAAAAAA==/',
      '_etag': '\'0500a8bc-0000-0000-0000-5a559dd00000\'',
      '_attachments': 'attachments/',
      '_ts': 1515560400,
    };
  },
  isValidModel: (document) => {
    expect(document).toBeDefined();
    expect(document).toHaveProperty('application');
    expect(document).toHaveProperty('payer');
    expect(document.payer).toHaveProperty('payer_info');
    expect(document.payer).toHaveProperty('payment_method');
    expect(document).toHaveProperty('intent');
    expect(document).toHaveProperty('links');
    expect(document).toHaveProperty('redirect_urls');
    expect(document).toHaveProperty('invoice_number');
    expect(document).toHaveProperty('transaction');
    expect(document.transaction).toHaveProperty('item_list');
    expect(document.transaction).toHaveProperty('amount');
    expect(document.transaction).toHaveProperty('soft_descriptor');
    expect(document).toHaveProperty('state');
  },
};
