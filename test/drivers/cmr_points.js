'use strict';

module.exports = {
  CREATED_OBJECT() {
    return {
      'intent': 'sale',
      'application': '5f1611fe-a61c-9e48-2850-42bae1388f02',
      '_id': '5a281349f48a641c1df8dc8b',
      'redirect_urls': {
        'return_url': 'http://localhost:8000',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'thumbnail': 'http://localhost:8000/bundles/app/css/images/e-commerce-demo/product-icon.png',
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '5a281349f48a641c1df8dc8c',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'PTS',
          'total': 1000,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'document_type': 'RUT',
          'document_number': '107872388',
          'country': 'CL',
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'CMR_POINTS',
      },
      'links': [],
      'id': 'ddb8d859-a2e4-2aef-f909-527c63f86800',
      'create_time': '2017-12-06T15:57:04.983Z',
      'update_time': '2017-12-06T15:57:04.983Z',
      'state': 'created',
      'invoice_number': 'INPA-0000002943',
      '_rid': 'C5Y3AKyhegCDCwAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegCDCwAAAAAAAA==/',
      '_etag': '\'3d006231-0000-0000-0000-5a2813500000\'',
      '_attachments': 'attachments/',
      '_ts': 1512575824,
    };
  },
  isValidModel: (document) => {
    expect(document).toBeDefined();
    expect(document).toHaveProperty('payer');
    expect(document.payer).toHaveProperty('payer_info');
    expect(document.payer).toHaveProperty('payment_method');
    expect(document).toHaveProperty('intent');
    expect(document).toHaveProperty('links');
    expect(document).toHaveProperty('redirect_urls');
    expect(document).toHaveProperty('invoice_number');
    expect(document).toHaveProperty('transaction');
    expect(document.transaction).toHaveProperty('item_list');
    expect(document.transaction).toHaveProperty('amount');
    expect(document.transaction).toHaveProperty('soft_descriptor');
    expect(document).toHaveProperty('state');
  },
};
