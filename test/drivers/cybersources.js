'use strict';

module.exports = {
  RESPONSE_BODY_EMPTY() {
    return `
    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Header>
        <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
            <wsu:Timestamp wsu:Id="Timestamp-1552538797" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
                <wsu:Created>2017-12-29T14:40:22.656Z</wsu:Created>
            </wsu:Timestamp>
        </wsse:Security>
        </soap:Header>
        <soap:Body>
        </soap:Body>
    </soap:Envelope>`;
  },
  INVALID_DECISIONMANAGER_RESPONSE() {
    return `
    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Header>
        <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
            <wsu:Timestamp wsu:Id="Timestamp-1552538797" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
                <wsu:Created>2017-12-29T14:40:22.656Z</wsu:Created>
            </wsu:Timestamp>
        </wsse:Security>
        </soap:Header>
        <soap:Body>
        <c:replyMessage xmlns:c="urn:schemas-cybersource-com:transaction-data-1.115">
            <c:merchantReferenceCode>INPA-50000001249</c:merchantReferenceCode>
            <c:requestID>5145584224056589504011</c:requestID>
            <c:decision>ERROR</c:decision>
            <c:reasonCode>150</c:reasonCode>
            <c:requestToken>Ahjz7wSTFvFTdU5LBU4LigFLi1ZBnBJgV9DmHDJpJl6MVy/LACcmLeKm6pyWCpwWAAAA+//j</c:requestToken>
            <c:afsReply>
                <c:reasonCode>150</c:reasonCode>
                <c:afsResult>62</c:afsResult>
                <c:hostSeverity>1</c:hostSeverity>
                <c:consumerLocalTime>11:40:22</c:consumerLocalTime>
                <c:afsFactorCode>A^G</c:afsFactorCode>
                <c:addressInfoCode>COR-BA^INTL-BA^INTL-SA^MM-BIN</c:addressInfoCode>
                <c:internetInfoCode>FREE-EM</c:internetInfoCode>
                <c:scoreModelUsed>default</c:scoreModelUsed>
                <c:binCountry>US</c:binCountry>
                <c:cardAccountType>Visa Gold</c:cardAccountType>
                <c:cardScheme>VISA CREDIT</c:cardScheme>
                <c:cardIssuer>RIVER VALLEY CREDIT UNION</c:cardIssuer>
            </c:afsReply>
        </c:replyMessage>
        </soap:Body>
    </soap:Envelope>`;
  },
  VALID_DECISIONMANAGER_RESPONSE() {
    return `
    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Header>
        <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
            <wsu:Timestamp wsu:Id="Timestamp-1552538797" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
                <wsu:Created>2017-12-29T14:40:22.656Z</wsu:Created>
            </wsu:Timestamp>
        </wsse:Security>
        </soap:Header>
        <soap:Body>
        <c:replyMessage xmlns:c="urn:schemas-cybersource-com:transaction-data-1.115">
            <c:merchantReferenceCode>INPA-50000001249</c:merchantReferenceCode>
            <c:requestID>5145584224056589504011</c:requestID>
            <c:decision>ACCEPT</c:decision>
            <c:reasonCode>100</c:reasonCode>
            <c:requestToken>Ahjz7wSTFvFTdU5LBU4LigFLi1ZBnBJgV9DmHDJpJl6MVy/LACcmLeKm6pyWCpwWAAAA+//j</c:requestToken>
            <c:afsReply>
                <c:reasonCode>100</c:reasonCode>
                <c:afsResult>62</c:afsResult>
                <c:hostSeverity>1</c:hostSeverity>
                <c:consumerLocalTime>11:40:22</c:consumerLocalTime>
                <c:afsFactorCode>A^G</c:afsFactorCode>
                <c:addressInfoCode>COR-BA^INTL-BA^INTL-SA^MM-BIN</c:addressInfoCode>
                <c:internetInfoCode>FREE-EM</c:internetInfoCode>
                <c:scoreModelUsed>default</c:scoreModelUsed>
                <c:binCountry>US</c:binCountry>
                <c:cardAccountType>Visa Gold</c:cardAccountType>
                <c:cardScheme>VISA CREDIT</c:cardScheme>
                <c:cardIssuer>RIVER VALLEY CREDIT UNION</c:cardIssuer>
            </c:afsReply>
        </c:replyMessage>
        </soap:Body>
    </soap:Envelope>`;
  },
};
