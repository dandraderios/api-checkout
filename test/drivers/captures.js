'use strict';

module.exports = {
  INVALID_CAPTURE() {
    return {};
  },
  VALID_CAPTURE() {
    return {
      'id': '123',
      'gateway': {
        'payment_token': '123',
        'installments_number': '123123',
      },
      'links': [
        {
          'rel': 'cap',
        },
      ],
    };
  },
};
