'use strict';

module.exports = {
  BODY_RESPONSE_QUICKPAY_OPERATIONS() {
    return {
      'secureToken': '2dc6f6ea4139f7dbb14e3e8c52cc8c28d2e6f8f63806421976e566631e2723add71fc4ae613f4ff83550357ae6bc28065fe2ca78100c11d47e282d858afd5d19068b69138a50',
      'logTrackId': '#LGID=e540aec2edf78f03f4e70a39843f8add#MID=009580120#CHID=WEB#PYMT=D#DT=#DID=#UUID=729b55b7-b7e6-4d87-9635-c6df8511f73f#TID=INPA-0000003072#',
      'uuid': '729b55b7-b7e6-4d87-9635-c6df8511f73f',
      'links': [
        {
          'rel': 'initiate',
          'method': 'GET',
          'title': 'Initiate operation',
          'href': 'https://quickpay-qa.fif.tech/init/2dc6f6ea4139f7dbb14e3e8c52cc8c28d2e6f8f63806421976e566631e2723add71fc4ae613f4ff83550357ae6bc28065fe2ca78100c11d47e282d858afd5d19068b69138a50?logTrackId=#LGID=e540aec2edf78f03f4e70a39843f8add#MID=009580120#CHID=WEB#PYMT=D#DT=#DID=#UUID=729b55b7-b7e6-4d87-9635-c6df8511f73f#TID=INPA-0000003072#',
        },
      ],
    };
  },
  INVALID_ERROR_RESPONSE() {
    return {
      'statusCode': 500,
    };
  },
  INVALID_RESPONSE() {
    return {};
  },
  VALID_RESPONSE() {
    return {
      'statusCode': 200,
      'response': '1',
      'continuation': [],
    };
  },
  VALID_RESPONSE_WITH_CODE_0() {
    return {
      'statusCode': 200,
      'codigo': 0,
    };
  },
  VALID_RESPONSE_WITH_CODE_1() {
    return {
      'statusCode': 200,
      'codigo': 1,
    };
  },
  VALID_RESPONSE_201() {
    return {
      'statusCode': 201,
    };
  },
  ERROR_RESPONSE() {
    return {
      'statusCode': 500,
    };
  },
};
