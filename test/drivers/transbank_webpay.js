'use strict';

module.exports = {
  CREATED_OBJECT() {
    return {
      'intent': 'sale',
      'application': 'acb361cc-e4e7-24fb-d4e2-17bb3aa74066',
      'redirect_urls': {
        'return_url': 'http://localhost:8000',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'thumbnail': 'http://localhost:8000/bundles/app/css/images/e-commerce-demo/product-icon.png',
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '595d3f8544c234344a9d3f3c',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 1000,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'documentType': 'RUT',
          'documentNumber': '107872388',
          'country': 'CL',
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'TRANSBANK_WEBPAY',
      },
      'links': [],
      'id': '90bfdf00-95d3-9937-0c98-eb52adca5812',
      'create_time': '2017-07-05T19:35:33.173Z',
      'update_time': '2017-07-05T19:35:33.173Z',
      'state': 'created',
      'invoice_number': 'INPA-0000000267',
      '_rid': 'C5Y3AKyhegAPAQAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegAPAQAAAAAAAA==/',
      '_etag': '\'0800b0f8-0000-0000-0000-595d3f850000\'',
      '_attachments': 'attachments/',
      '_ts': 1499283319,
    };
  },
  VALID_PAYMENT_PAID() {
    return {
      'intent': 'sale',
      'application': '9e1442fa-480c-1f7f-4ab7-6f47306fc469',
      'redirect_urls': {
        'return_url': 'http://portal.sandbox.connect.fif.tech',
        'cancel_url': 'http://portal.sandbox.connect.fif.tech',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'thumbnail': 'http://portal.sandbox.connect.fif.tech/bundles/app/css/images/e-commerce-demo/product-icon.png',
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '59ebe4e054530b000fa10f31',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 1000,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'documentType': 'RUT',
          'documentNumber': '107872388',
          'country': 'CL',
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'TRANSBANK_WEBPAY',
      },
      'links': [],
      'id': '2f10c721-c914-3d77-ffe0-0b417f57955a',
      'create_time': '2017-10-22T00:22:56.392Z',
      'update_time': '2017-10-22T00:23:30.782Z',
      'state': 'paid',
      'invoice_number': 'INPA-50000000010',
      'gateway': {
        'accountingDate': '1021',
        'buyOrder': 'INPA-50000000010',
        'cardDetail': {
          'cardNumber': '3123',
        },
        'detailOutput': [
          {
            'sharesNumber': 0,
            'amount': '1000',
            'commerceCode': '597020000541',
            'buyOrder': 'INPA-50000000010',
            'authorizationCode': '172986',
            'paymentTypeCode': 'VD',
            'responseCode': 0,
          },
        ],
        'sessionId': '2f10c721-c914-3d77-ffe0-0b417f57955a',
        'transactionDate': '2017-10-22T00:23:00.161Z',
        'urlRedirection': 'https://webpay3gint.transbank.cl/filtroUnificado/voucher.cgi',
        'VCI': 'TSY',
      },
      '_rid': 'QloVAMLECQALAAAAAAAAAA==',
      '_self': 'dbs/QloVAA==/colls/QloVAMLECQA=/docs/QloVAMLECQALAAAAAAAAAA==/',
      '_etag': '\'5400bae3-0000-0000-0000-59ebe5020000\'',
      '_attachments': 'attachments/',
      '_ts': 1508631810,
      'additional_attributes': {
        'last_four_digits': 3038,
        'card_bin': 548740,
      },
    };
  },
  REJECTED_PAYMENT_OBJECT() {
    return {
      'intent': 'sale',
      'application': 'f93e841e-0b48-1697-79ee-b766a8029429',
      'redirect_urls': {
        'return_url': 'http://localhost:8000',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'thumbnail': 'http://localhost:8000/bundles/app/css/images/e-commerce-demo/product-icon.png',
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '5a559d778bab1b3104a3a460',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 1000,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'document_type': 'RUT',
          'document_number': '107872388',
          'country': 'CL',
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'TRANSBANK_WEBPAY',
      },
      'links': [],
      'id': 'e4013e76-7f54-26a4-b0a0-983b22ee06c3',
      'create_time': '2018-01-10T04:58:31.912Z',
      'update_time': '2018-01-10T04:58:37.517Z',
      'state': 'rejected',
      'invoice_number': 'INPA-0000003374',
      'gateway': {
        'TBK_TOKEN': 'e74e74610139cd1e2ba99560839dbd5da686ed367e4b852aad9a9dc7d1faebe1',
        'TBK_ID_SESION': 'e4013e76-7f54-26a4-b0a0-983b22ee06c3',
        'TBK_ORDEN_COMPRA': 'INPA-0000003374',
        'message': 'timeout/anuled error',
        'amount': 1000,
      },
      '_rid': 'C5Y3AKyhegAyDQAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegAyDQAAAAAAAA==/',
      '_etag': '\'0500acba-0000-0000-0000-5a559d7d0000\'',
      '_attachments': 'attachments/',
      '_ts': 1515560317,
    };
  },
  isValidModel: (document) => {
    expect(document).toBeDefined();
    expect(document).toHaveProperty('application');
    expect(document).toHaveProperty('payer');
    expect(document.payer).toHaveProperty('payer_info');
    expect(document.payer).toHaveProperty('payment_method');
    expect(document).toHaveProperty('intent');
    expect(document).toHaveProperty('links');
    expect(document).toHaveProperty('redirect_urls');
    expect(document).toHaveProperty('invoice_number');
    expect(document).toHaveProperty('transaction');
    expect(document.transaction).toHaveProperty('item_list');
    expect(document.transaction).toHaveProperty('amount');
    expect(document.transaction).toHaveProperty('soft_descriptor');
    expect(document).toHaveProperty('state');
  },
};
