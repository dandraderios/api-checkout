'use strict';

module.exports = {
  INVVALID_MODEL() {
    return {
    };
  },
  VALID_MODEL() {
    return {
      'application': '9e1442fa-480c-1f7f-4ab7-6f47306fc469',
      'active': true,
      'name': 'Webhook de Prueba',
      'url': 'https://requestb.in/p7nv04p7',
      'events': [
        'payment_created',
      ],
    };
  },
  DB_VALID_MODEL() {
    return {
      'application': '9e1442fa-480c-1f7f-4ab7-6f47306fc469',
      'active': true,
      'name': 'Webhook de Prueba',
      'url': 'https://requestb.in/p7nv04p7',
      'events': [
        'payment_created',
        'payment_canceled',
        'payment_paid',
        'payment_reversed',
        'payment_accounted',
      ],
      'id': '3c5b213a-30f5-e15e-6d03-e646ee3dc534',
      '_rid': 'QloVANMn5AABAAAAAAAAAA==',
      '_self': 'dbs/QloVAA==/colls/QloVANMn5AA=/docs/QloVANMn5AABAAAAAAAAAA==/',
      '_etag': '\'cc001f8d-0000-0000-0000-59ecf2350000\'',
      '_attachments': 'attachments/',
      '_ts': 1508700725,
    };
  },
  isValidModel: (document) => {
    expect(document).toBeDefined();
    expect(document).toHaveProperty('application');
    expect(document).toHaveProperty('active');
    expect(document).toHaveProperty('name');
    expect(document).toHaveProperty('url');
    expect(document).toHaveProperty('events');
    expect(document).toHaveProperty('id');
    expect(document).toHaveProperty('toJSON');
  },
};
