'use strict';

module.exports = {
  CREATED_OBJECT() {
    return {
      'intent': 'sale',
      'application': '5f1611fe-a61c-9e48-2850-42bae1388f02',
      '_id': '5a380c16e8294cded54e91c9',
      'redirect_urls': {
        'return_url': 'http://localhost:8000',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'thumbnail': 'http://localhost:8000/bundles/app/css/images/e-commerce-demo/product-icon.png',
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '5a380c16e8294cded54e91ca',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 1000,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'document_type': 'RUT',
          'document_number': '107872388',
          'country': 'CL',
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'QUICKPAY_DEBIT',
      },
      'links': [],
      'id': '453d50fa-afa3-9481-9278-8599de21ac3b',
      'create_time': '2017-12-18T18:42:36.427Z',
      'update_time': '2017-12-18T18:42:36.427Z',
      'state': 'created',
      'invoice_number': 'INPA-0000003168',
      '_rid': 'C5Y3AKyhegBkDAAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegBkDAAAAAAAAA==/',
      '_etag': '\'38001bc4-0000-0000-0000-5a380c1c0000\'',
      '_attachments': 'attachments/',
      '_ts': 1513622556,
    };
  },
  VALID_PAYMENT_CANCELED() {
    return {
      'intent': 'sale',
      'application': 'f93e841e-0b48-1697-79ee-b766a8029429',
      'redirect_urls': {
        'return_url': 'http://localhost:8000',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'thumbnail': 'http://localhost:8000/bundles/app/css/images/e-commerce-demo/product-icon.png',
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '5a380c47e8294cded54e91cc',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 1000,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'document_type': 'RUT',
          'document_number': '107872388',
          'country': 'CL',
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'QUICKPAY_DEBIT',
      },
      'links': [],
      'id': 'f9e48dcc-7fd8-a364-b7b7-989e5a91645c',
      'create_time': '2017-12-18T18:43:19.889Z',
      'update_time': '2017-12-18T18:43:23.347Z',
      'state': 'canceled',
      'invoice_number': 'INPA-0000003169',
      'gateway': {
        'msgType': 'Frontend-Time-Out-Error',
        'responseCode': 101,
        'responseDescription': 'Frontend Time-Out Error. Backend service did not respond in the expected timeout',
        'amount': 1000,
      },
      '_rid': 'C5Y3AKyhegBlDAAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegBlDAAAAAAAAA==/',
      '_etag': '\'3800d5c7-0000-0000-0000-5a380c4b0000\'',
      '_attachments': 'attachments/',
      '_ts': 1513622603,
      'additional_attributes': {
        'last_four_digits': 3038,
        'card_bin': 548740,
      },
    };
  },
  VALID_PAYMENT_PAID() {
    return {
      'intent': 'sale',
      'application': 'f93e841e-0b48-1697-79ee-b766a8029429',
      'redirect_urls': {
        'return_url': 'http://localhost:8000',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'thumbnail': 'http://localhost:8000/bundles/app/css/images/e-commerce-demo/product-icon.png',
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '5a382c7116d90ce83a3de60e',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 1000,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'document_type': 'RUT',
          'document_number': '107872388',
          'country': 'CL',
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'QUICKPAY_DEBIT',
      },
      'links': [],
      'id': 'cd30b01d-2e1a-2803-761c-7959f075a4b8',
      'create_time': '2017-12-18T21:00:37.769Z',
      'update_time': '2017-12-18T21:01:37.639Z',
      'state': 'paid',
      'invoice_number': 'INPA-0000003196',
      'gateway': {
        'msgType': 'DebitResponse',
        'responseCode': 0,
        'responseDescription': 'Success',
        'responseSignature': 'fxzfPs8mfNhyjfZWOkApgqSFRICv/zpBTDFak+NsqAY=',
        'payerUserProfile': null,
        'logTrackId': '#LGID=4f2ff51a482d10b831f49e07de3bf983#MID=006342112#CID=WEB#PYMT=D#DT=RUT#DID=173877323#',
        'qpTxnId': 357370,
        'authorizationCode': '48456188',
        'amount': 1000,
        'resume': {},
      },
      '_rid': 'C5Y3AKyhegCADAAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegCADAAAAAAAAA==/',
      '_etag': '\'1b00e350-0000-0000-0000-5a382cb10000\'',
      '_attachments': 'attachments/',
      '_ts': 1513630897,
      'additional_attributes': {
        'last_four_digits': 3038,
        'card_bin': 548740,
      },
    };
  },
  VALID_PAYMENT_REJECTED() {
    return {
      'intent': 'sale',
      'application': 'f93e841e-0b48-1697-79ee-b766a8029429',
      'redirect_urls': {
        'return_url': 'http://localhost:8000',
        'cancel_url': 'http://localhost:8000',
      },
      'transaction': {
        'description': 'Transaction detailed description',
        'soft_descriptor': 'Transaction Short description',
        'item_list': {
          'shipping_method': 'DIGITAL',
          'items': [
            {
              'thumbnail': 'http://localhost:8000/bundles/app/css/images/e-commerce-demo/product-icon.png',
              'sku': 'TRK345-2',
              'name': 'Flight 2344',
              'description': 'Flight SCL - ONT',
              'quantity': 2,
              'price': 500,
              'tax': 0,
              '_id': '5a380c47e8294cded54e91cc',
            },
          ],
          'shipping_address': {
            'line1': 'General Carol Urzua 1020, Depto 102A',
            'city': 'Santiago',
            'country_code': 'CL',
            'phone': '+56 9 8762 1244',
            'type': 'HOME_OR_WORK',
            'recipient_name': 'Jhon Doe Son',
          },
        },
        'amount': {
          'currency': 'CLP',
          'total': 1000,
          'details': {
            'subtotal': 1000,
            'tax': 0,
            'shipping': 0,
            'shipping_discount': 0,
          },
        },
      },
      'payer': {
        'payer_info': {
          'document_type': 'RUT',
          'document_number': '107872388',
          'country': 'CL',
          'full_name': 'Jhon Doe',
          'email': 'jhondoe@gmail.com',
        },
        'payment_method': 'QUICKPAY_DEBIT',
      },
      'links': [],
      'id': 'f9e48dcc-7fd8-a364-b7b7-989e5a91645c',
      'create_time': '2017-12-18T18:43:19.889Z',
      'update_time': '2017-12-18T18:43:23.347Z',
      'state': 'rejected',
      'invoice_number': 'INPA-0000003169',
      'gateway': {
        'msgType': 'Frontend-Time-Out-Error',
        'responseCode': 101,
        'responseDescription': 'Frontend Time-Out Error. Backend service did not respond in the expected timeout',
        'amount': 1000,
      },
      '_rid': 'C5Y3AKyhegBlDAAAAAAAAA==',
      '_self': 'dbs/C5Y3AA==/colls/C5Y3AKyhegA=/docs/C5Y3AKyhegBlDAAAAAAAAA==/',
      '_etag': '\'3800d5c7-0000-0000-0000-5a380c4b0000\'',
      '_attachments': 'attachments/',
      '_ts': 1513622603,
      'additional_attributes': {
        'last_four_digits': 3038,
        'card_bin': 548740,
      },
    };
  },
  isValidModel: (document) => {
    expect(document).toBeDefined();
    expect(document).toHaveProperty('application');
    expect(document).toHaveProperty('payer');
    expect(document.payer).toHaveProperty('payer_info');
    expect(document.payer).toHaveProperty('payment_method');
    expect(document).toHaveProperty('intent');
    expect(document).toHaveProperty('links');
    expect(document).toHaveProperty('redirect_urls');
    expect(document).toHaveProperty('invoice_number');
    expect(document).toHaveProperty('transaction');
    expect(document.transaction).toHaveProperty('item_list');
    expect(document.transaction).toHaveProperty('amount');
    expect(document.transaction).toHaveProperty('soft_descriptor');
    expect(document).toHaveProperty('state');
  },
};
