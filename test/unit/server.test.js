'use strict';
const sinon = require('sinon');
const proxyquire = require('proxyquire').noPreserveCache();
const supertest = require('supertest');
const logger = require('peanut-restify/logger');
const bootstrap = require('./../drivers/bootstrap');

const sandbox = sinon.createSandbox();

describe('Server', () => {
  // Setting things up....
  beforeAll(() => {
    bootstrap.setEnvironment();

    this.app = require('./../../server/server');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should lift server', (done) => {
    logger.mute();
    supertest(this.app.getServer())
      .get('/health')
      .send()
      .end((err, response) => {
        if (err) throw err;

        expect(response.status).toEqual(200);
        expect(response.body).toHaveProperty('up_time');
        done();
      });
  });

  it('should lift server when call app.start() method', (done) => {
    logger.mute();

    this.app.start();
    this.app.on('started', (server) => {
      logger.unmute();
      expect(server).toBeDefined();
      this.app.close();
      done();
    });
  });

  it('should throw error when core.boot failed to start the server', (done) => {
    logger.mute();
    // Mock core.boot
    const core = sandbox.mock(require('peanut-restify'));
    core
      .expects('boot')
      .callsFake((app, settings, callback) => {
        callback(new Error('DUMMY_REJECTION'));
      });

    // Boot event

    expect(() => this.app.boot()).toThrowError();
    done();
  });

  it('should set debug to true when NODE_ENV is DEV', (done) => {
    logger.mute();
    process.env.NODE_ENV = 'DEV';

    // Boot event
    this.app.on('loaded', () => {
      done();
    });

    // Should fails!
    this.app.boot();
    process.env.NODE_ENV = 'TEST';
  });
});
