'use strict';
const sinon = require('sinon');
const logger = require('peanut-restify/logger');
const sandbox = sinon.createSandbox();
const proxyquire = require('proxyquire');
const bootstrap = require('../../drivers/bootstrap');

const app = require('../../../server/server');

describe('Utils > GatewayClientBase', () => {
  let mockDB, GatewayClientBase, instance;

  beforeAll(() => {
    bootstrap.setEnvironment();
    // Get the class to test
    GatewayClientBase = require('../../../server/utils/GatewayClientBase');
    instance = new GatewayClientBase();
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should pass sanity client', () => {
    expect(GatewayClientBase).toBeDefined();
  });

  // getAdditionalLinks
  it('should return an empty array when call method getAdditionalLinks', () => {
    expect(instance.getAdditionalLinks).toBeDefined();
    let result = instance.getAdditionalLinks();
    expect(result).toEqual([]);
  });

  // isInvalid
  it('should return false when call method isInvalid', () => {
    expect(instance.isInvalid).toBeDefined();
    let result = instance.isInvalid('DUMMY_MODEL');
    expect(result).toEqual(false);
  });
});
