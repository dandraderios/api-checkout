'use strict';
const sinon = require('sinon');
const logger = require('peanut-restify/logger');
const sandbox = sinon.createSandbox();
const proxyquire = require('proxyquire');

describe('Utils > Localization', () => {
  let Localization;

  beforeAll(() => {
    // Get the class to test
    Localization = require('../../../server/utils/Localization');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should pass sanity client', () => {
    expect(Localization).toBeDefined();
  });

  it('should return an localize string when call method getText', () => {
    expect(Localization.getText).toBeDefined();
    expect(Localization.getText('ERRORS.GW01_150')).toContain('General system');
  });

  it('should return an localization string with error when call method getText with an invalid label', () => {
    expect(Localization.getText).toBeDefined();
    try {
      expect(Localization.getText('DUMMY'));
    } catch (ex) {
      expect(ex).toBeDefined();
      expect(ex.message).toContain('LOCALIZED_LABEL_NOT_FOUND');
    }
  });

  it('should return the default string  when call method getText with an invalid label but a default string', () => {
    expect(Localization.getText).toBeDefined();

    expect(Localization.getText('DUMMY', null, 'DEFAULT')).toContain('DEFAULT');
  });
});
