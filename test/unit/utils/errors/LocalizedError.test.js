'use strict';
const sinon = require('sinon');
const logger = require('peanut-restify/logger');

const sandbox = sinon.createSandbox();

describe('LocalizedError', () => {
  let mockLocalizedError;

  // Setting things up....
  beforeAll(() => {
    mockLocalizedError = require('./../../../../server/utils/errors/LocalizedError');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should pass sanity', () => {
    logger.mute();
    expect(mockLocalizedError).toBeDefined();
  });

  it('should create a new instance with error code', () => {
    logger.mute();
    const result = new mockLocalizedError('GW01_150');

    expect(result).toBeDefined();
    expect(result.error_code).toContain('GW01_150');
  });

  it('should throw an error when create a new instance without error code', () => {
    logger.mute();

    try {
      const result = new mockLocalizedError(null);
    } catch (ex) {
      expect(ex).toBeDefined();
      expect(ex.message).toContain('errorCode is required');
    }
  });
});
