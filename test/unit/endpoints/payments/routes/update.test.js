'use strict';
const sinon = require('sinon');
const supertest = require('supertest');
const logger = require('peanut-restify/logger');
const bootstrap = require('./../../../../drivers/bootstrap');
const paymentDriver = require('./../../../../drivers/payments');
const responseDriver = require('./../../../../drivers/responses');
const restifyRouter = require('restify-router');

const app = require('./../../../../../server/server');
const sandbox = sinon.createSandbox();

describe('PUT /payments/:id/edit', () => {
  // Setting things up....
  let paymentClient, routeToTest, testServer;
  const dummyId = '31109ad9-971f-5eba-5a7c-7a71f7ffc544';
  beforeAll(() => {
    bootstrap.setEnvironment();
    testServer = app.getServer();
    paymentClient = require('./../../../../../server/endpoints/payments/clients/paymentClient');
    routeToTest = require('./../../../../../server/endpoints/payments/routes/update');
    expect(restifyRouter).toBeDefined();
    const router = new restifyRouter.Router();
    routeToTest(router);
    router.applyRoutes(testServer, 'test_route');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });
  it('should return 500 (Server Error) status code when call method updatePartial and Errors Occurs', (done) => {
    const expectedResult = new Error('DUMMY_ERROR');
    expectedResult.type = expectedResult.message;
    sandbox
      .stub(paymentClient, 'updatePartial')
      .rejects(expectedResult);

    supertest(testServer)
      .put(`/test_route/${dummyId}/edit`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send()
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(500);
        done();
      });
  });
  it('should return 200 status code when call method updatePartial and is successfull', (done) => {
    let body = {};
    sandbox
      .stub(paymentClient, 'updatePartial')
      .resolves(responseDriver.VALID_RESPONSE());
    supertest(testServer)
      .put(`/test_route/${dummyId}/edit`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send(body)
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(200);
        expect(response).toHaveProperty('header');
        expect(response).toHaveProperty('body');
        expect(response).toHaveProperty('text');
        done();
      });
  });
});
