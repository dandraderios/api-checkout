'use strict';
const sinon = require('sinon');
const supertest = require('supertest');
const logger = require('peanut-restify/logger');
const bootstrap = require('./../../../../drivers/bootstrap');
const paymentDriver = require('./../../../../drivers/payments');
const restifyRouter = require('restify-router');

const app = require('./../../../../../server/server');
const sandbox = sinon.createSandbox();

describe('POST /payments', () => {
  // Setting things up....
  let paymentClient, routeToTest, testServer;
  beforeAll(() => {
    bootstrap.setEnvironment();
    testServer = app.getServer();
    paymentClient = require('./../../../../../server/endpoints/payments/clients/paymentClient');
    routeToTest = require('./../../../../../server/endpoints/payments/routes/create');
    expect(restifyRouter).toBeDefined();
    const router = new restifyRouter.Router();
    routeToTest(router);
    router.applyRoutes(testServer, 'test_route');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });
  it('should return 500 (Server Error) status code when call method create and Errors Occurs', (done) => {
    const expectedResult = new Error('DUMMY_ERROR');
    expectedResult.type = expectedResult.message;
    let body = {};
    sandbox
      .stub(paymentClient, 'create')
      .rejects(expectedResult);

    supertest(testServer)
      .post('/test_route')
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send(body)
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(500);
        done();
      });
  });
  it('should return 200 status code when call method create and is successfull', (done) => {
    let body = {
      'application': 'D4819F1W2F6Q84W86F',
      'payment_method': 'QUICKPAY_CREDIT',
    };
    const expectedResult = Object.assign(paymentDriver.CREATED_OBJECT(), {
      toJSON: () => {
        return paymentDriver.CREATED_OBJECT();
      },
    });
    sandbox
      .stub(paymentClient, 'create')
      .resolves(expectedResult);
    supertest(testServer)
      .post('/test_route')
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send(body)
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(201);
        expect(response).toHaveProperty('header');
        expect(response).toHaveProperty('body');
        expect(response).toHaveProperty('text');
        done();
      });
  });
});
