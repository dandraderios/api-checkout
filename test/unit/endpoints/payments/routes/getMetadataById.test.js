'use strict';
const sinon = require('sinon');
const supertest = require('supertest');
const logger = require('peanut-restify/logger');
const bootstrap = require('./../../../../drivers/bootstrap');
const paymentDriver = require('./../../../../drivers/payments');
const restifyRouter = require('restify-router');

const app = require('./../../../../../server/server');
const sandbox = sinon.createSandbox();

describe('GET /payments/:id/metadata', () => {
  // Setting things up....
  let paymentClient, routeToTest, testServer;
  const dummyId = '31109ad9-971f-5eba-5a7c-7a71f7ffc544';
  beforeAll(() => {
    bootstrap.setEnvironment();
    testServer = app.getServer();
    paymentClient = require('./../../../../../server/endpoints/payments/clients/paymentClient');
    routeToTest = require('./../../../../../server/endpoints/payments/routes/getMetadataById');
    expect(restifyRouter).toBeDefined();
    const router = new restifyRouter.Router();
    routeToTest(router);
    router.applyRoutes(testServer, 'test_route');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });
  it('should return 500 (Server Error) status code when call method getMetadataById and Errors Occurs', (done) => {
    const expectedResult = new Error('DUMMY_ERROR');
    expectedResult.type = expectedResult.message;
    sandbox
      .stub(paymentClient, 'getMetadataById')
      .rejects(expectedResult);

    supertest(testServer)
      .get(`/test_route/${dummyId}/metadata`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send()
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(500);
        done();
      });
  });
  it('should return 200 status code when call method getMetadataById and is successfull', (done) => {
    let body = {};
    const expectedResult = Object.assign(paymentDriver.CREATED_OBJECT(), {
      toJSON: () => {
        return paymentDriver.CREATED_OBJECT();
      },
    });
    sandbox
      .stub(paymentClient, 'getMetadataById')
      .resolves(expectedResult);
    supertest(testServer)
      .get(`/test_route/${dummyId}/metadata`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send(body)
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(200);
        expect(response).toHaveProperty('header');
        expect(response).toHaveProperty('body');
        expect(response).toHaveProperty('text');
        done();
      });
  });
});
