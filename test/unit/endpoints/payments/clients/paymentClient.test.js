'use strict';
const sinon = require('sinon');
const logger = require('peanut-restify/logger');
const sandbox = sinon.createSandbox();
const bootstrap = require('../../../../drivers/bootstrap');
const paymentDriver = require('../../../../drivers/payments');
const responseDriver = require('../../../../drivers/responses');

describe('Endpoints > Payments > Clients > paymentClient', () => {
  let app, mockDB, paymentClient, webhookClient;

  beforeAll(() => {
    bootstrap.setEnvironment();
    app = require('../../../../../server/server');
    mockDB = require('peanut-restify/db');
    // Get the class to test
    paymentClient = require('./../../../../../server/endpoints/payments/clients/paymentClient');
    webhookClient = require('./../../../../../server/endpoints/webhooks/clients/$webhooks');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should pass sanity client', () => {
    expect(paymentClient).toBeDefined();
  });

  // getById
  it('should return an error when call method getById and getConnection fails', (done) => {
    logger.mute();
    let error = new Error('DUMMY_ERROR');
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(error, null);
              },
            };
          },
        };
      });
    paymentClient
      .getById('DUMMY_TOKEN')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.error_code).toEqual('DATABASE_ERROR');
        expect(ex.error_description).toEqual('unknown error has ocurred when trying to get a payment by his id');
        expect(ex.meta_data.message).toEqual('');
        done();
      }).catch(done);
  });
  it('should return an error when call method getById and getConnection doesnt return payments', (done) => {
    logger.mute();
    let error = new Error('DUMMY_ERROR');
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, []);
              },
            };
          },
        };
      });
    paymentClient
      .getById('DUMMY_TOKEN')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.error_code).toEqual('DOCUMENT_NOT_FOUND');
        expect(ex.error_description).toEqual('the document you requested is not found');
        expect(ex.meta_data.id_not_found).toEqual('DUMMY_TOKEN');
        done();
      }).catch(done);
  });
  it('should return a valid payment when call method getById and is successful', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, [paymentDriver.CREATED_OBJECT()]);
              },
            };
          },
        };
      });
    paymentClient
      .getById('DUMMY_TOKEN')
      .then((response) => {
        expect(response).toBeDefined();
        response.toJSON();
        paymentDriver.isValidModel(response);
        done();
      });
  });

  // getByGatewayOrder
  it('should return an error when call method getByGatewayOrder and getConnection fails', (done) => {
    logger.mute();
    let error = new Error('DUMMY_ERROR');
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(error, null);
              },
            };
          },
        };
      });
    paymentClient
      .getByGatewayOrder('DUMMY_GATEWAY_ORDER')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.error_code).toEqual('DATABASE_ERROR');
        expect(ex.error_description).toEqual('unknown error has ocurred when trying to get a payment by his gateway order');
        expect(ex.meta_data.message).toEqual('');
        done();
      }).catch(done);
  });
  it('should return an error when call method getByGatewayOrder and getConnection doesnt return payments', (done) => {
    logger.mute();
    let error = new Error('DUMMY_ERROR');
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, []);
              },
            };
          },
        };
      });
    paymentClient
      .getByGatewayOrder('DUMMY_GATEWAY_ORDER')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.error_code).toEqual('DOCUMENT_NOT_FOUND');
        expect(ex.error_description).toEqual('the document you requested is not found');
        expect(ex.meta_data.id_not_found).toEqual('DUMMY_GATEWAY_ORDER');
        done();
      }).catch(done);
  });
  it('should return a valid payment when call method getByGatewayOrder and is successful', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, [paymentDriver.CREATED_OBJECT()]);
              },
            };
          },
        };
      });
    paymentClient
      .getByGatewayOrder('DUMMY_GATEWAY_ORDER')
      .then((response) => {
        expect(response).toBeDefined();
        response.toJSON();
        paymentDriver.isValidModel(response);
        done();
      });
  });

  // getMetadataById
  it('should return an error when call method getMetadataById and getConnection fails', (done) => {
    logger.mute();
    let error = new Error('DUMMY_ERROR');
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(error, null);
              },
            };
          },
        };
      });
    paymentClient
      .getMetadataById('DUMMY_GATEWAY_ORDER')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.error_code).toEqual('DATABASE_ERROR');
        expect(ex.error_description).toEqual('unknown error has ocurred when trying to get a payment meta_data');
        expect(ex.meta_data.message).toEqual('');
        done();
      }).catch(done);
  });
  it('should return an error when call method getMetadataById and getConnection doesnt return payments', (done) => {
    logger.mute();
    let error = new Error('DUMMY_ERROR');
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, []);
              },
            };
          },
        };
      });
    paymentClient
      .getMetadataById('DUMMY_GATEWAY_ORDER')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.error_code).toEqual('DOCUMENT_NOT_FOUND');
        expect(ex.error_description).toEqual('the document you requested is not found');
        expect(ex.meta_data.id_not_found).toEqual('DUMMY_GATEWAY_ORDER');
        done();
      }).catch(done);
  });
  it('should return metadata when call method getMetadataById and is successful', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, [paymentDriver.CREATED_OBJECT()]);
              },
            };
          },
        };
      });
    paymentClient
      .getMetadataById('DUMMY_GATEWAY_ORDER')
      .then((response) => {
        expect(response).toBeDefined();
        response.toJSON();
        done();
      });
  });

  // create
  it('should return an error when call method create and model is invalid', (done) => {
    logger.mute();
    paymentClient
      .create(paymentDriver.INVALID_MODEL(), 'DUMMY_BASE_URL')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.error_description).toEqual('the requested model has invalid properties');
        expect(ex.error_code).toEqual('INVALID_MODEL');
        expect(ex.meta_data.validations).toBeDefined();
        done();
      }).catch(done);
  });
  it('should return an error when call method create and getConnection fails', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          createDocument: (collectionLink, model, { }, callback) => {
            callback(new Error('DUMMY_ERROR'), null);
          },
        };
      });
    paymentClient
      .create(paymentDriver.VALID_MODEL(), 'DUMMY_BASE_URL')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.error_code).toEqual('DATABASE_ERROR');
        expect(ex.error_description).toEqual('Can\'t create a valid payment intention document');
        expect(ex.meta_data.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });

  // createSuccessRedirectionPage
  it('should return a html response when call method createSuccessRedirectionPage and is successful', (done) => {
    logger.mute();
    paymentClient
      .createSuccessRedirectionPage(paymentDriver.CREATED_OBJECT(), 'DUMMY_VALUES')
      .then((response) => {
        expect(response).toBeDefined();
        expect(response).toContain('html');
        expect(response).toContain('body');
        expect(response).toContain('head');
        done();
      });
  });

  // createErrorRedirectionPage
  it('should return a html response when call method createErrorRedirectionPage and is successful', (done) => {
    logger.mute();
    paymentClient
      .createErrorRedirectionPage(paymentDriver.CREATED_OBJECT(), 'DUMMY_VALUES')
      .then((response) => {
        expect(response).toBeDefined();
        expect(response).toContain('html');
        expect(response).toContain('body');
        expect(response).toContain('head');
        done();
      });
  });
});
