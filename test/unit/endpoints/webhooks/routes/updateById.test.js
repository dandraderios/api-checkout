'use strict';
const sinon = require('sinon');
const supertest = require('supertest');
const logger = require('peanut-restify/logger');
const bootstrap = require('./../../../../drivers/bootstrap');
const restifyRouter = require('restify-router');

const app = require('./../../../../../server/server');
const sandbox = sinon.createSandbox();
const webhookDriver = require('../../../../drivers/webhooks');

describe('GET /:app/webhooks/:id', () => {
  let webhookClient, tokenizationClient, routeToTest, testServer;
  const dummId = '9e1442fa-480c-1f7f-4ab7-6f47306fc469';
  beforeAll(() => {
    bootstrap.setEnvironment();
    testServer = app.getServer();
    routeToTest = require('./../../../../../server/endpoints/webhooks/routes/updateById');
    webhookClient = require('./../../../../../server/endpoints/webhooks/clients/$webhooks');
    expect(restifyRouter).toBeDefined();

    const router = new restifyRouter.Router();
    routeToTest(router);

    router.applyRoutes(testServer, 'test_route');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should return 500 (Server Error) status code when call method updateById and Errors Occurs', (done) => {
    const params = {
      appId: 'DUMMY_ID',
    };
    const expectedResult = new Error('DUMMY_ERROR');
    expectedResult.type = expectedResult.message;
    sandbox
      .stub(webhookClient, 'updateById')
      .rejects(expectedResult);
    supertest(testServer)
      .put(`/test_route/${dummId}`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send({params: params})
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(500);
        expect(response).toHaveProperty('error');
        done();
      });
  });
  it('should return 206 status code when call method updateById and is successfull', (done) => {
    const params = {
      appId: 'DUMMY_ID',
    };
    const webhook = Object.assign(webhookDriver.DB_VALID_MODEL(), {
      toJSON: () => {
        return webhookDriver.DB_VALID_MODEL();
      },
    });
    sandbox
      .stub(webhookClient, 'updateById')
      .resolves(webhook);
    supertest(testServer)
      .put(`/test_route/${dummId}`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send({params: params})
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(206);
        expect(response).toHaveProperty('text');
        done();
      });
  });
});
