'use strict';
const sinon = require('sinon');
const supertest = require('supertest');
const logger = require('peanut-restify/logger');
const bootstrap = require('./../../../../drivers/bootstrap');
const restifyRouter = require('restify-router');

const app = require('./../../../../../server/server');
const sandbox = sinon.createSandbox();
const webhookDriver = require('../../../../drivers/webhooks');

describe('GET /:app/webhooks/', () => {
  let webhookClient, tokenizationClient, routeToTest, testServer;
  beforeAll(() => {
    bootstrap.setEnvironment();

    testServer = app.getServer();
    routeToTest = require('./../../../../../server/endpoints/webhooks/routes/getByApplication');
    webhookClient = require('./../../../../../server/endpoints/webhooks/clients/$webhooks');
    expect(restifyRouter).toBeDefined();

    const router = new restifyRouter.Router();
    routeToTest(router);

    router.applyRoutes(testServer, 'test_route');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should return 500 (Server Error) status code when call method and Errors Occurs', (done) => {
    const expectedResult = new Error('DUMMY_ERROR');
    expectedResult.type = expectedResult.message;

    sandbox
      .stub(webhookClient, 'getByApplication')
      .rejects(expectedResult);

    supertest(testServer)
      .get('/test_route')
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send()
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(500);
        expect(response).toHaveProperty('error');
        done();
      });
  });
  it('should return 200 status code when call method successfully', (done) => {
    const webhook = Object.assign(webhookDriver.DB_VALID_MODEL(), {
      toJSON: () => {
        return webhookDriver.DB_VALID_MODEL();
      },
    });
    sandbox
      .stub(webhookClient, 'getByApplication')
      .resolves([webhook]);
    supertest(testServer)
      .get('/test_route')
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send()
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(200);
        expect(response).toHaveProperty('body');
        expect(response).toHaveProperty('header');
        expect(response).toHaveProperty('text');
        done();
      });
  });
});
