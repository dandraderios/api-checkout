'use strict';
const sinon = require('sinon');
const logger = require('peanut-restify/logger');
const sandbox = sinon.createSandbox();
const proxyquire = require('proxyquire');
const bootstrap = require('../../../../drivers/bootstrap');

const configurationDriver = require('../../../../drivers/configurations');
const webhookDriver = require('../../../../drivers/webhooks');

describe('Endpoints > Webhooks > Clients > $webhooks', () => {
  let app, mockDB, webhookClient;

  beforeAll(() => {
    bootstrap.setEnvironment();
    app = require('../../../../../server/server');
    mockDB = require('peanut-restify/db');
    // Get the class to test
    webhookClient = require('./../../../../../server/endpoints/webhooks/clients/$webhooks');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should pass sanity client', () => {
    expect(webhookClient).toBeDefined();
  });

  // getByApplication
  it('should return an error when call method getByApplication and getConfiguration fails', (done) => {
    logger.mute();
    let error = new Error('DUMMY_ERROR');
    error.type = 'DOCUMENTDB_ERROR';
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query, { }) => {
            return {
              toArray: (callback) => {
                return callback(error, null);
              },
            };
          },
        };
      });
    webhookClient
      .getByApplication('APPLICATION_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.type).toEqual('DOCUMENTDB_ERROR');
        done();
      }).catch(done);
  });
  it('should return an valid webhook when call method getByApplication is successful', (done) => {
    logger.mute();
    let webhook = webhookDriver.DB_VALID_MODEL();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query, { }) => {
            return {
              toArray: (callback) => {
                return callback(null, [webhook]);
              },
            };
          },
        };
      });
    webhookClient
      .getByApplication('APPLICATION_ID')
      .then((response) => {
        expect(response).toBeDefined();
        response[0].toJSON();
        expect(response).toEqual([webhook]);
        done();
      });
  });

  // getById
  it('should return an error when call method getById and getConfiguration fails', (done) => {
    logger.mute();
    let error = new Error('DUMMY_ERROR');
    error.type = 'DOCUMENTDB_ERROR';
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query, { }) => {
            return {
              toArray: (callback) => {
                return callback(error, null);
              },
            };
          },
        };
      });
    webhookClient
      .getById('APPLICATION_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.type).toEqual('DOCUMENTDB_ERROR');
        done();
      }).catch(done);
  });
  it('should return an error when call method getById and getConfiguration doesnt return items', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query, { }) => {
            return {
              toArray: (callback) => {
                return callback(null, []);
              },
            };
          },
        };
      });
    webhookClient
      .getById('DUMMY_TOKEN')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.type).toEqual('NOT_FOUND');
        done();
      }).catch(done);
  });
  it('should return an valid webhook when call method getById is successful', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query, { }) => {
            return {
              toArray: (callback) => {
                return callback(null, [webhookDriver.DB_VALID_MODEL()]);
              },
            };
          },
        };
      });
    webhookClient
      .getById('APPLICATION_ID')
      .then((response) => {
        expect(response).toBeDefined();
        response.toJSON();
        webhookDriver.isValidModel(response);
        done();
      });
  });

  // getById
  it('should return an error when call method getByEvent and getConfiguration fails', (done) => {
    logger.mute();
    let error = new Error('DUMMY_ERROR');
    error.type = 'DOCUMENTDB_ERROR';
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query, { }) => {
            return {
              toArray: (callback) => {
                return callback(error, null);
              },
            };
          },
        };
      });
    webhookClient
      .getByEvent('APPLICATION_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.type).toEqual('DOCUMENTDB_ERROR');
        done();
      }).catch(done);
  });
  it('should return an valid webhook when call method getByEvent is successful', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query, { }) => {
            return {
              toArray: (callback) => {
                return callback(null, [webhookDriver.DB_VALID_MODEL()]);
              },
            };
          },
        };
      });
    webhookClient
      .getByEvent('APPLICATION_ID')
      .then((response) => {
        expect(response).toBeDefined();
        expect(response).toEqual([webhookDriver.DB_VALID_MODEL()]);
        done();
      });
  });

  // create
  it('should return an error when call method create and getConnection throw an error', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .throwsException(new Error('DUMMY_ERROR'));
    webhookClient
      .create('DUMMY_MODEL')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        done();
      }).catch(done);
  });
  it('should return an error when call method create and model is invalid', (done) => {
    logger.mute();
    let model = new app.models.newWebhook(webhookDriver.INVVALID_MODEL());
    webhookClient
      .create(model)
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.type).toEqual('INVALID_MODEL');
        done();
      }).catch(done);
  });
  it('should return an error when call method create and getConnection fails', (done) => {
    logger.mute();
    let model = new app.models.newWebhook(webhookDriver.VALID_MODEL());
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          createDocument: (collectionLink, model, { }, callback) => {
            callback(new Error('DUMMY_ERROR'), null);
          },
        };
      });
    webhookClient
      .create(model)
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.type).toEqual('DOCUMENTDB_ERROR');
        done();
      }).catch(done);
  });
  it('should return an a valid model when call method create is successful', (done) => {
    logger.mute();
    let webhook = new app.models.newWebhook(webhookDriver.VALID_MODEL());
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          createDocument: (collectionLink, model, { }, callback) => {
            callback(null, webhook);
          },
        };
      });
    webhookClient
      .create(webhook)
      .then((response) => {
        expect(response).toBeDefined();
        response.toJSON();
        expect(response).toEqual(webhook);
        done();
      });
  });

  // updateById
  it('should return an error when call method updateById and model is invalid', (done) => {
    logger.mute();
    let model = new app.models.newWebhook(webhookDriver.INVVALID_MODEL());
    webhookClient
      .updateById('DUMMY_TOKEN', model)
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('INVALID_MODEL');
        done();
      }).catch(done);
  });
  it('should return an error when call method updateById and getConfiguration fails', (done) => {
    logger.mute();
    let model = new app.models.newWebhook(webhookDriver.VALID_MODEL());
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          replaceDocument: (self, docToUpdate, {
            enableCrossPartitionQuery,
          }, callback) => {
            callback(new Error('DUMMY_ERROR'), null);
          },
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(new Error('DUMMY_ERROR'), null);
              },
            };
          },
        };
      });
    webhookClient
      .updateById('DUMMY_TOKEN', model)
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an error when call method updateById and replaceDocument fails', (done) => {
    logger.mute();
    let webhook = new app.models.newWebhook(webhookDriver.VALID_MODEL());
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          replaceDocument: (self, docToUpdate, {
            enableCrossPartitionQuery,
          }, callback) => {
            callback(new Error('DUMMY_ERROR'), null);
          },
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, [webhook]);
              },
            };
          },
        };
      });
    webhookClient
      .updateById('DUMMY_TOKEN', webhook)
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return a valid model when call method updateById and is successful', (done) => {
    logger.mute();
    let webhook = new app.models.newWebhook(webhookDriver.VALID_MODEL());
    webhook._self = 'WWW.DUMMY_LINK.COM';
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          replaceDocument: (self, docToUpdate, {
            enableCrossPartitionQuery,
          }, callback) => {
            callback(null, webhook);
          },
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, [webhook]);
              },
            };
          },
        };
      });
    webhookClient
      .updateById('DUMMY_TOKEN', webhook)
      .then((response) => {
        expect(response).toBeDefined();
        response.toJSON();
        expect(response).toEqual(webhook);
        done();
      });
  });

  // del
  it('should return an error when call method del and getById fails', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .throwsException(new Error('DUMMY_ERROR'));
    webhookClient
      .del('DUMMY_TOKEN')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an error when call method del and deleteDocument fails', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          deleteDocument: (self, { }, callback) => {
            callback(new Error('DUMMY_ERROR'));
          },
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, [webhookDriver.VALID_MODEL()]);
              },
            };
          },
        };
      });
    sandbox
      .stub(webhookClient, 'getById')
      .resolves(webhookDriver.VALID_MODEL());

    webhookClient
      .del('DUMMY_TOKEN')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        expect(ex.type).toEqual('DOCUMENTDB_ERROR');
        done();
      }).catch(done);
  });
  it('should return a null when call method del is successful', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          deleteDocument: (self, { }, callback) => {
            callback(null);
          },
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, [webhookDriver.VALID_MODEL()]);
              },
            };
          },
        };
      });
    sandbox
      .stub(webhookClient, 'getById')
      .resolves(webhookDriver.VALID_MODEL());

    webhookClient
      .del('DUMMY_TOKEN')
      .then((response) => {
        expect(response).toBeUndefined();
        done();
      });
  });
});
