'use strict';
const sinon = require('sinon');
const logger = require('peanut-restify/logger');
const sandbox = sinon.createSandbox();
const proxyquire = require('proxyquire');
const bootstrap = require('../../../../drivers/bootstrap');

const app = require('../../../../../server/server');
const configurationDriver = require('../../../../drivers/configurations');
const tokenizationDriver = require('../../../../drivers/tokenizations');

describe('Endpoints > Tokenizations > Clients > tokenizationClient', () => {
  let mockDB, tokenizationClient;

  beforeAll(() => {
    bootstrap.setEnvironment();
    mockDB = require('peanut-restify/db');
    // Get the class to test
    tokenizationClient = require('./../../../../../server/endpoints/tokenizations/clients/tokenizationClient');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should pass sanity client', () => {
    expect(tokenizationClient).toBeDefined();
  });

  // getById
  it('should return an error when call method getById and getConnection throws an exception', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .throwsException(new Error('DUMMY_ERROR'));
    tokenizationClient
      .getById('DUMMY_TOKEN')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an error when call method getById and getConfiguration fails', (done) => {
    logger.mute();
    let error = new Error('DUMMY_ERROR');
    error.body = 'ERROR_BODY';
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query, { }) => {
            return {
              toArray: (callback) => {
                return callback(error, null);
              },
            };
          },
        };
      });
    tokenizationClient
      .getById('DUMMY_TOKEN')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex).toEqual('ERROR_BODY');
        done();
      }).catch(done);
  });
  it('should return an error when call method getById and getConfiguration doesnt return items', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query, { }) => {
            return {
              toArray: (callback) => {
                return callback(null, []);
              },
            };
          },
        };
      });
    tokenizationClient
      .getById('DUMMY_TOKEN')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex).toEqual('DOCUMENT_NOT_FOUND');
        done();
      }).catch(done);
  });
  it('should return a tokenization model when call method getById and is successful', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query, { }) => {
            return {
              toArray: (callback) => {
                return callback(null, [tokenizationDriver.DB_VALID_MODEL()]);
              },
            };
          },
        };
      });
    tokenizationClient
      .getById('DUMMY_TOKEN')
      .then((response) => {
        expect(response).toBeDefined();
        delete response.toJSON;
        expect(response).toEqual(tokenizationDriver.DB_VALID_MODEL());
        done();
      });
  });

  // create
  it('should return an error when call method getById and getConfiguration throws an Exception', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .throwsException(new Error('DUMMY_ERROR'));
    tokenizationClient
      .create('DUMMY_DATA', 'BASE_DUMMY_URL')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an error when call method getById and getConfiguration fails', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          createDocument: (collectionLink, data, callback) => {
            callback(new Error('DUMMY_ERROR'), null);
          },
        };
      });
    tokenizationClient
      .create('DUMMY_DATA', 'BASE_DUMMY_URL')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        expect(ex.type).toEqual('DOCUMENTDB_ERROR');
        done();
      }).catch(done);
  });
  it('should return a valid tokenization model when call method getById and is successful', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          createDocument: (collectionLink, data, callback) => {
            callback(null, tokenizationDriver.DB_VALID_MODEL());
          },
        };
      });
    tokenizationClient
      .create('DUMMY_DATA', 'BASE_DUMMY_URL')
      .then((response) => {
        expect(response).toBeDefined();
        expect(response).toEqual(tokenizationDriver.DB_VALID_MODEL());
        done();
      });
  });
});
