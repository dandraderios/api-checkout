'use strict';
const sinon = require('sinon');
const supertest = require('supertest');
const logger = require('peanut-restify/logger');
const bootstrap = require('./../../../../drivers/bootstrap');
const restifyRouter = require('restify-router');
const HttpStatus = require('http-status-codes');

const app = require('./../../../../../server/server');
const sandbox = sinon.createSandbox();

describe('POST /tokenizations/captures', () => {
  let tokenizationClient, routeToTest, testServer;
  beforeAll(() => {
    bootstrap.setEnvironment();

    testServer = app.getServer();
    routeToTest = require('./../../../../../server/endpoints/tokenizations/routes/create');
    tokenizationClient = require('./../../../../../server/endpoints/tokenizations/clients/tokenizationClient');
    expect(restifyRouter).toBeDefined();

    const router = new restifyRouter.Router();
    routeToTest(router);

    router.applyRoutes(testServer, 'test_route');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should return 500 (Server Error) status code when call method create and Errors Occurs', (done) => {
    const expectedResult = new Error('DUMMY_ERROR');
    expectedResult.type = expectedResult.message;

    sandbox
      .stub(tokenizationClient, 'create')
      .rejects(expectedResult);

    supertest(testServer)
      .post('/test_route')
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send()
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(500);
        expect(response).toHaveProperty('error');
        done();
      });
  });
  it('should return 200 status code when call method create is successfully', (done) => {
    sandbox
      .stub(tokenizationClient, 'create')
      .resolves(HttpStatus.OK);

    supertest(testServer)
      .post('/test_route')
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send()
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(200);
        done();
      });
  });
});
