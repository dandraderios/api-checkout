'use strict';
const sinon = require('sinon');
const supertest = require('supertest');
const logger = require('peanut-restify/logger');
const bootstrap = require('./../../../../../drivers/bootstrap');
const restifyRouter = require('restify-router');

const app = require('./../../../../../../server/server');
const paymentDriver = require('./../../../../../drivers/payments');

const sandbox = sinon.createSandbox();

describe('POST /payments/gateways/quickpay/token/:id/silent', () => {
  // Setting things up....
  let mockClient, routeToTest, testServer;
  const dummyId = '31109ad9-971f-5eba-5a7c-7a71f7ffc544';
  beforeAll(() => {
    bootstrap.setEnvironment();
    testServer = app.getServer();
    mockClient = require('./../../../../../../server/endpoints/gateways/quickpay_token/clients/gatewayClient');
    routeToTest = require('./../../../../../../server/endpoints/gateways/quickpay_token/routes/silent');
    expect(restifyRouter).toBeDefined();
    const router = new restifyRouter.Router();
    routeToTest(router);
    router.applyRoutes(testServer, 'test_route');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should return 500 (Server Error) status code when call method approvePayment and Errors Occurs', (done) => {
    const expectedResult = new Error('DUMMY_ERROR');
    expectedResult.type = expectedResult.message;
    sandbox
      .stub(mockClient, 'approvePayment')
      .rejects(expectedResult);
    supertest(testServer)
      .post(`/test_route/${dummyId}/silent`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send()
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(500);
        expect(response.body).toHaveProperty('type');
        expect(response.body.type).toEqual(expectedResult.type);
        done();
      });
  });
  it('should return 200 status code when call method approvePayment and is successfull', (done) => {
    sandbox
      .stub(mockClient, 'approvePayment')
      .resolves(paymentDriver.CREATED_OBJECT());
    supertest(testServer)
      .post(`/test_route/${dummyId}/silent`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send()
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(200);
        expect(response).toHaveProperty('header');
        expect(response).toHaveProperty('body');
        expect(response).toHaveProperty('text');
        done();
      });
  });
});
