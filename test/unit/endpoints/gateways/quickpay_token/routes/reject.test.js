'use strict';
const sinon = require('sinon');
const supertest = require('supertest');
const logger = require('peanut-restify/logger');
const bootstrap = require('./../../../../../drivers/bootstrap');
const quickpayTokenDriver = require('./../../../../../drivers/quickpay_token');
const responseDriver = require('./../../../../../drivers/responses');
const restifyRouter = require('restify-router');

const app = require('./../../../../../../server/server');
const sandbox = sinon.createSandbox();

describe('POST /payments/gateways/quickpay/token/:id/reject', () => {
  // Setting things up....
  let mockClient, routeToTest, testServer, paymentClient, mockDB;
  const dummyId = '31109ad9-971f-5eba-5a7c-7a71f7ffc544';
  beforeAll(() => {
    bootstrap.setEnvironment();
    mockDB = require('peanut-restify/db');
    testServer = app.getServer();
    mockClient = require('./../../../../../../server/endpoints/gateways/quickpay_token/clients/gatewayClient');
    paymentClient = require('./../../../../../../server/endpoints/payments/clients/paymentClient');
    routeToTest = require('./../../../../../../server/endpoints/gateways/quickpay_token/routes/reject');
    expect(restifyRouter).toBeDefined();
    const router = new restifyRouter.Router();
    routeToTest(router);
    router.applyRoutes(testServer, 'test_route');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should return a ErrorRedirectionPage when call method rejectPayment and return a error', (done) => {
    sandbox
      .stub(mockClient, 'rejectPayment')
      .resolves(quickpayTokenDriver.REJECTED_PAYMENT_OBJECT());
    sandbox
      .stub(paymentClient, 'createErrorRedirectionPage')
      .resolves('<html></html>');
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, [quickpayTokenDriver.REJECTED_PAYMENT_OBJECT()]);
              },
            };
          },
        };
      });
    supertest(testServer)
      .post(`/test_route/${dummyId}/reject`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send(responseDriver.VALID_RESPONSE())
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(200);
        expect(response).toHaveProperty('header');
        expect(response).toHaveProperty('body');
        done();
      });
  });
});
