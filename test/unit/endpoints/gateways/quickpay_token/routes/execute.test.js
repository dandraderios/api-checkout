'use strict';
const sinon = require('sinon');
const supertest = require('supertest');
const logger = require('peanut-restify/logger');
const bootstrap = require('./../../../../../drivers/bootstrap');
const paymentDriver = require('./../../../../../drivers/payments');
const responseDriver = require('./../../../../../drivers/responses');
const restifyRouter = require('restify-router');

const app = require('./../../../../../../server/server');
const sandbox = sinon.createSandbox();

describe('POST /payments/gateways/quickpay/token/:id/execute', () => {
  // Setting things up....
  let mockClient, routeToTest, testServer, paymentClient, mockDB;
  const dummyId = '31109ad9-971f-5eba-5a7c-7a71f7ffc544';
  beforeAll(() => {
    bootstrap.setEnvironment();
    mockDB = require('peanut-restify/db');
    testServer = app.getServer();
    mockClient = require('./../../../../../../server/endpoints/gateways/quickpay_token/clients/gatewayClient');
    paymentClient = require('./../../../../../../server/endpoints/payments/clients/paymentClient');
    routeToTest = require('./../../../../../../server/endpoints/gateways/quickpay_token/routes/execute');
    expect(restifyRouter).toBeDefined();
    const router = new restifyRouter.Router();
    routeToTest(router);
    router.applyRoutes(testServer, 'test_route');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should return an error when call method approvePayment and fails', (done) => {
    sandbox
      .stub(mockClient, 'approvePayment')
      .rejects(new Error('DUMMY_ERROR'));

    supertest(testServer)
      .post(`/test_route/${dummyId}/execute`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send(responseDriver.VALID_RESPONSE())
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(500);
        expect(response).toHaveProperty('header');
        expect(response).toHaveProperty('body');
        done();
      });
  });
  it('should return 200 status code when call method approvePayment and is successfull', (done) => {
    sandbox
      .stub(mockClient, 'approvePayment')
      .resolves(paymentDriver.PAID_OBJECT());
    sandbox
      .stub(paymentClient, 'createSuccessRedirectionPage')
      .resolves('<html></html>');
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, [paymentDriver.PAID_OBJECT()]);
              },
            };
          },
        };
      });
    supertest(testServer)
      .post(`/test_route/${dummyId}/execute`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send(responseDriver.VALID_RESPONSE())
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(200);
        expect(response).toHaveProperty('header');
        expect(response).toHaveProperty('body');
        done();
      });
  });
});
