'use strict';
const sinon = require('sinon');
const logger = require('peanut-restify/logger');
const sandbox = sinon.createSandbox();
const bootstrap = require('./../../../../../drivers/bootstrap');

const paymentDriver = require('./../../../../../drivers/payments');
const quickpayTokenDriver = require('./../../../../../drivers/quickpay_token');
const configurationDriver = require('./../../../../../drivers/configurations');
const responseDriver = require('./../../../../../drivers/responses');
const tokenizationDriver = require('./../../../../../drivers/tokenizations');
const gatewayDriver = require('./../../../../../drivers/gateways');
const captureDriver = require('./../../../../../drivers/captures');
const cybersourceDriver = require('./../../../../../drivers/cybersources');

describe('Endpoints > Gateways > Quickpay_token  > Clients > gatewayClient', () => {
  let app, mockDB, gatewayClient, configurations, paymentClient, mockRequest, tokenizationClient;

  beforeAll(() => {
    bootstrap.setEnvironment();
    app = require('./../../../../../../server/server');
    mockRequest = require('request');
    mockDB = require('peanut-restify/db');

    // Get the class to test
    configurations = require('./../../../../../../server/endpoints/configurations/clients/$configurations');
    paymentClient = require('./../../../../../../server/endpoints/payments/clients/paymentClient');
    gatewayClient = require('./../../../../../../server/endpoints/gateways/quickpay_token/clients/gatewayClient');
    tokenizationClient = require('./../../../../../../server/endpoints/tokenizations/clients/tokenizationClient');

    // Dummy Client
    sinon
      .stub(paymentClient, 'sendWebHook')
      .callsFake(() => {
      });
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should pass sanity clients', () => {
    expect(gatewayClient).toBeDefined();
    expect(paymentClient).toBeDefined();
    expect(configurations).toBeDefined();
  });

  // getAdditionalLinks
  it('should return an empty array when call method getAdditionalLinks', () => {
    expect(gatewayClient.getAdditionalLinks).toBeDefined();
    let result = gatewayClient.getAdditionalLinks('DUMMY_PAY_ROUTE');
    expect(result[0]).toHaveProperty('href');
    expect(result[0]).toHaveProperty('rel');
    expect(result[0]).toHaveProperty('security');
    expect(result[0]).toHaveProperty('method');
  });

  // createCaptureIntention
  it('should return an error when call method createCaptureIntention and request fails', (done) => {
    logger.mute();
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(new Error('DUMMY_ERROR'), null, null);
      });

    gatewayClient
      .createCaptureIntention(quickpayTokenDriver.CREATED_OBJECT(), 'DUMMY_URL')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.error_code).toEqual('REQUEST_ERROR');
        done();
      }).catch(done);
  });
  it('should return an error when call method createCaptureIntention and request return a wrong statusCode', (done) => {
    logger.mute();
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(null, responseDriver.ERROR_RESPONSE(), 'DUMMY_BODY');
      });
    gatewayClient
      .createCaptureIntention(quickpayTokenDriver.CREATED_OBJECT(), 'DUMMY_URL')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.error_code).toEqual('INVALID_HTTP_STATUS_CODE_IN_REQUEST');
        done();
      }).catch(done);
  });
  it('should return a body when call method createCaptureIntention is successful', (done) => {
    logger.mute();
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(null, responseDriver.VALID_RESPONSE_201(), '<body>');
      });
    gatewayClient
      .createCaptureIntention(quickpayTokenDriver.CREATED_OBJECT(), 'DUMMY_URL')
      .then((response) => {
        expect(response).toBeDefined();
        expect(response).toContain('<body>');
        done();
      }, done).catch(done);
  });

  // getCaptureCardDetails
  it('should return a valid DB_VALID_MODEL when call method createCaptureIntention and is successful', (done) => {
    logger.mute();
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, cb) => {
        cb(null, {
          statusCode: 201,
        }, tokenizationDriver.DB_VALID_MODEL());
      });

    gatewayClient
      .createCaptureIntention(paymentDriver.VALID_MODEL(), 'http://www.google.cl')
      .then((response) => {
        expect(response).toBeDefined();
        expect(response).toEqual(tokenizationDriver.DB_VALID_MODEL());
        done();
      }, done).catch(done);
  });

  // getApprovalDialog
  it('should return an error when call method getApprovalDialog and getById function fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .rejects(new Error('DUMMY_ERROR'));
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an html response when call method getApprovalDialog and is successful', (done) => {
    logger.mute();
    let payment = paymentDriver.CREATED_OBJECT();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(payment);
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          replaceDocument: (modelSelf, model, callback) => {
            callback(null, payment);
          },
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, payment);
              },
            };
          },
        };
      });
    sandbox
      .stub(tokenizationClient, 'getById')
      .resolves(payment);
    sandbox
      .stub(paymentClient, 'getBaseUrl')
      .resolves('www.dummy.com');
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then((response) => {
        expect(response).toBeDefined();
        expect(response).toContain('html');
        expect(response).toContain('head');
        expect(response).toContain('body');
        done();
      }, done).catch(done);
  });
  it('should return an error when call method getApprovalDialog without capture_token and createCaptureIntention fails', (done) => {
    logger.mute();
    let payment = paymentDriver.CREATED_OBJECT();
    delete payment.additional_attributes.capture_token;
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(payment);
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          replaceDocument: (modelSelf, model, callback) => {
            callback(null, payment);
          },
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, payment);
              },
            };
          },
        };
      });
    sandbox
      .stub(paymentClient, 'getBaseUrl')
      .resolves('www.dummy.com');
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex).toEqual(new Error('An error has ocurred trying to create a capture intention for quickpay_token'));
        done();
      }).catch(done);
  });
  it('should return an html response when call method getApprovalDialog without capture_token and is successful', (done) => {
    logger.mute();
    let payment = paymentDriver.CREATED_OBJECT();
    delete payment.additional_attributes.capture_token;
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(payment);
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          replaceDocument: (modelSelf, model, callback) => {
            callback(null, payment);
          },
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, payment);
              },
            };
          },
        };
      });
    sandbox
      .stub(paymentClient, 'getBaseUrl')
      .resolves('www.dummy.com');
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(null, responseDriver.VALID_RESPONSE_201(), '<body>');
      });
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then((response) => {
        expect(response).toBeDefined();
        expect(response).toContain('body');
        done();
      }, done).catch(done);
  });
  it('should return an error when call method getApprovalDialog and tokenizationClient fails', (done) => {
    logger.mute();
    let payment = paymentDriver.CREATED_OBJECT();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(payment);
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          replaceDocument: (modelSelf, model, callback) => {
            callback(null, payment);
          },
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, payment);
              },
            };
          },
        };
      });
    sandbox
      .stub(tokenizationClient, 'getById')
      .rejects(new Error('Error'));
    sandbox
      .stub(paymentClient, 'getBaseUrl')
      .resolves('www.dummy.com');
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex).toEqual(new Error('Error'));
        done();
      }).catch(done);
  });

  // approvePayment
  it('should return an error when call method approvePayment and requests fails', (done) => {
    logger.mute();
    let capture = captureDriver.VALID_CAPTURE();
    let gateway = gatewayDriver.VALID_GATEWAY();
    let payment = quickpayTokenDriver.CREATED_OBJECT();

    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          replaceDocument: (modelSelf, model, callback) => {
            callback(null, payment);
          },
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, payment);
              },
            };
          },
        };
      });

    sandbox
      .stub(paymentClient, 'getById')
      .resolves(payment);

    sandbox
      .stub(tokenizationClient, 'getById')
      .resolves(capture);

    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(configurationDriver.VALID_CONFIGURATION_OBJECT());

    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(new Error('DUMMY_ERROR'), null);
      });

    gatewayClient
      .approvePayment('DUMMY_PAYMENT_ID', gateway)
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.error_code).toEqual('REQUEST_ERROR');
        done();
      }).catch(done);
  });
  it('should return an error when call method approvePayment and update fails', (done) => {
    logger.mute();
    let capture = captureDriver.VALID_CAPTURE();
    let gateway = gatewayDriver.VALID_GATEWAY();
    let payment = quickpayTokenDriver.CREATED_OBJECT();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          replaceDocument: (modelSelf, model, callback) => {
            callback(null, payment);
          },
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, payment);
              },
            };
          },
        };
      });
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(payment);
    sandbox
      .stub(tokenizationClient, 'getById')
      .resolves(capture);
    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(configurationDriver.VALID_CONFIGURATION_OBJECT());

    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(null, null, cybersourceDriver.VALID_DECISIONMANAGER_RESPONSE());
      });
    sandbox
      .stub(paymentClient, 'updateToPaidState')
      .rejects(payment);
    gatewayClient
      .approvePayment('DUMMY_PAYMENT_ID', gateway)
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        done();
      }).catch(done);
  });
  it('should return a payment when call method approvePayment and is successful', (done) => {
    logger.mute();
    let capture = captureDriver.VALID_CAPTURE();
    let gateway = gatewayDriver.VALID_GATEWAY();
    let payment = quickpayTokenDriver.CREATED_OBJECT();
    delete payment.additional_attributes;
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          replaceDocument: (modelSelf, model, callback) => {
            callback(null, payment);
          },
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, payment);
              },
            };
          },
        };
      });
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(payment);
    sandbox
      .stub(tokenizationClient, 'getById')
      .resolves(capture);
    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(configurationDriver.VALID_CONFIGURATION_DECISION_MANAGER());
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(null, null, cybersourceDriver.VALID_DECISIONMANAGER_RESPONSE());
      });
    gatewayClient
      .approvePayment('DUMMY_PAYMENT_ID', gateway)
      .then((response) => {
        expect(response).toBeDefined();
        expect(response).toEqual(payment);
        done();
      });
  });

  // refundPayment
  it('should return an error when call method refundPayment and getById function fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .rejects(new Error('DUMMY_ERROR'));
    gatewayClient
      .refundPayment('DUMMY_PAYMENT_ID', 'DUMMY_REVERSE_MODEL')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an error when call method refundPayment have state reserved', (done) => {
    logger.mute();
    let payment = quickpayTokenDriver.VALID_PAYMENT_REFUNDED();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, payment);
              },
            };
          },
        };
      });
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(payment);
    gatewayClient
      .refundPayment('DUMMY_PAYMENT_ID', 'DUMMY_REVERSE_MODEL')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.error_code).toEqual('PAYMENT_ALREADY_REFUNDED');
        done();
      }).catch(done);
  });
  it('should return an error when call method refundPayment and configurations fails', (done) => {
    logger.mute();
    let payment = quickpayTokenDriver.VALID_PAYMENT_PAID();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, payment);
              },
            };
          },
        };
      });
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(payment);
    sandbox
      .stub(configurations, 'getConfiguration')
      .rejects(new Error('DUMMY_ERROR'));
    gatewayClient
      .refundPayment('DUMMY_PAYMENT_ID', {
        'refunded_amount': payment.transaction.amount.total,
      })
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex).toEqual(new Error('DUMMY_ERROR'));
        done();
      }).catch(done);
  });
  it('should return an error when call method refundPayment and request fails', (done) => {
    logger.mute();
    let payment = quickpayTokenDriver.VALID_PAYMENT_PAID();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, payment);
              },
            };
          },
        };
      });
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(payment);
    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(configurationDriver.VALID_CONFIGURATION_OBJECT());
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(new Error('DUMMY_ERROR'), null, null);
      });
    gatewayClient
      .refundPayment('DUMMY_PAYMENT_ID', {
        'refunded_amount': payment.transaction.amount.total,
      })
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.error_code).toEqual('REQUEST_ERROR');
        done();
      }).catch(done);
  });
  it('should return a payment when call method refundPayment and payment update fails', (done) => {
    logger.mute();
    let payment = quickpayTokenDriver.VALID_PAYMENT_PAID();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(payment);
    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(configurationDriver.VALID_CONFIGURATION_OBJECT());
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(null, null, cybersourceDriver.VALID_DECISIONMANAGER_RESPONSE());
      });
    sandbox
      .stub(paymentClient, 'updateToRefundedState')
      .rejects(new Error('DUMMY_ERROR'));

    gatewayClient
      .refundPayment('DUMMY_PAYMENT_ID',  quickpayTokenDriver.VALID_REFUNDED_AMOUNT())
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return a payment when call method refundPayment and is successful', (done) => {
    logger.mute();
    let payment = quickpayTokenDriver.VALID_PAYMENT_PAID();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, payment);
              },
            };
          },
          replaceDocument: (modelSelf, model, callback) => {
            callback(null, payment);
          },
        };
      });
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(payment);
    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(configurationDriver.VALID_CONFIGURATION_OBJECT());
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(null, null, cybersourceDriver.VALID_DECISIONMANAGER_RESPONSE());
      });
    gatewayClient
      .refundPayment('DUMMY_PAYMENT_ID', quickpayTokenDriver.VALID_REFUNDED_AMOUNT())
      .then((response) => {
        expect(response).toBeDefined();
        expect(response).toEqual(payment);
        done();
      });
  });

  it('should return an error when call method refundPayment without refunded_amount in model', (done) => {
    logger.mute();
    let payment = quickpayTokenDriver.VALID_PAYMENT_PAID();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, payment);
              },
            };
          },
        };
      });
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(payment);
    gatewayClient
      .refundPayment('DUMMY_PAYMENT_ID', {})
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.error_code).toEqual('INVALID_PARAMETER_REQUEST');
        done();
      }).catch(done);
  });

  it('should return an error when call method refundPayment with refunded_amount with text value', (done) => {
    logger.mute();
    let payment = quickpayTokenDriver.VALID_PAYMENT_PAID();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, payment);
              },
            };
          },
        };
      });
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(payment);
    gatewayClient
      .refundPayment('DUMMY_PAYMENT_ID', quickpayTokenDriver.INVALID_TEXT_REFUNDED_AMOUNT())
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.error_code).toEqual('INVALID_REFUNDED_AMOUNT');
        done();
      }).catch(done);
  });

  it('should return an error when call method refundPayment with refunded_amount with a value less than zero', (done) => {
    logger.mute();
    let payment = quickpayTokenDriver.VALID_PAYMENT_PAID();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, payment);
              },
            };
          },
        };
      });
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(payment);
    gatewayClient
      .refundPayment('DUMMY_PAYMENT_ID', quickpayTokenDriver.INVALID_NUMERIC_REFUNDED_AMOUNT())
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.error_code).toEqual('INVALID_REFUNDED_AMOUNT');
        done();
      }).catch(done);
  });

  // rejectPayment
  it('should return an error when call method rejectPayment and PaymentClient update fails', (done) => {
    logger.mute();
    let payment = quickpayTokenDriver.REJECTED_PAYMENT_OBJECT();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          replaceDocument: (modelSelf, model, callback) => {
            callback(new Error('Error'));
          },
        };
      });
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(payment);
    gatewayClient
      .rejectPayment('DUMMY_PAYMENT_ID', gatewayDriver.VALID_GATEWAY())
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        // expect(ex).toEqual(payment);
        done();
      }).catch(done);
  });
  it('should return a valid payment model when call method rejectPayment and is successful', (done) => {
    logger.mute();
    let payment = quickpayTokenDriver.REJECTED_PAYMENT_OBJECT();
    sandbox
      .stub(paymentClient, 'updateToRejectState')
      .resolves(payment);
    gatewayClient
      .rejectPayment('DUMMY_PAYMENT_ID', gatewayDriver.VALID_GATEWAY())
      .then((response) => {
        expect(response).toBeDefined();
        expect(response).toEqual(payment);
        done();
      });
  });

  // runDecisionManager
  it('should return an error when call method runDecisionManager and request fails', (done) => {
    logger.mute();
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(new Error('DUMMY_ERROR'), null, null);
      });
    gatewayClient
      .runDecisionManager(captureDriver.VALID_CAPTURE(), configurationDriver.VALID_CONFIGURATION_OBJECT(), quickpayTokenDriver.REJECTED_PAYMENT_OBJECT())
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.error_code).toEqual('REQUEST_ERROR');
        expect(ex.error_description).toEqual('Internal error, Soap request fails');
        done();
      }).catch(done);
  });
  it('should return an error when call method runDecisionManager and cybersource response doesnt have nodes', (done) => {
    logger.mute();
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(null, null, cybersourceDriver.RESPONSE_BODY_EMPTY());
      });
    gatewayClient
      .runDecisionManager(captureDriver.VALID_CAPTURE(), configurationDriver.VALID_CONFIGURATION_OBJECT(), quickpayTokenDriver.REJECTED_PAYMENT_OBJECT())
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.error_code).toEqual('GATEWAY_DATA_DONT_EXISTS');
        expect(ex.error_description).toEqual('Fraud Message from cybersource is empty');
        done();
      }).catch(done);
  });
  it('should return an error when call method runDecisionManager and reasonCode is not 100', (done) => {
    logger.mute();
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(null, null, cybersourceDriver.INVALID_DECISIONMANAGER_RESPONSE());
      });
    gatewayClient
      .runDecisionManager(captureDriver.VALID_CAPTURE(), configurationDriver.VALID_CONFIGURATION_OBJECT(), quickpayTokenDriver.REJECTED_PAYMENT_OBJECT())
      .then(() => {
        throw new Error('Expected Error but goqut OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.error_code).toContain('GW01_');
        done();
      }).catch(done);
  });
  it('should return a valid gatewayData when call method runDecisionManager and is successful', (done) => {
    logger.mute();
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(null, null, cybersourceDriver.VALID_DECISIONMANAGER_RESPONSE());
      });
    gatewayClient
      .runDecisionManager(captureDriver.VALID_CAPTURE(), configurationDriver.VALID_CONFIGURATION_OBJECT(), quickpayTokenDriver.REJECTED_PAYMENT_OBJECT())
      .then((response) => {
        expect(response).toBeDefined();
        expect(response.decision).toEqual('ACCEPT');
        expect(response.reasonCode).toEqual('100');
        done();
      });
  });

  // isInvalid
  it('should return false when call isInvalid method and model is valid', () => {
    expect(gatewayClient.isInvalid).toBeDefined();
    let result = gatewayClient.isInvalid(paymentDriver.VALID_MODEL());
    expect(result).toEqual(false);
  });
});
