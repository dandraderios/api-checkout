'use strict';
const sinon = require('sinon');
const logger = require('peanut-restify/logger');
const sandbox = sinon.createSandbox();
const bootstrap = require('./../../../../../drivers/bootstrap');

const app = require('./../../../../../../server/server');
const cardbillPaymentDriver = require('./../../../../../drivers/cardbill_payment');
const configurationDriver = require('./../../../../../drivers/configurations');
const responseDriver = require('./../../../../../drivers/responses');

describe('Endpoints > Gateways > Cardbill_payment_debit_bf  > Clients > gatewayClient', () => {
  let mockDB, gatewayClient, configurations, paymentClient, mockRequest;

  beforeAll(() => {
    bootstrap.setEnvironment();
    mockDB = require('peanut-restify/db');
    mockRequest = require('request');
    // Get the class to test
    configurations = require('./../../../../../../server/endpoints/configurations/clients/$configurations');
    paymentClient = require('./../../../../../../server/endpoints/payments/clients/paymentClient');
    gatewayClient = require('./../../../../../../server/endpoints/gateways/cardbill_payment_debit_bf/clients/gatewayClient');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should pass sanity check clients', () => {
    expect(gatewayClient).toBeDefined();
    expect(paymentClient).toBeDefined();
    expect(configurations).toBeDefined();
  });

  // isInvalid
  it('should pass sanity check method for isInvalid', () => {
    expect(gatewayClient.isInvalid).toBeDefined();
  });
  it('should return an error when call method isInvalid and model is invalid', () => {
    let result = gatewayClient.isInvalid(cardbillPaymentDriver.INVALID_MODEL());
    expect(result).toBeDefined();
    expect(result[0].kind).toEqual('required');
  });
  it('should return false when call method isInvalid and model is valid', () => {
    expect(gatewayClient.isInvalid).toBeDefined();
    let result = gatewayClient.isInvalid(cardbillPaymentDriver.VALID_MODEL());
    expect(result).toEqual(false);
  });

  // getApprovalDialog
  it('should pass sanity check method for getApprovalDialog', () => {
    expect(gatewayClient.getApprovalDialog).toBeDefined();
  });
  it('should return an error when call method getApprovalDialog throw getById of PaymentClient fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .rejects(new Error('DUMMY_ERROR'));
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an error when call method getApprovalDialog function getConfiguration fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(cardbillPaymentDriver.CREATED_OBJECT());
    sandbox
      .stub(configurations, 'getConfiguration')
      .rejects(new Error('DUMMY_ERROR'));
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an  error when call method getApprovalDialog make a request and fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(cardbillPaymentDriver.CREATED_OBJECT());
    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(configurationDriver.VALID_CONFIGURATION_OBJECT());
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(new Error('DUMMY_ERROR'), null);
      });
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return a CARDBILL_PAYMENT_DEBIT_BF_CREATE_INTENTION_ERROR  error when call method getApprovalDialog throw response status code is not equal to 200', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(cardbillPaymentDriver.CREATED_OBJECT());
    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(configurationDriver.VALID_CONFIGURATION_OBJECT());
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(null, 'DUMMY_CODE', {});
      });
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('CARDBILL_PAYMENT_DEBIT_BF_CREATE_INTENTION_ERROR');
        done();
      }).catch(done);
  });
  it('should return a INVALID_BODY_RESPONSE  error when call method getApprovalDialog make a request and the body is invalid', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(cardbillPaymentDriver.CREATED_OBJECT());
    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(configurationDriver.VALID_CONFIGURATION_OBJECT());
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(null, responseDriver.VALID_RESPONSE(), responseDriver.INVALID_RESPONSE());
      });
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('INVALID_BODY_RESPONSE');
        done();
      }).catch(done);
  });
  it('should return a html response when call method getApprovalDialog is successful', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(cardbillPaymentDriver.CREATED_OBJECT());
    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(configurationDriver.VALID_CONFIGURATION_OBJECT());
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(null, responseDriver.VALID_RESPONSE(), responseDriver.BODY_RESPONSE_QUICKPAY_OPERATIONS());
      });
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then((response) => {
        expect(response).toBeDefined();
        expect(response).toContain('<body>');
        done();
      }, done).catch(done);
  });

  // rejectPayment
  it('should pass sanity check method for rejectPayment', () => {
    expect(gatewayClient.rejectPayment).toBeDefined();
  });
  it('should return an Error when call method rejectPayment and getById function fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .rejects(new Error('DUMMY_ERROR'));
    gatewayClient
      .rejectPayment('PAYMENT_ID', 'CANCEL_DATA')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an  error when call method rejectPayment make an update of PaymentClient and fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(cardbillPaymentDriver.REJECTED_PAYMENT_OBJECT());
    sandbox
      .stub(paymentClient, 'updateToRejectState')
      .rejects(cardbillPaymentDriver.REJECTED_PAYMENT_OBJECT());
    gatewayClient
      .rejectPayment('PAYMENT_ID', 'CANCEL_DATA')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        cardbillPaymentDriver.isValidModel(ex);
        done();
      }).catch(done);
  });
  it('should return a valid cancel payment when call method rejectPayment is successful', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(cardbillPaymentDriver.REJECTED_PAYMENT_OBJECT());
    sandbox
      .stub(paymentClient, 'updateToRejectState')
      .resolves(cardbillPaymentDriver.REJECTED_PAYMENT_OBJECT());
    gatewayClient
      .rejectPayment('PAYMENT_ID', 'CANCEL_DATA')
      .then((response) => {
        expect(response).toBeDefined();
        cardbillPaymentDriver.isValidModel(response);
        done();
      }, done).catch(done);
  });

  // approvePayment
  it('should pass sanity check method for approvePayment', () => {
    expect(gatewayClient.approvePayment).toBeDefined();
  });
  it('should return an error when call method approvePayment and getById function fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .rejects(new Error('DUMMY_ERROR'));

    sandbox
      .stub(mockRequest, 'get')
      .callsFake((config, callback) => {
        callback(null, null, {
          status: 'DONE',
        });
      });

    gatewayClient
      .approvePayment('PAYMENT_ID', 'GATEWAY_DATA')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an error when call method approvePayment and request fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(cardbillPaymentDriver.CREATED_OBJECT());
    sandbox
      .stub(mockRequest, 'get')
      .callsFake((options, callback) => {
        callback(new Error('DUMMY_ERROR'), null);
      });
    gatewayClient
      .approvePayment('PAYMENT_ID', 'GATEWAY_DATA')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an rejected state when call method approvePayment and gateway data is not done', (done) => {
    logger.mute();
    sandbox
      .stub(mockRequest, 'get')
      .callsFake((options, callback) => {
        callback(null, null, {
          status: 'ERROR',
          paymentTransactionDetails: 'DUMMY',
        });
      });
    sandbox
      .stub(paymentClient, 'updateToRejectState')
      .resolves(cardbillPaymentDriver.CREATED_OBJECT());

    gatewayClient
      .approvePayment('PAYMENT_ID', 'GATEWAY_DATA')
      .then((payment) => {
        expect(payment).toBeDefined();
        expect(payment.intent).toEqual('sale');
        done();
      }, (ex) => {
        throw new Error('Expected Ok but got Error');
      }).catch(done);
  });
  it('should return a valid payment when call method approvePayment and is successful', (done) => {
    logger.mute();
    const body = {
      status: 'DONE',
      paymentTransactionDetails: {
        amount: 123,
        authorizationCode: 'DUMMY',
        cardLast4: 'DUMMY_CARD_4',
        cardFirst6: 'DUMMY_CARD_6',
      },
    };
    const gatewayData = {
      uuid: 'DUMMY',
    };
    const payment = cardbillPaymentDriver.CREATED_OBJECT();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(payment);
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          replaceDocument: (modelSelf, model, callback) => {
            callback(null, payment);
          },
        };
      });
    sandbox
      .stub(mockRequest, 'get')
      .callsFake((options, callback) => {
        callback(null, null, body);
      });
    sandbox
      .stub(paymentClient, 'updateToPaidState')
      .resolves(payment);
    gatewayClient
      .approvePayment('PAYMENT_ID', gatewayData)
      .then((response) => {
        expect(response).toBeDefined();
        cardbillPaymentDriver.isValidModel(response);
        done();
      }, done).catch(done);
  });
});
