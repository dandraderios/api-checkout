'use strict';
const sinon = require('sinon');
const supertest = require('supertest');
const logger = require('peanut-restify/logger');
const bootstrap = require('./../../../../../drivers/bootstrap');
const restifyRouter = require('restify-router');

const app = require('./../../../../../../server/server');
const paymentDriver = require('./../../../../../drivers/payments');
const responseDriver = require('./../../../../../drivers/responses');
const sandbox = sinon.createSandbox();

describe('POST /cardbill/payment_debit_bf/:id/execute', () => {
  // Setting things up....
  let gatewayClient, routeToTest, testServer, paymentClient, mockDB;
  const dummyId = '31109ad9-971f-5eba-5a7c-7a71f7ffc544';
  beforeAll(() => {
    bootstrap.setEnvironment();
    mockDB = require('peanut-restify/db');
    testServer = app.getServer();
    gatewayClient = require('./../../../../../../server/endpoints/gateways/cardbill_payment_debit_bf/clients/gatewayClient');
    paymentClient = require('./../../../../../../server/endpoints/payments/clients/paymentClient');
    routeToTest = require('./../../../../../../server/endpoints/gateways/cardbill_payment_debit_bf/routes/execute');
    expect(restifyRouter).toBeDefined();
    let router = new restifyRouter.Router();
    routeToTest(router);
    router.applyRoutes(testServer, 'test_route');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should return 200 status code when call method approvePayment and is successfull', (done) => {
    sandbox
      .stub(gatewayClient, 'approvePayment')
      .resolves(paymentDriver.CREATED_OBJECT());
    sandbox
      .stub(paymentClient, 'createSuccessRedirectionPage')
      .resolves('<html></html>');
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, [paymentDriver.CREATED_OBJECT()]);
              },
            };
          },
        };
      });
    supertest(testServer)
      .post(`/test_route/${dummyId}/execute`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send(responseDriver.VALID_RESPONSE())
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(200);
        expect(response).toHaveProperty('header');
        expect(response).toHaveProperty('body');
        done();
      });
  });
  it('should return a ErrorRedirectionPage when call method approvePayment and return a error', (done) => {
    sandbox
      .stub(gatewayClient, 'approvePayment')
      .rejects(paymentDriver.CREATED_OBJECT());
    sandbox
      .stub(paymentClient, 'createErrorRedirectionPage')
      .resolves('<html></html>');
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, [paymentDriver.CREATED_OBJECT()]);
              },
            };
          },
        };
      });
    supertest(testServer)
      .post(`/test_route/${dummyId}/execute`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send(responseDriver.VALID_RESPONSE())
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(200);
        expect(response).toHaveProperty('header');
        expect(response).toHaveProperty('body');
        done();
      });
  });
});
