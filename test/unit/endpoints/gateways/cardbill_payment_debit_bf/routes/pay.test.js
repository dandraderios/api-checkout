'use strict';
const sinon = require('sinon');
const supertest = require('supertest');
const logger = require('peanut-restify/logger');
const bootstrap = require('./../../../../../drivers/bootstrap');
const restifyRouter = require('restify-router');

const app = require('./../../../../../../server/server');
const sandbox = sinon.createSandbox();

describe('GET /cardbill/payment_debit_bf/:id/pay', () => {
  // Setting things up....
  let mockClient, routeToTest, testServer;
  const dummyId = '31109ad9-971f-5eba-5a7c-7a71f7ffc544';
  beforeAll(() => {
    bootstrap.setEnvironment();
    testServer = app.getServer();
    mockClient = require('./../../../../../../server/endpoints/gateways/cardbill_payment_debit_bf/clients/gatewayClient');
    routeToTest = require('./../../../../../../server/endpoints/gateways/cardbill_payment_debit_bf/routes/pay');
    expect(restifyRouter).toBeDefined();
    let router = new restifyRouter.Router();
    routeToTest(router);
    router.applyRoutes(testServer, 'test_route');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should return 500 (Server Error) status code when call method getApprovalDialog and Errors Occurs', (done) => {
    let expectedResult = new Error('DUMMY_ERROR');
    expectedResult.type = expectedResult.message;

    sandbox
      .stub(mockClient, 'getApprovalDialog')
      .rejects(expectedResult);

    supertest(testServer)
      .get(`/test_route/${dummyId}/pay`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send()
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(500);
        expect(response.body).toHaveProperty('type');
        expect(response.body.type).toEqual(expectedResult.type);
        done();
      });
  });
  it('should return 200 status code when call method getApprovalDialog and is successfull', (done) => {
    let expectedResult = '<html></html>';
    sandbox
      .stub(mockClient, 'getApprovalDialog')
      .resolves(expectedResult);

    supertest(testServer)
      .get(`/test_route/${dummyId}/pay`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send()
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(200);
        expect(response).toHaveProperty('header');
        expect(response).toHaveProperty('body');
        done();
      });
  });
});
