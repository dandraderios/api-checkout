'use strict';
const sinon = require('sinon');
const supertest = require('supertest');
const logger = require('peanut-restify/logger');
const bootstrap = require('./../../../../../drivers/bootstrap');
const paymentDriver = require('./../../../../../drivers/payments');
const responseDriver = require('./../../../../../drivers/responses');
const restifyRouter = require('restify-router');
const errors = require('./../../../../../../server/utils/errors');

const driver = require('./../../../../../drivers/configurations');
const sandbox = sinon.createSandbox();

describe('POST /quickpay/credit/:id/execute', () => {
  let mockGatewayClient, paymentClient, routeToTest, testServer;
  const dummyId = '31109ad9-971f-5eba-5a7c-7a71f7ffc544';
  beforeAll(() => {
    bootstrap.setEnvironment();

    const app = require('./../../../../../../server/server');
    paymentClient = require('./../../../../../../server/endpoints/payments/clients/paymentClient');
    mockGatewayClient = require('./../../../../../../server/endpoints/gateways/quickpay_credit/clients/gatewayClient');
    routeToTest = require('./../../../../../../server/endpoints/gateways/quickpay_credit/routes/execute');
    testServer = app.getServer();
    expect(restifyRouter).toBeDefined();

    const router = new restifyRouter.Router();
    routeToTest(router);

    router.applyRoutes(testServer, 'test_route');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should pass sanity Client', () => {
    expect(mockGatewayClient).toBeDefined();
  });
  it('should return a error when call method approvePayment and fails', (done) => {
    sandbox
      .stub(mockGatewayClient, 'approvePayment')
      .rejects(new errors.PeinauUnloggedError('DUMMY_ERROR', 'Dummy Description'));

    supertest(testServer)
      .post(`/test_route/${dummyId}/execute`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send(responseDriver.VALID_RESPONSE())
      .end((err, response) => {
        expect(response).toBeDefined();
        expect(response.statusCode).toEqual(500);
        expect(response.body.error_code).toEqual('DUMMY_ERROR');
        done();
      });
  });
  it('should return 200 status code when call method approvePayment and is successfull', (done) => {
    sandbox
      .stub(mockGatewayClient, 'approvePayment')
      .resolves(paymentDriver.PAID_OBJECT());
    sandbox
      .stub(paymentClient, 'createSuccessRedirectionPage')
      .resolves('<html></html>');

    supertest(testServer)
      .post(`/test_route/${dummyId}/execute`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send(responseDriver.VALID_RESPONSE())
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(200);
        expect(response).toHaveProperty('header');
        expect(response).toHaveProperty('body');
        done();
      });
  });
});
