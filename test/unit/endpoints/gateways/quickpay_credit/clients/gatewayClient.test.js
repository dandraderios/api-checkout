'use strict';
const sinon = require('sinon');
const logger = require('peanut-restify/logger');
const sandbox = sinon.createSandbox();
const bootstrap = require('./../../../../../drivers/bootstrap');

const app = require('./../../../../../../server/server');
const quickpayDriver = require('./../../../../../drivers/quickpay_credit');
const configurationDriver = require('./../../../../../drivers/configurations');
const responseDriver = require('./../../../../../drivers/responses');
const gatewayDriver = require('./../../../../../drivers/gateways');

describe('Endpoints > Gateways > Quickpay_credit  > Clients > gatewayClient', () => {
  let mockDB, gatewayClient, configurations, paymentClient, mockRequest;

  beforeAll(() => {
    bootstrap.setEnvironment();
    mockDB = require('peanut-restify/db');
    mockRequest = require('request');
    // Get the class to test
    configurations = require('./../../../../../../server/endpoints/configurations/clients/$configurations');
    paymentClient = require('./../../../../../../server/endpoints/payments/clients/paymentClient');
    gatewayClient = require('./../../../../../../server/endpoints/gateways/quickpay_credit/clients/gatewayClient');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should pass sanity clients', () => {
    expect(gatewayClient).toBeDefined();
    expect(paymentClient).toBeDefined();
    expect(configurations).toBeDefined();
  });

  // getAdditionalLinks
  it('should return an empty array when call method getAdditionalLinks', () => {
    expect(gatewayClient.getAdditionalLinks).toBeDefined();
    let result = gatewayClient.getAdditionalLinks();
    expect(result).toEqual([]);
  });

  // getApprovalDialog
  it('should return an error when call method getApprovalDialog and getById function fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .rejects(new Error('DUMMY_ERROR'));
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an error when call method getApprovalDialog and the function getConfiguration fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(quickpayDriver.CREATED_OBJECT());
    sandbox
      .stub(configurations, 'getConfiguration')
      .rejects(new Error('DUMMY_ERROR'));
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return a html response when call method getApprovalDialog is successful', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(quickpayDriver.CREATED_OBJECT());
    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(configurationDriver.VALID_CONFIGURATION_OBJECT());
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then((response) => {
        expect(response).toBeDefined();
        expect(response).toContain('<body>');
        done();
      }, done).catch(done);
  });

  // approvePayment
  it('should return an error when call method approvePayment and getById throw an error', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .rejects(new Error('DUMMY_ERROR'));
    gatewayClient
      .approvePayment('DUMMY_PAYMENT_ID', 'DUMMY_GATEWAY_DATA')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return a payment object when call method approvePayment and update fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'updateToPaidState')
      .rejects(new Error('DUMMY_ERROR'));
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(quickpayDriver.CREATED_OBJECT());
    gatewayClient
      .approvePayment('DUMMY_PAYMENT_ID', gatewayDriver.VALID_GATEWAY_QUICKPAY_CREDIT())
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }, done).catch(done);
  });
  it('should return a valid payment object when call method approvePayment and is successsful', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'updateToPaidState')
      .resolves(quickpayDriver.VALID_PAYMENT_PAID());

    gatewayClient
      .approvePayment('DUMMY_PAYMENT_ID', gatewayDriver.VALID_GATEWAY_QUICKPAY_CREDIT())
      .then((response) => {
        expect(response).toBeDefined();
        expect(response.state).toEqual('paid');
        quickpayDriver.isValidModel(response);
        done();
      }, done).catch(done);
  });

  // rejectPayment
  it('should return an error when call method rejectPayment and getById function fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .rejects(new Error('DUMMY_ERROR'));
    gatewayClient
      .rejectPayment('DUMMY_PAYMENT_ID', 'DUMMY_GATEWAY_DATA')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an error when call method rejectPayment and update fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'updateToRejectState')
      .rejects(new Error('DUMMY_ERROR'));

    gatewayClient
      .rejectPayment('DUMMY_PAYMENT_ID', gatewayDriver.VALID_GATEWAY_QUICKPAY_CREDIT())
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return a valid payment object when call method rejectPayment and is successsful', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'updateToRejectState')
      .resolves(quickpayDriver.REJECTED_PAYMENT());

    gatewayClient
      .rejectPayment('DUMMY_PAYMENT_ID', gatewayDriver.VALID_GATEWAY_QUICKPAY_CREDIT())
      .then((response) => {
        expect(response).toBeDefined();
        expect(response.state).toEqual('rejected');
        quickpayDriver.isValidModel(response);
        done();
      }, done).catch(done);
  });
});
