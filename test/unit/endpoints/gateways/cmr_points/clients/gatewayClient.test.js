'use strict';
const sinon = require('sinon');
const logger = require('peanut-restify/logger');
const sandbox = sinon.createSandbox();
const bootstrap = require('./../../../../../drivers/bootstrap');

const app = require('./../../../../../../server/server');
const paymentDriver = require('./../../../../../drivers/payments');
const cmrPointsDriver = require('./../../../../../drivers/cmr_points');
const configurationDriver = require('./../../../../../drivers/configurations');

describe('Endpoints > Gateways > Cmr_points  > Clients > gatewayClient', () => {
  let mockDB, gatewayClient, configurations, paymentClient, mockRequest, mockCMRCipher;

  beforeAll(() => {
    bootstrap.setEnvironment();
    mockDB = require('peanut-restify/db');
    mockRequest = require('request');
    mockCMRCipher = require('./../../../../../../server/endpoints/gateways/cmr_points/third_party/CMRCypher');

    sinon
      .stub(mockCMRCipher, 'encrypt')
      .returns('ENCRYPTED');

    sinon
      .stub(mockCMRCipher, 'decrypt')
      .returns('DECRYPTED');

    // Get the class to test
    configurations = require('./../../../../../../server/endpoints/configurations/clients/$configurations');
    paymentClient = require('./../../../../../../server/endpoints/payments/clients/paymentClient');
    gatewayClient = require('./../../../../../../server/endpoints/gateways/cmr_points/clients/gatewayClient');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should pass sanity clients', () => {
    expect(gatewayClient).toBeDefined();
    expect(paymentClient).toBeDefined();
    expect(configurations).toBeDefined();
  });

  // isInvalid
  it('should pass sanity check method for isInvalid', () => {
    expect(gatewayClient.isInvalid).toBeDefined();
  });

  // getApprovalDialog
  it('should pass sanity check method for getApprovalDialog', () => {
    expect(gatewayClient.getApprovalDialog).toBeDefined();
  });
  it('should return an error when call method getApprovalDialog  and getConnection and throw an exception', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .throwsException(new Error('DUMMY_ERROR'));
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID', 'REQ')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('An error has ocurred trying to find a payment by Id');
        done();
      }).catch(done);
  });

  it('should return an error when call method getApprovalDialog and method getById throws an error', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .rejects(new Error('DUMMY_ERROR'));
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID', 'req')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an error when call method getApprovalDialog and function getConfiguration fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(cmrPointsDriver.CREATED_OBJECT());
    sandbox
      .stub(configurations, 'getConfiguration')
      .rejects(new Error('DUMMY_ERROR'));
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  /*
  it('should return an error when call method getApprovalDialog make a request and fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(cmrPointsDriver.CREATED_OBJECT());
    sandbox
      .stub(paymentClient, 'getBaseUrl')
      .resolves('www.dummy.com');
    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(configurationDriver.VALID_CONFIGURATION_OBJECT());
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID', 'REQ')
      .then((response) => {
        expect(response).toBeDefined();
        expect(response).toContain('<body>');
        done();
      }, done).catch(done);
  }); */
});
