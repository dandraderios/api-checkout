'use strict';
const sinon = require('sinon');
const logger = require('peanut-restify/logger');
const sandbox = sinon.createSandbox();
describe('Cmr Points > Third Party > CMRCypher', () => {
  let CMRCypher, mockExec;

  beforeAll(() => {
    // Get the class to test
    mockExec = require('child_process');
    sinon.stub(mockExec, 'execSync').returns('DUMMY');

    CMRCypher = require('../../../../../../server/endpoints/gateways/cmr_points/third_party/CMRCypher');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should pass sanity', () => {
    expect(CMRCypher).toBeDefined();
  });

  it('should return an encrypt text when call method encrypt', (done) => {
    expect(CMRCypher.encrypt).toBeDefined();
    CMRCypher
      .encrypt('DUMMY_TEXT')
      .then((encriptedText) => {
        expect(encriptedText).toContain('DUMMY');
        done();
      }, done);
  });

  it('should return an decrypt text when call method encrypt', (done) => {
    expect(CMRCypher.decrypt).toBeDefined();
    CMRCypher
      .encrypt('ENCRYPTED_TEXT')
      .then((decriptedText) => {
        expect(decriptedText).toContain('DUMMY');
        done();
      }, done);
  });
});
