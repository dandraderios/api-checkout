'use strict';
const sinon = require('sinon');
const logger = require('peanut-restify/logger');
const sandbox = sinon.createSandbox();
const bootstrap = require('./../../../../../drivers/bootstrap');

const app = require('./../../../../../../server/server');
const selfCheckoutDriver = require('./../../../../../drivers/self_checkout');
const paymentDriver = require('./../../../../../drivers/payments');
const configurationDriver = require('./../../../../../drivers/configurations');
const gatewayDriver = require('./../../../../../drivers/gateways');
const responseDriver = require('./../../../../../drivers/responses');

describe('Endpoints > Gateways > Self_checkout  > Clients > gatewayClient', () => {
  let mockDB, gatewayClient, configurations, paymentClient, mockRequest;

  beforeAll(() => {
    bootstrap.setEnvironment();
    mockRequest = require('request');
    mockDB = require('peanut-restify/db');
    // Get the class to test
    configurations = require('./../../../../../../server/endpoints/configurations/clients/$configurations');
    paymentClient = require('./../../../../../../server/endpoints/payments/clients/paymentClient');
    gatewayClient = require('./../../../../../../server/endpoints/gateways/self_checkout/clients/gatewayClient');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should pass sanity clients', () => {
    expect(gatewayClient).toBeDefined();
    expect(paymentClient).toBeDefined();
    expect(configurations).toBeDefined();
  });

  // getAdditionalLinks
  it('should return an empty array when call method getAdditionalLinks', () => {
    expect(gatewayClient.getAdditionalLinks).toBeDefined();
    let result = gatewayClient.getAdditionalLinks('DUMMY_PAY_ROUTE');
    expect(result).toEqual([]);
  });

  // getApprovalDialog
  it('should return an error when call method getApprovalDialog and getById function fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .rejects(new Error('DUMMY_ERROR'));
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an error when call method getApprovalDialog and getConfiguration  function fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(selfCheckoutDriver.CREATED_OBJECT());
    sandbox
      .stub(configurations, 'getConfiguration')
      .rejects(new Error('DUMMY_ERROR'));
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an error when call method getApprovalDialog and request fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(selfCheckoutDriver.VALID_PAYMENT_PAID());
    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(configurationDriver.VALID_CONFIGURATION_OBJECT());
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(new Error('DUMMY_ERROR'), null);
      });
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an error when call method getApprovalDialog and request fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(selfCheckoutDriver.VALID_PAYMENT_PAID());
    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(configurationDriver.VALID_CONFIGURATION_OBJECT());
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(null, responseDriver.VALID_RESPONSE_WITH_CODE_1().statusCode, responseDriver.VALID_RESPONSE_WITH_CODE_1());
      });
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('BAD_RESPONSE');
        done();
      }).catch(done);
  });
  it('should return a html response when call method getApprovalDialog and is successful', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(selfCheckoutDriver.VALID_PAYMENT_PAID());
    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(configurationDriver.VALID_CONFIGURATION_OBJECT());
    sandbox
      .stub(paymentClient, 'getBaseUrl')
      .resolves('www.dummy.com');
    sandbox
      .stub(mockRequest, 'post')
      .callsFake((options, callback) => {
        callback(null, responseDriver.VALID_RESPONSE_WITH_CODE_0().statusCode, responseDriver.VALID_RESPONSE_WITH_CODE_0());
      });
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then((response) => {
        expect(response).toBeDefined();
        expect(response).toContain('<body>');
        expect(response).toContain('<head>');
        expect(response).toContain('html');
        done();
      }, done).catch(done);
  });

  // approvePayment
  it('should return a payment object when call method approvePayment and update fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'updateToPaidState')
      .rejects(new Error('DUMMY_ERROR'));

    gatewayClient
      .approvePayment('DUMMY_PAYMENT_ID', gatewayDriver.VALID_GATEWAY_QUICKPAY_CREDIT())
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }, done).catch(done);
  });
  it('should return a valid payment object when call method approvePayment and is successsful', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'updateToPaidState')
      .resolves(selfCheckoutDriver.VALID_PAYMENT_PAID());

    gatewayClient
      .approvePayment('DUMMY_PAYMENT_ID', gatewayDriver.VALID_GATEWAY_QUICKPAY_CREDIT())
      .then((response) => {
        expect(response).toBeDefined();
        expect(response).toHaveProperty('payer');
        expect(response).toHaveProperty('links');
        expect(response).toHaveProperty('create_time');
        expect(response).toHaveProperty('id');
        expect(response).toHaveProperty('state');
        expect(response.state).toEqual('paid');
        done();
      }, done).catch(done);
  });

  // rejectPayment
  it('should return an error when call method rejectPayment and getById function fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .rejects(new Error('DUMMY_ERROR'));
    gatewayClient
      .rejectPayment('DUMMY_PAYMENT_ID', 'DUMMY_GATEWAY_DATA')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an payment when call method rejectPayment and update fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'updateToRejectState')
      .rejects(new Error('DUMMY_ERROR'));
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(selfCheckoutDriver.CREATED_OBJECT());
    gatewayClient
      .rejectPayment('DUMMY_PAYMENT_ID', gatewayDriver.VALID_GATEWAY_QUICKPAY_CREDIT())
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return a valid payment object when call method rejectPayment is successsful', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'updateToRejectState')
      .resolves(selfCheckoutDriver.REJECTED_PAYMENT_OBJECT());
    gatewayClient
      .rejectPayment('DUMMY_PAYMENT_ID', gatewayDriver.VALID_GATEWAY_QUICKPAY_CREDIT())
      .then((response) => {
        expect(response).toBeDefined();
        selfCheckoutDriver.isValidModel(response);
        expect(response.state).toEqual('rejected');
        done();
      }, done).catch(done);
  });
});
