'use strict';
const sinon = require('sinon');
const logger = require('peanut-restify/logger');
const sandbox = sinon.createSandbox();
const bootstrap = require('./../../../../../drivers/bootstrap');

const app = require('./../../../../../../server/server');
const paymentDriver = require('./../../../../../drivers/payments');
const configurationDriver = require('./../../../../../drivers/configurations');

describe('Endpoints > Gateways > Transbank WebPay  > Clients > gatewayClient', () => {
  let mockDB, gatewayClient, configurations, paymentClient, mockRequest;

  beforeAll(() => {
    bootstrap.setEnvironment();
    mockRequest = require('request');
    mockDB = require('peanut-restify/db');
    // Get the class to test
    configurations = require('./../../../../../../server/endpoints/configurations/clients/$configurations');
    paymentClient = require('./../../../../../../server/endpoints/payments/clients/paymentClient');
    gatewayClient = require('./../../../../../../server/endpoints/gateways/transbank_webpay/clients/gatewayClient');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should pass sanity clients', () => {
    expect(gatewayClient).toBeDefined();
    expect(paymentClient).toBeDefined();
    expect(configurations).toBeDefined();
  });

  // getAdditionalLinks
  it('should return an empty array when call method getAdditionalLinks', () => {
    expect(gatewayClient.getAdditionalLinks).toBeDefined();
    let result = gatewayClient.getAdditionalLinks('DUMMY_PAY_ROUTE');
    expect(result).toEqual([]);
  });

  // getApprovalDialog
  it('should return an error when call method getApprovalDialog and getById function fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .rejects(new Error('DUMMY_ERROR'));
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an error when call method getApprovalDialog and getConfiguration  function fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .resolves(paymentDriver.CREATED_OBJECT());
    sandbox
      .stub(configurations, 'getConfiguration')
      .rejects(new Error('DUMMY_ERROR'));
    gatewayClient
      .getApprovalDialog('DUMMY_PAYMENT_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });

  // rejectPayment
  it('should return an error when call method rejectPayment and getById function fails', (done) => {
    logger.mute();
    sandbox
      .stub(paymentClient, 'getById')
      .rejects(new Error('DUMMY_ERROR'));
    gatewayClient
      .rejectPayment('DUMMY_PAYMENT_ID', 'DUMMY_GATEWAY_DATA')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });

  // resolveTBKConfiguration
  it('should return a TBK Configuration when call resolveTBKConfiguration', () => {
    logger.mute();

    const config = gatewayClient.resolveTBKConfiguration({
      'commerceCode': 'DUMMY_WEBPAY_KEY',
      'PRIVATE_KEY': 'DUMMY_PRIVATE_KEY',
      'PUBLIC_KEY': 'DUMMY_PUBLIC_KEY',
    });
  });
});
