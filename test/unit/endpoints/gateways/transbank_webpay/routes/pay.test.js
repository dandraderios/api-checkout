'use strict';
const sinon = require('sinon');
const supertest = require('supertest');
const logger = require('peanut-restify/logger');
const bootstrap = require('./../../../../../drivers/bootstrap');
const restifyRouter = require('restify-router');

const app = require('./../../../../../../server/server');
const driver = require('./../../../../../drivers/configurations');
const sandbox = sinon.createSandbox();

describe('GET /payments/gateways/transbank/webpay/:id/pay', () => {
  let gatewayClient, routeToTest, testServer;
  const dummyId = '31109ad9-971f-5eba-5a7c-7a71f7ffc544';
  beforeAll(() => {
    bootstrap.setEnvironment();

    testServer = app.getServer();
    routeToTest = require('./../../../../../../server/endpoints/gateways/transbank_webpay/routes/pay');
    gatewayClient = require('./../../../../../../server/endpoints/gateways/transbank_webpay/clients/gatewayClient');
    expect(restifyRouter).toBeDefined();

    const router = new restifyRouter.Router();
    routeToTest(router);

    router.applyRoutes(testServer, 'test_route');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should return 500 (Server Error) status code when call method and Errors Occurs', (done) => {
    logger.mute();
    const expectedResult = new Error('DUMMY_ERROR');
    expectedResult.type = expectedResult.message;

    sandbox
      .stub(gatewayClient, 'getApprovalDialog')
      .rejects(expectedResult);

    supertest(testServer)
      .get(`/test_route/${dummyId}/pay`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send()
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(500);
        expect(response).toHaveProperty('error');
        done();
      });
  });
  it('should return 200 status code when call method successfully', (done) => {
    logger.mute();
    const expectedResult = '<html></html>';
    sandbox
      .stub(gatewayClient, 'getApprovalDialog')
      .resolves(expectedResult);

    supertest(testServer)
      .get(`/test_route/${dummyId}/pay`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send()
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(200);
        expect(response).toHaveProperty('header');
        expect(response).toHaveProperty('body');
        done();
      });
  });
});
