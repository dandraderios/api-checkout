'use strict';
const sinon = require('sinon');
const supertest = require('supertest');
const logger = require('peanut-restify/logger');
const bootstrap = require('./../../../../../drivers/bootstrap');
const restifyRouter = require('restify-router');

const app = require('./../../../../../../server/server');
const transbankWebpayDriver = require('./../../../../../drivers/transbank_webpay');
const responseDriver = require('./../../../../../drivers/responses');
const paymentDriver = require('./../../../../../drivers/payments');
const driver = require('./../../../../../drivers/configurations');
const sandbox = sinon.createSandbox();

describe('POST /payments/gateways/transbank/webpay/:id/execute', () => {
  let gatewayClient, routeToTest, testServer, paymentClient, mockDB;
  const dummyId = '31109ad9-971f-5eba-5a7c-7a71f7ffc544';
  beforeAll(() => {
    bootstrap.setEnvironment();
    mockDB = require('peanut-restify/db');
    testServer = app.getServer();
    routeToTest = require('./../../../../../../server/endpoints/gateways/transbank_webpay/routes/execute');
    paymentClient = require('./../../../../../../server/endpoints/payments/clients/paymentClient');
    gatewayClient = require('./../../../../../../server/endpoints/gateways/transbank_webpay/clients/gatewayClient');
    expect(restifyRouter).toBeDefined();

    const router = new restifyRouter.Router();
    routeToTest(router);

    router.applyRoutes(testServer, 'test_route');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should pass sanity Client', () => {
    expect(gatewayClient).toBeDefined();
  });
  it('should return 200 status code and OK html when call method successfully', (done) => {
    logger.mute();
    sandbox
      .stub(gatewayClient, 'processPayment')
      .resolves(transbankWebpayDriver.VALID_PAYMENT_PAID());
    sandbox
      .stub(paymentClient, 'createSuccessRedirectionPage')
      .resolves('<html></html>');

    supertest(testServer)
      .post(`/test_route/${dummyId}/execute`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send({
        'token_ws': 'DUMMY_TOKEN',
      })
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(200);
        expect(response).toHaveProperty('header');
        expect(response).toHaveProperty('body');
        done();
      });
  });
  it('should return a error when call method gatewayClient without token_ws', (done) => {
    logger.mute();
    sandbox
      .stub(gatewayClient, 'processPayment')
      .rejects(new Error('DUMMY_ERROR'));

    supertest(testServer)
      .post(`/test_route/${dummyId}/execute`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send({})
      .end((err, response) => {
        expect(response).toBeDefined();
        expect(response.status).toEqual(500);
        expect(response.body.error_code).toEqual('TOKEN_WS_IS_REQUIRED');
        done();
      });
  });
});
