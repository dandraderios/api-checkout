'use strict';
const sinon = require('sinon');
const supertest = require('supertest');
const logger = require('peanut-restify/logger');
const bootstrap = require('./../../../../../drivers/bootstrap');
const restifyRouter = require('restify-router');

const app = require('./../../../../../../server/server');
const driver = require('./../../../../../drivers/configurations');
const transbankWebpayDriver = require('./../../../../../drivers/transbank_webpay');
const sandbox = sinon.createSandbox();

describe('GET /payments/gateways/transbank/webpay/:id/reject', () => {
  logger.mute();
  let gatewayClient, routeToTest, testServer, paymentClient, mockDB;
  const dummyId = '31109ad9-971f-5eba-5a7c-7a71f7ffc544';
  beforeAll(() => {
    bootstrap.setEnvironment();

    testServer = app.getServer();
    mockDB = require('peanut-restify/db');
    routeToTest = require('./../../../../../../server/endpoints/gateways/transbank_webpay/routes/reject');
    paymentClient = require('./../../../../../../server/endpoints/payments/clients/paymentClient');
    gatewayClient = require('./../../../../../../server/endpoints/gateways/transbank_webpay/clients/gatewayClient');
    expect(restifyRouter).toBeDefined();

    const router = new restifyRouter.Router();
    routeToTest(router);

    router.applyRoutes(testServer, 'test_route');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should return a ErrorRedirectionPage when call method rejectPayment', (done) => {
    logger.mute();
    const params = {
      id: 'dummId',
      body: {
        message: 'dummyMessage',
      },
    };
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, [transbankWebpayDriver.REJECTED_PAYMENT_OBJECT()]);
              },
            };
          },
        };
      });
    sandbox
      .stub(gatewayClient, 'rejectPayment')
      .resolves(transbankWebpayDriver.REJECTED_PAYMENT_OBJECT());
    sandbox
      .stub(paymentClient, 'createErrorRedirectionPage')
      .resolves('<html></html>');
    supertest(testServer)
      .post(`/test_route/${dummyId}/reject`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send(params)
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(200);
        expect(response).toHaveProperty('header');
        expect(response).toHaveProperty('body');
        done();
      });
  });
});
