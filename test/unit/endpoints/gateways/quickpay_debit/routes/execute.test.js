'use strict';
const sinon = require('sinon');
const supertest = require('supertest');
const logger = require('peanut-restify/logger');
const bootstrap = require('./../../../../../drivers/bootstrap');
const restifyRouter = require('restify-router');

const app = require('./../../../../../../server/server');
const paymentDriver = require('./../../../../../drivers/payments');
const responseDriver = require('./../../../../../drivers/responses');
const driver = require('./../../../../../drivers/configurations');
const errors = require('./../../../../../../server/utils/errors');
const sandbox = sinon.createSandbox();

describe('GET /payments/gateways/quickpay/debit/:id/execute', () => {
  let gatewayClient, routeToTest, testServer, paymentClient, mockDB;
  const dummyId = '31109ad9-971f-5eba-5a7c-7a71f7ffc544';
  beforeAll(() => {
    bootstrap.setEnvironment();
    mockDB = require('peanut-restify/db');
    testServer = app.getServer();
    routeToTest = require('./../../../../../../server/endpoints/gateways/quickpay_debit/routes/execute');
    paymentClient = require('./../../../../../../server/endpoints/payments/clients/paymentClient');
    gatewayClient = require('./../../../../../../server/endpoints/gateways/quickpay_debit/clients/gatewayClient');
    expect(restifyRouter).toBeDefined();

    const router = new restifyRouter.Router();
    routeToTest(router);

    router.applyRoutes(testServer, 'test_route');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should pass sanity Client', () => {
    expect(gatewayClient).toBeDefined();
  });
  it('should return 200 status code when call method approvePayment and is successfull', (done) => {
    sandbox
      .stub(gatewayClient, 'approvePayment')
      .resolves(paymentDriver.PAID_OBJECT());
    sandbox
      .stub(paymentClient, 'createSuccessRedirectionPage')
      .resolves('<html></html>');
    supertest(testServer)
      .post(`/test_route/${dummyId}/execute`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send(responseDriver.VALID_RESPONSE())
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(200);
        expect(response).toHaveProperty('header');
        expect(response).toHaveProperty('body');
        done();
      });
  });
  /**
   * TODO: REMOVE THIS COMMENTS
  it('should return a error when call method approvePayment and fails', (done) => {
    sandbox
      .stub(gatewayClient, 'approvePayment')
      .rejects(new errors.PeinauUnloggedError('DUMMY_ERROR', 'Dummy Description'));

    sandbox
      .stub(paymentClient, 'createErrorRedirectionPage')
      .resolves('<html></html>');
    supertest(testServer)
      .post(`/test_route/${dummyId}/execute`)
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send(responseDriver.VALID_RESPONSE())
      .end((err, response) => {
        console.log(err);
        console.log(response);
        expect(response).toBeDefined();
        expect(response.statusCode).toEqual(500);
        expect(response.body.error_code).toEqual('DUMMY_ERROR');
        done();
      });
  });
   */
});
