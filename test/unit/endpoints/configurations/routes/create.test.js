'use strict';
const sinon = require('sinon');
const supertest = require('supertest');
const logger = require('peanut-restify/logger');
const bootstrap = require('./../../../../drivers/bootstrap');
const configurationDriver = require('./../../../../drivers/configurations');
const restifyRouter = require('restify-router');

const app = require('./../../../../../server/server');
const sandbox = sinon.createSandbox();

describe('POST /:application/configurations/:payment_method', () => {
  // Setting things up....
  let configurations, routeToTest, testServer;
  beforeAll(() => {
    bootstrap.setEnvironment();
    testServer = app.getServer();
    configurations = require('./../../../../../server/endpoints/configurations/clients/$configurations');
    routeToTest = require('./../../../../../server/endpoints/configurations/routes/create');
    expect(restifyRouter).toBeDefined();
    let router = new restifyRouter.Router();
    routeToTest(router);
    router.applyRoutes(testServer, 'test_route');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should return 500 (Server Error) status code when call method updateConfiguration and Errors Occurs', (done) => {
    let expectedResult = new Error('DUMMY_ERROR');
    expectedResult.type = expectedResult.message;
    let body = {
      'application': 'D4819F1W2F6Q84W86F',
      'payment_method': 'QUICKPAY_CREDIT',
    };
    sandbox
      .stub(configurations, 'updateConfiguration')
      .rejects(expectedResult);

    supertest(testServer)
      .post('/test_route')
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send(body)
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(500);
        done();
      });
  });
  it('should return 200 status code when call method updateConfiguration and is successfull', (done) => {
    let body = {
      'application': 'D4819F1W2F6Q84W86F',
      'payment_method': 'QUICKPAY_CREDIT',
    };
    let expectedResult = Object.assign(configurationDriver.VALID_CONFIGURATION_OBJECT(), {
      toJSON: () => {
        return configurationDriver.VALID_CONFIGURATION_OBJECT();
      },
    });
    sandbox
      .stub(configurations, 'updateConfiguration')
      .resolves(expectedResult);
    supertest(testServer)
      .post('/test_route')
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send(body)
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(201);
        expect(response).toHaveProperty('header');
        expect(response).toHaveProperty('body');
        done();
      });
  });
});
