'use strict';
const sinon = require('sinon');
const logger = require('peanut-restify/logger');
const sandbox = sinon.createSandbox();
const proxyquire = require('proxyquire');
const bootstrap = require('../../../../drivers/bootstrap');

const configurationDriver = require('../../../../drivers/configurations');

describe('Endpoints > Configurations > Clients > configurations', () => {
  let app, mockDB, configurations;

  beforeAll(() => {
    bootstrap.setEnvironment();
    app = require('../../../../../server/server');
    mockDB = require('peanut-restify/db');
    // Get the class to test
    configurations = require('./../../../../../server/endpoints/configurations/clients/$configurations');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should pass sanity check client', () => {
    expect(configurations).toBeDefined();
  });

  // get configuration method
  it('should pass sanity check method for getConfiguration', () => {
    expect(configurations.getConfiguration).toBeDefined();
  });
  it('should return an  error when call method getConfiguration and getConnection throws an Exception', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .throwsException(new Error('DUMMY_ERROR'));
    configurations
      .getConfiguration('DUMMY_ID', 'DUMMY_PAYMENT_METHOD', 'DUMMY_SKIP_ERRORS')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return an  error when call method getConfiguration has a DB error', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(new Error('DUMMY_ERROR'), null);
              },
            };
          },
        };
      });
    configurations
      .getConfiguration('DUMMY_ID', 'DUMMY_PAYMENT_METHOD', 'DUMMY_SKIP_ERRORS')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return GATEWAY_NOT_CONFIGURED when call method getConfiguration has parameter skipErrors in false', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, []);
              },
            };
          },
        };
      });
    configurations
      .getConfiguration('DUMMY_ID', 'DUMMY_PAYMENT_METHOD', false)
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex).toEqual('GATEWAY_NOT_CONFIGURED');
        done();
      }).catch(done);
  });
  it('should return null when call method getConfiguration has parameter skipErrors in true', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, []);
              },
            };
          },
        };
      });
    configurations
      .getConfiguration('DUMMY_ID', 'DUMMY_PAYMENT_METHOD', true)
      .then((response) => {
        expect(response).toEqual(null);
        done();
      }, done).catch(done);
  });
  it('should return GATEWAY_IS_DISABLED when call method getConfiguration returns a valid model with body.enable false', (done) => {
    logger.mute();
    let configuration = configurationDriver.VALID_CONFIGURATION_OBJECT();
    configuration.enabled = false;
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, [configuration]);
              },
            };
          },
        };
      });
    configurations
      .getConfiguration('DUMMY_ID', 'DUMMY_PAYMENT_METHOD', false)
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex).toEqual('GATEWAY_IS_DISABLED');
        done();
      }).catch(done);
  });
  it('should return a valid model when call method getConfiguration is successful', (done) => {
    logger.mute();
    let configuration = configurationDriver.VALID_CONFIGURATION_OBJECT();
    configuration.enabled = false;
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, [configuration]);
              },
            };
          },
        };
      });
    configurations
      .getConfiguration('DUMMY_ID', 'DUMMY_PAYMENT_METHOD', true)
      .then((response) => {
        expect(response).toBeDefined();
        expect(response).toHaveProperty('client_id');
        expect(response).toHaveProperty('enabled');
        expect(response).toHaveProperty('merchantAuthKey');
        expect(response).toHaveProperty('merchantId');
        expect(response).toHaveProperty('branchId');
        expect(response).toHaveProperty('terminalId');
        expect(response).toHaveProperty('commerceCodeDebit');
        expect(response).toHaveProperty('commerceCodeCmr');
        expect(response).toHaveProperty('channel');
        expect(response).toHaveProperty('application');
        expect(response).toHaveProperty('payment_method');
        response.toJSON();
        done();
      }, done).catch(done);
  });

  // getGatewaysAvailability
  it('should pass sanity check method for getGatewaysAvailability', () => {
    expect(configurations.getGatewaysAvailability).toBeDefined();
  });
  it('should return an  error when call method getGatewaysAvailability DB return an error', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(new Error('DUMMY_ERROR'), null);
              },
            };
          },
        };
      });
    configurations
      .getGatewaysAvailability('APPLICATION_ID')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex.message).toEqual('DUMMY_ERROR');
        done();
      }).catch(done);
  });
  it('should return a array of configurations when call method getGatewaysAvailability is successful', (done) => {
    logger.mute();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, [configurationDriver.VALID_CONFIGURATION_OBJECT()]);
              },
            };
          },
        };
      });
    configurations
      .getGatewaysAvailability('APPLICATION_ID')
      .then((response) => {
        expect(response).toBeDefined();
        expect(response).toEqual([configurationDriver.VALID_CONFIGURATION_OBJECT()]);
        done();
      });
  });

  // updateConfiguration
  it('should pass sanity check method for updateConfiguration', () => {
    expect(configurations.updateConfiguration).toBeDefined();
  });
  it('should return an error when call updateConfiguration and base model is not a model', (done) => {
    logger.mute();
    configurations
      .updateConfiguration('BASE_DUMMY_MODEL')
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        done();
      }).catch(done);
  });
  it('should return an error when call updateConfiguration method with invalid model', (done) => {
    logger.mute();
    let invalidModel = configurationDriver.INVALID_NEW_CONFIGURATION_MODEL();
    configurations
      .updateConfiguration(invalidModel)
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex).toEqual(new Error('INVALID_MODEL'));
        done();
      }).catch(done);
  });
  it('should return an error when call updateConfiguration method and there is a DB ERROR', (done) => {
    logger.mute();
    let validModel = configurationDriver.VALID_CONFIGURATION_OBJECT();
    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(null);
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(new Error('DUMMY_ERROR'), null);
              },
            };
          },
        };
      });
    configurations
      .updateConfiguration(validModel)
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex).toEqual(new Error('DUMMY_ERROR'));
        done();
      }).catch(done);
  });
  it('should return an error when call updateConfiguration method and there is a DB ERROR when try to create a document', (done) => {
    logger.mute();
    let validModel = configurationDriver.VALID_CONFIGURATION_OBJECT();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, []);
              },
            };
          },
          createDocument: (collectionLink, model, options, callback) => {
            callback(new Error('DUMMY_ERROR'), null);
          },
        };
      });
    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(null);
    configurations
      .updateConfiguration(validModel)
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex).toEqual(new Error('DUMMY_ERROR'));
        done();
      }).catch(done);
  });
  it('should return a configuration when call updateConfiguration and when getConfiguration output is null', (done) => {
    logger.mute();
    let validModel = configurationDriver.VALID_CONFIGURATION_OBJECT();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, []);
              },
            };
          },
          createDocument: (collectionLink, model, options, callback) => {
            callback(null, validModel);
          },
        };
      });
    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(null);
    configurations
      .updateConfiguration(validModel)
      .then((configuration) => {
        expect(configuration).toBeDefined();
        configuration.toJSON();
        expect(configuration).toHaveProperty('client_id');
        expect(configuration).toHaveProperty('enabled');
        expect(configuration).toHaveProperty('merchantAuthKey');
        expect(configuration).toHaveProperty('merchantId');
        expect(configuration).toHaveProperty('branchId');
        expect(configuration).toHaveProperty('terminalId');
        expect(configuration).toHaveProperty('commerceCodeDebit');
        expect(configuration).toHaveProperty('commerceCodeCmr');
        expect(configuration).toHaveProperty('channel');
        expect(configuration).toHaveProperty('application');
        expect(configuration).toHaveProperty('payment_method');
        done();
      });
  });
  it('should return an error when call updateConfiguration method and there is a DB ERROR when try to update a document', (done) => {
    logger.mute();
    let validModel = configurationDriver.VALID_CONFIGURATION_OBJECT();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, [configurationDriver.VALID_CONFIGURATION_OBJECT()]);
              },
            };
          },
          replaceDocument: (modelSelf, model, callback) => {
            callback(new Error('DUMMY_ERROR'), null);
          },
        };
      });
    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(configurationDriver.VALID_CONFIGURATION_OBJECT());
    configurations
      .updateConfiguration(validModel)
      .then(() => {
        throw new Error('Expected Error but got OK');
      }, (ex) => {
        expect(ex).toBeDefined();
        expect(ex).toEqual(new Error('DUMMY_ERROR'));
        done();
      }).catch(done);
  });
  it('should return a configuration when call updateConfiguration method and update is successful', (done) => {
    logger.mute();
    let validModel = configurationDriver.VALID_CONFIGURATION_OBJECT();
    sandbox
      .stub(mockDB, 'getConnection')
      .callsFake(() => {
        return {
          queryDocuments: (collectionLink, query) => {
            return {
              toArray: (callback) => {
                return callback(null, [configurationDriver.VALID_CONFIGURATION_OBJECT()]);
              },
            };
          },
          replaceDocument: (modelSelf, model, callback) => {
            callback(null, validModel);
          },
        };
      });
    sandbox
      .stub(configurations, 'getConfiguration')
      .resolves(configurationDriver.VALID_CONFIGURATION_OBJECT());
    configurations
      .updateConfiguration(validModel)
      .then((configuration) => {
        expect(configuration).toBeDefined();
        configuration._test = '_id';
        configuration.toJSON();
        expect(configuration).toHaveProperty('client_id');
        expect(configuration).toHaveProperty('enabled');
        expect(configuration).toHaveProperty('merchantAuthKey');
        expect(configuration).toHaveProperty('merchantId');
        expect(configuration).toHaveProperty('branchId');
        expect(configuration).toHaveProperty('terminalId');
        expect(configuration).toHaveProperty('commerceCodeDebit');
        expect(configuration).toHaveProperty('commerceCodeCmr');
        expect(configuration).toHaveProperty('channel');
        expect(configuration).toHaveProperty('application');
        expect(configuration).toHaveProperty('payment_method');
        done();
      });
  });
});
