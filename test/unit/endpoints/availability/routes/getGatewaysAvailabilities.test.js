'use strict';
const sinon = require('sinon');
const supertest = require('supertest');
const logger = require('peanut-restify/logger');
const bootstrap = require('./../../../../drivers/bootstrap');
const restifyRouter = require('restify-router');

const app = require('./../../../../../server/server');
const sandbox = sinon.createSandbox();

describe('GET /:application/availability/gateways', () => {
  // Setting things up....
  let configurations, routeToTest, testServer;
  beforeAll(() => {
    bootstrap.setEnvironment();
    testServer = app.getServer();
    configurations = require('./../../../../../server/endpoints/configurations/clients/$configurations');
    routeToTest = require('./../../../../../server/endpoints/availability/routes/getGatewaysAvailabilities');
    expect(restifyRouter).toBeDefined();
    const router = new restifyRouter.Router();
    routeToTest(router);
    router.applyRoutes(testServer, 'test_route');
  });

  afterEach(() => {
    logger.unmute();
    sandbox.restore();
  });

  it('should return 500 (Server Error) status code when call method getGatewaysAvailability and Errors Occurs', (done) => {
    const expectedResult = new Error('DUMMY_ERROR');
    expectedResult.type = expectedResult.message;

    sandbox
      .stub(configurations, 'getGatewaysAvailability')
      .rejects(expectedResult);

    supertest(testServer)
      .get('/:application/availability/gateways')
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send()
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(500);
        expect(response.body).toHaveProperty('type');
        expect(response.body.type).toEqual(expectedResult.type);
        done();
      });
  });
  it('should return 200 status code when call method getGatewaysAvailability successfully', (done) => {
    const expectedResult = '<html></html>';
    sandbox
      .stub(configurations, 'getGatewaysAvailability')
      .resolves(expectedResult);

    supertest(testServer)
      .get('/:application/availability/gateways')
      .set('Authorization', `bearer ${bootstrap.getValidJwt()}`)
      .send()
      .end((err, response) => {
        if (err) throw err;
        expect(response).toBeDefined();
        expect(response.status).toEqual(200);
        expect(response).toHaveProperty('header');
        expect(response).toHaveProperty('body');
        done();
      });
  });
});
